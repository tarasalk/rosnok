<?php

namespace App;

use App\Facades\PeopleApiFacade;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class User extends Authenticatable
{
    protected $hidden = ['remember_token'];
    protected $guarded = ['remember_token'];

	protected $cacheUsers = [];

	public function getRoles($user_id = null) {
		if (is_null($user_id))
			$user_id = Auth::user()->id;

        $response = Cache::remember('userRoles'.$user_id, 2, function() use ($user_id) {
            return PeopleApiFacade::getRoles($user_id);
        });

		if ($response)
			return $response->roles;

		return null;
	}

	public function getUser($user_id = null) {
		if (is_null($user_id))
			$user_id = Auth::user()->id;

		if (isset($this->cacheUsers[$user_id])) {
			$oUser = $this->cacheUsers[$user_id];
		}
		else {
            $response = PeopleApiFacade::getEntityUser($user_id);

            if ($response) {
                $oUser = $response->user;
                $this->cacheUsers[$user_id] = $oUser;
            }
            else exit(PeopleApiFacade::getLastError());
		}

		return $oUser;
	}

	public function getFirstName($user_id = null) {
		return $this->getUser($user_id)->first_name;
	}

	public function getLastName($user_id = null) {
		return $this->getUser($user_id)->last_name;
	}

    public function getName($user_id = null) {
        return $this->getFirstName().' '.$this->getLastName();
    }
}
