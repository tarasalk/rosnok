<?php

namespace App\Providers;

use App\Helpers\CurlWrapper;
use App\Helpers\SmsHelper;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\App;

class SmsServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SmsHelper::class, function($app)
        {
            return new SmsHelper(env('URL_SMS'), env('SEND_SMS'), new CurlWrapper());
        });

        App::bind(SmsHelper::class, function() {
            return new SmsHelper(env('URL_SMS'), env('SEND_SMS'), new CurlWrapper());
        });
    }

    public function provides()
    {
        return [SmsHelper::class];
    }
}
