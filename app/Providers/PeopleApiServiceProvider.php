<?php

namespace App\Providers;

use App\Helpers\CurlWrapper;
use App\Models\Api\People;
use Illuminate\Support\ServiceProvider;

class PeopleApiServiceProvider extends ServiceProvider
{
    //protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
		$curl = new CurlWrapper();
        $this->app->singleton(People::class, function ($app) use ($curl) {
            return new People($curl);
        });
    }

    public function provides()
    {
        return [People::class];
    }
}
