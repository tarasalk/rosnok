<?php

namespace App\Providers;

use App\Helpers\CurlWrapper;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class CurlServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CurlWrapper::class, function ($app) {
            return new CurlWrapper();
        });

		App::bind(CurlWrapper::class, function() {
			return new CurlWrapper();
		});
    }

    public function provides()
    {
        return [CurlWrapper::class];
    }
}
