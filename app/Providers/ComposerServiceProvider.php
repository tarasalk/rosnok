<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
	/**
	 * Регистрация привязок в контейнере.
	 *
	 * @return void
	 */
	public function boot()
	{
        // TODO а если не клиент?
		View::composer('cabinet.role.client.main', 'App\Http\ViewComposers\ClientComposer');

		View::composer('cabinet.include.sidebar', 'App\Http\ViewComposers\CabinetSidebarComposer');
	}

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
