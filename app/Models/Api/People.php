<?php

namespace App\Models\Api;

use App\Helpers\CurlWrapper;

class People {
	protected $curl;

    private $last_error;

	public function __construct(CurlWrapper $curl) {
		$this->curl = $curl;
	}

    /**
     * @return mixed
     */
    private function sendRequest($url, $params, $logging = false) {
        $response = $this->curl->send('POST', env('URL_PEOPLE').$url, $params, $logging);

        if ($response->status == 'ok') {
            $this->last_error = '';
            return empty($response->response) ? true : $response->response;
        }
        elseif ($response->status == 'error') {
            $this->last_error = $response->response;
            return false;
        }
        else {
            $this->last_error = 'undefined_error';
            return false;
        }
    }

    public function getLastError() {
        return $this->last_error;
    }

	/* -------------------------------------*/
	/* ------------ РЕГИСТРАЦИЯ ------------*/
	/* -------------------------------------*/

    /**
     * @return mixed
     */
	public function registrationCreate($country, $phone, $first_name, $last_name, $middle_name = '') {
        return $this->sendRequest('/registration/create', [
            'query' => [
                'country' => $country,
                'phone' => $phone,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'middle_name' => $middle_name
            ]
        ], true);
	}

	public function sendCodeConfirmPhone($country, $phone) {
        return $this->sendRequest('/registration/sendCodeConfirmPhone', [
            'query' => [
                'country' => $country,
                'phone' => $phone
            ]
        ], true);
	}

	public function confirmPhone($country, $phone, $code) {
        return $this->sendRequest('/registration/confirmPhone', [
            'query' => [
                'country' => $country,
                'phone' => $phone,
                'code' =>$code
            ]
        ], true);
	}

    public function recoveryPassword($country, $phone) {
        return $this->sendRequest('/registration/recoveryPassword', [
            'query' => [
                'country' => $country,
                'phone' => $phone
            ]
        ], true);
    }


    /* -------------------------------------*/
	/* --------------- ПРОЧЕЕ --------------*/
    /* -------------------------------------*/

	public function userSearch($country, $phone, $password = '') {
        return $this->sendRequest('/user/search', [
            'query' => [
                'country' => $country,
                'phone' => $phone,
                'password' => $password
            ]
        ]);
	}

	/**
	 * получение базовой инфы пользователя(ей) по user_id
	 * @param mixed $user int/array
	 * @return mixed
	 */
	public function userGet($user, $fields = []) {
		if (is_array($user)) {
			$oneUser = false;
			$user_ids = $user;
		}
		else {
			$oneUser = true;
			$user_ids = [$user];
		}

        $response = $this->sendRequest('/user/get', [
            'json' => [
                'user_ids' => $user_ids,
                'fields' => $fields
            ]
        ], false);

        if ($response) {
            if ($oneUser)
                return isset($response->users->{$user}) ? $response->users->{$user} : [];
            else
                return $response->users;
        }

        return false;
	}

    public function getRoles($user_id) {
        return $this->sendRequest('/user/getRoles', [
            'query' => ['user_id' => $user_id]
        ], false);
    }

	public function setRole($user_id, $role_id) {
        return $this->sendRequest('/user/setRole', [
            'query' => [
                'user_id' => $user_id,
                'role_id' => $role_id
            ]
        ]);
	}

    public function getEntityUser($user_id) {
        return $this->sendRequest('/user/getEntity', [
            'query' => ['user_id' => $user_id]
        ]);
    }


    /* -------------------------------------*/
	/* -------------- ПАСПОРТ --------------*/
    /* -------------------------------------*/

	public function getPassportData($user_id) {
        return $this->sendRequest('/passport/data/get', [
            'query'=> ['user_id' => $user_id]
        ], false);
	}

    public function createOrEditPassportData($user_id, $data) {
        return $this->sendRequest('/passport/data/createOrEdit', [
            'query' => [
                'user_id' => $user_id,
                'data' => $data
            ]
        ], false);
    }

    /**
     * @param string $fileName
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     * @return mixed
     */
    public function uploadPassportScan($file, $fileName) {
        return $this->curl->sendMultipart(env('URL_PEOPLE').'/upload/passportScans', [
            [
                'name' => $fileName,
                'contents' => fopen($file->getRealPath(), 'r')
            ],
            [
                'name' => 'extension',
                'contents' => $file->getClientOriginalExtension()
            ]
        ]);
    }
}
