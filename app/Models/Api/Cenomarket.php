<?php

namespace App\Models\Api;

use App\Helpers\CurlWrapper;

class Cenomarket {
	protected $curl;

    private $last_error;

	public function __construct(CurlWrapper $curl) {
		$this->curl = $curl;
	}

    /**
     * @return mixed
     */
    private function sendRequest($url, $params, $logging = false) {
        $response = $this->curl->send('POST', env('URL_CENOMARKET').$url, $params, $logging);

        if ($response->status == 'ok') {
            $this->last_error = '';
            return empty($response->response) ? true : $response->response;
        }
        elseif ($response->status == 'error') {
            $this->last_error = $response->error;
            return false;
        }
        else {
            $this->last_error = 'undefined_error';
            return false;
        }
    }

    public function getLastError() {
        return $this->last_error;
    }

	public function getUserIdBySubdomain($subdomain) {
        return $this->sendRequest('/api/getUserIdBySubdomain', [
            'query' => [
                'subdomain' => $subdomain
            ]
        ], true);
	}
}
