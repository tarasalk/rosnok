<?php

namespace App\Models\Logic\Docs;

class Docs
{
	public static function getAllMainDocs() {
		return [
			[
				'name' => 'Заявление о вступлении',
				'small_name' => 'statement_entry'
			],
			[
				'name' => 'Выписка из реестра пайщиков',
				'small_name' => 'statement_copying'
			],
			[
				'name' => 'Договор на юридические работы',
				'small_name' => 'contract'
			],
			[
				'name' => 'Новый договор на юридические работы',
				'small_name' => 'contract_new'
			],
			[
				'name' => 'Уведомление о приеме документов',
				'small_name' => 'notification_acceptance_doc'
			],
			[
				'name' => 'Протокол заседания совета',
				'small_name' => 'protocol'
			]
		];
	}

	public static function getAvailableMainDocs($client_id) {
		$aDocs = self::getAllMainDocs();

		$availableDocs = [];
		foreach ($aDocs as $doc) {
			$docAccess = DocsAccess::checkMainDoc($doc['small_name']);

			if ($docAccess)
				$availableDocs[] = $doc;
		}

		return $availableDocs;
	}
}
