<?php

namespace App\Models;

use App\Facades\PeopleApiFacade;
use App\Helpers\ArrayMerge;
use App\Helpers\PaginatorWrapper;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
	protected $guarded = ['id'];

	public function office() {
		return $this->hasOne(Offices::class, 'id', 'office_id');
	}

    public function inboxDocs() {
        return $this->hasMany(InboxDocs::class, 'client_id', 'client_id');
    }

    public static function getClientsCategoriesCount($agent_id = null) {
        return (object) [
            'all'               => Client::byAgentId($agent_id)->count(),
            'withoutDelay'      => Client::byAgentId($agent_id)->withoutDelay()->count(),
            'notPayRate'        => Client::byAgentId($agent_id)->notPayRate()->count(),
            'soonPay'           => Client::byAgentId($agent_id)->soonPay()->count(),
            'delayLess1Month'   => Client::byAgentId($agent_id)->delayLess1Month()->count(),
            'delayMore1Month'   => Client::byAgentId($agent_id)->delayMore1Month()->count(),
        ];
    }

    public function scopeByAgentId($query, $agent_id = null) {
        if (isset($agent_id))
            return $query->where('agent_id', $agent_id);

        return $query;
    }

    public function scopeWithoutDelay($query) {
        return $query->where('date_end_paid_month', '>', date('Y-m-d H:i:s', time() + 3600*24*7));
    }

    public function scopeNotPayRate($query) {
        return $query->where('pay_status', 0);
    }

    public function scopeSoonPay($query) {
        return $query->where('date_end_paid_month', '<', date('Y-m-d H:i:s', time() + 3600*24*7))
                     ->where('date_end_paid_month', '>', date('Y-m-d H:i:s', time()));
    }

    public function scopeDelayLess1Month($query) {
        return $query->where('date_end_paid_month', '>', date('Y-m-d H:i:s', time() - 3600*24*30))
                     ->where('date_end_paid_month', '<', date('Y-m-d H:i:s', time()));
    }

    public function scopeDelayMore1Month($query) {
        return $query->where('date_end_paid_month', '<', date('Y-m-d H:i:s', time() - 3600*24*30));
    }
}
