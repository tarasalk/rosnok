<?php

namespace App\Models;

use App\Facades\PeopleApiFacade;
use Illuminate\Database\Eloquent\Model;

class InboxDocs extends Model {
	const status_denied = -1; // отклонен
	const status_new = 0; // новый
	const status_rebuke = 1; // принят с замечаниями
	const status_accepted = 2; // принят

    public function events()
    {
        return $this->hasMany(InboxDocEvents::class, 'doc_id', 'id');
    }

    public function event() {
        return $this->hasOne(InboxDocEvents::class, 'doc_id', 'id');
    }

    // TODO перенести в репозиторий?
    public static function getDocsByStatus($status = 'new') {
        $aDocs = InboxDocs::with('events');

        switch ($status) {
            case 'all':
                break;

            case 'accepted':
                $aDocs = $aDocs->where('status', self::status_accepted);
                break;

            case 'rebuke':
                $aDocs = $aDocs->where('status', self::status_rebuke);
                break;

            case 'denied':
                $aDocs = $aDocs->where('status', self::status_denied);
                break;

            case 'new':
                $aDocs = $aDocs->where('status', self::status_new);
                break;

            default:
                return [];
        }

        $aDocs = $aDocs->orderBy('id', 'desc')->get();

        $client_ids = [];
        $sender_ids = [];
        foreach($aDocs as $aDoc) {
            $client_ids[] = $aDoc->client_id;
            $sender_ids[] = $aDoc->sender_id;
        }

        $aUsersIdsPeople = array_merge($client_ids, $sender_ids);
        $aUsersIdsPeople = array_unique($aUsersIdsPeople);

        $response = PeopleApiFacade::userGet($aUsersIdsPeople);
        if ($response) {
            $aUsersPeople = $response;
        }
        else {
            $aDocs = [];
            $aUsersPeople = [];
        }

        return ['aDocs' => $aDocs, 'aUsersPeople' => $aUsersPeople];
    }
}
