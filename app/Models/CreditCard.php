<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CreditCard extends Model {
	public $table = 'credit_cards';
	protected $guarded = ['id'];
}
