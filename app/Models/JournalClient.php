<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JournalClient extends Model
{
    const status_cold = 1;
    const status_warm = 2;
    const status_hot = 3;
    const status_real = 4;

    protected $guarded = ['id'];
}
