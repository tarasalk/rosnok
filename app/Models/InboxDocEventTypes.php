<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InboxDocEventTypes extends Model
{
    public $timestamps = false;
}
