<?php

namespace App\Models\Log;

use Illuminate\Database\Eloquent\Model;

class Curl extends Model
{
    protected $table = 'log_curl';

    protected $guarded = ['id'];
}
