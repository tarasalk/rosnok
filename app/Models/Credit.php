<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Credit
{
	const type_consumer = 'consumer';
	const type_card = 'card';
	const type_micro = 'micro';

	public static function addCredit($client_id, $params, $type) {
		if (isset($params['credit_task'])) {
			$aCreditTasks = $params['credit_task'];
			unset($params['credit_task']);
		}

		switch ($type) {
			case self::type_consumer:
				$oCredit = new CreditConsumer($params);
				break;

			case self::type_card:
				$oCredit = new CreditCard($params);
				break;

			case self::type_micro:
				$oCredit = new CreditMicro($params);
				break;

			default:
				return ['success' => false, 'error' => 'undefined_type'];
		}

		$oCredit->client_id = $client_id;
		$oCredit->save();

		if (isset($aCreditTasks)) CreditTask::addTasks($aCreditTasks, $oCredit->id, $type);

		return ['success' => true];
	}

	public static function editCredit($params, $type, $credit_id) {
		if (isset($params['credit_task'])) {
			$aCreditTasks = $params['credit_task'];
			unset($params['credit_task']);
		}

		switch ($type) {
			case self::type_consumer:
				$oCredit = CreditConsumer::find($credit_id);
				break;

			case self::type_card:
				$oCredit = CreditCard::find($credit_id);
				break;

			case self::type_micro:
				$oCredit = CreditMicro::find($credit_id);
				break;

			default:
				return ['success' => false, 'error' => 'undefined_type'];
		}

		if (isset($params['bank_name'])) $oCredit->bank_name = $params['bank_name'];
		if (isset($params['mfo_id'])) $oCredit->mfo_id = $params['mfo_id'];
		if (isset($params['contract_number'])) $oCredit->contract_number = $params['contract_number'];
		if (isset($params['card_number'])) $oCredit->card_number = $params['card_number'];
		if (isset($params['date_contract_create'])) $oCredit->date_contract_create = $params['date_contract_create'];
		if (isset($params['credit_sum'])) $oCredit->credit_sum = $params['credit_sum'];
		if (isset($params['account_number'])) $oCredit->account_number = $params['account_number'];
		if (isset($params['credit_commitments_name'])) $oCredit->credit_commitments_name = $params['credit_commitments_name'];
		if (isset($params['last_pay_sum'])) $oCredit->last_pay_sum = $params['last_pay_sum'];
		if (isset($params['date_last_pay'])) $oCredit->date_last_pay = $params['date_last_pay'];
		if (isset($params['date_credit_end'])) $oCredit->date_credit_end = $params['date_credit_end'];
		if (isset($params['average_monthly_pay'])) $oCredit->average_monthly_pay = $params['average_monthly_pay'];
		if (isset($params['notation'])) $oCredit->notation = $params['notation'];
		$oCredit->save();

		CreditTask::where('credit_type', $type)->where('credit_id', $credit_id)->delete();

		if (isset($aCreditTasks)) CreditTask::addTasks($aCreditTasks, $oCredit->id, $type);

		return ['success' => true];
	}

	public static function getAllCreditsByClientId($client_id) {
		$aCredits['consumer'] = CreditConsumer::where('client_id',  $client_id)->get();
		$aCredits['card'] = CreditCard::where('client_id',  $client_id)->get();
		$aCredits['micro'] = CreditMicro::where('client_id',  $client_id)->get();

		return $aCredits;
	}

	public static function getCreditInfo($type, $credit_id) {
		switch ($type) {
			case self::type_consumer:
				$aCredit = CreditConsumer::where('id',  $credit_id)->first();
				break;

			case self::type_card:
				$aCredit = CreditCard::where('id',  $credit_id)->first();
				break;

			case self::type_micro:
				$aCredit = CreditMicro::where('id',  $credit_id)->first();
				break;
		}

		$aCredit['tasks'] = CreditTask::getTasksByCreditId($type, $credit_id);

		return $aCredit;
	}

	public static function calculateRate($client_id) {
		$aCreditsGroup = static::getAllCreditsByClientId($client_id);

		$creditsCount = 0; $creditsSum = 0;
		foreach ($aCreditsGroup as $key => $aCredits ) {
			if (!in_array($key, ['consumer', 'card', 'micro']))
				continue;

            $creditsCount += count($aCredits);

			foreach ($aCredits as $oCredit)
                $creditsSum += $oCredit->credit_sum;
		}

		if ($creditsCount <= 3 && $creditsSum < 150000)
			$rate = 3015;
		elseif ($creditsCount <= 5 && $creditsSum < 300000)
			$rate = 5730;
		elseif ($creditsCount <= 7 && $creditsSum < 800000)
			$rate = 6735;
		elseif ($creditsSum < 3000000)
			$rate = 9550;
		else
			return 'undefined_rate';

		return $rate;
	}
}