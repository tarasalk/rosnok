<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CreditTaskList extends Model
{
    public $timestamps = false;
    protected $table = 'credit_task_list';
}
