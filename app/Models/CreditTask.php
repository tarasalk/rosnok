<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CreditTask extends Model
{
	public $timestamps = false;
	protected $guarded = ['id'];

	public static function addTasks(array $aTaskId, $credit_id, $credit_type) {
		foreach ($aTaskId as $task_id) {
			CreditTask::create([
				'credit_id' => $credit_id,
				'task_id' => $task_id,
				'credit_type' => $credit_type
			]);
		}
	}

	public static function getTasksByCreditId($type, $credit_id) {
		$aTask = CreditTask::where('credit_type', $type)->where('credit_id', $credit_id)
			->leftJoin('credit_task_list', 'task_id', '=', 'credit_task_list.id')
			->get();

		return $aTask;
	}
}