<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InboxDocEvents extends Model
{
    protected $guarded = ['id'];
}
