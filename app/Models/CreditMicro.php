<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CreditMicro extends Model {
    public $table = 'credit_micro';
	protected $guarded = ['id'];
}
