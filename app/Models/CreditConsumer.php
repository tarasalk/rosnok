<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CreditConsumer extends Model {
	public $table = 'credit_consumers';
    protected $guarded = ['id'];
}
