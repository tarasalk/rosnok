<?php

namespace App\Models;

use App\Facades\PeopleApiFacade;
use App\Facades\SmsHelperFacade;
use App\Helpers\ArrayMerge;
use App\Helpers\SmsHelper;
use Illuminate\Database\Eloquent\Model;

class LandingRequest extends Model {
    protected $guarded = ['id'];

    const CLIENT_TYPE_NOT_A_CLIENT = -1;
    const CLIENT_TYPE_NEW = 0;
    const CLIENT_TYPE_POTENTIAL = 1;
    const CLIENT_TYPE_REAL = 2;

	public static function getNewRequests($agent_id) {
		$aRequests = LandingRequest::whereRaw('agent_id = ? && client_type = ?', [$agent_id, self::CLIENT_TYPE_NEW])->get();

        $callbackTime = self::getAgentWorkingTime()['cb_period'] * 60;
        foreach ($aRequests as $oRequest) {
            $oRequest->timeLeftSeconds = $callbackTime - (time() - $oRequest->created_at->getTimestamp());
        }
        
		return $aRequests;
	}

	public static function getProcessedRequests($agent_id) {
		$aRequests = LandingRequest::whereRaw('agent_id = ? && client_type != ?', [$agent_id, ''])->get();

		$client_ids = [];
		foreach($aRequests as $aRequest) $client_ids[] = $aRequest['agent_id'];

		$aAgents = PeopleApiFacade::userGet($client_ids);
		if ($aAgents != 'undefined_error') {
			$aRequests = ArrayMerge::mergeByKey2(
				$aRequests->toArray(),
				$aAgents,
				'agent_id',
				'user_id',
				'agent'
			);
		}
		else {
			$aRequests = [];
		}

		return $aRequests;
	}

    public static function getRequestByPhone($phone, $date_check = null) {
        if (is_null($date_check))
            return LandingRequest::where('user_phone', $phone)->first();
        else
            return LandingRequest::whereRaw('user_phone = ? && created_at > ?', [$phone, $date_check])->first();
    }

    public static function addNewRequest($name, $phone, $agent_id) {
        $oRequest = LandingRequest::create();
        $oRequest->user_name = $name;
        $oRequest->user_phone = $phone;
        $oRequest->agent_id = $agent_id;

        if ($oRequest->save()) {
            $result = self::sendSmsAgentHasNewRequest($agent_id, $name, $phone);

            return $result ? true:false;
        }

        return false;
    }

    private static function sendSmsAgentHasNewRequest($agentId, $userName, $userPhone) {
        $response = PeopleApiFacade::userGet($agentId);
        if ($response) {
            $oAgent = $response;

            $message = sprintf("У вас новая завка в %sМск +%s %s",
                date('H:i'),
                $userPhone,
                $userName
            );

            $agentPhone = $oAgent->phone->country . $oAgent->phone->phone;

            return SmsHelperFacade::sendSms($agentPhone, $message);
        }

        return false;
    }

    /**
     * @return array
     */
    public static function getAgentWorkingTime() {
        return [
            'begin' => [
                'hour' => 9,
                'week_day' => 1
            ],
            'end' => [
                'hour' => 17,
                'week_day' => 5
            ],
            'cb_period' => 30 // макс. ремя за которое агент должен перезвонить клиенту
        ];
    }

    public static function getAgentCallbackMessage($agent_id) {
        $agentWorkingTime = self::getAgentWorkingTime();

        $hour_now = date('H');
        $day_of_week_now = date('N');
        $isWorkingTime =
            $hour_now >= $agentWorkingTime['begin']['hour'] &&
            $hour_now <= $agentWorkingTime['end']['hour'] &&
            $day_of_week_now >= $agentWorkingTime['begin']['week_day'] &&
            $day_of_week_now <= $agentWorkingTime['end']['week_day'];

        $response = PeopleApiFacade::userGet($agent_id);
        if ($response) {
            $oAgent = $response;

            // TODO форматирование +7 (123) 456-78-90
            $agentPhone = '+'.$oAgent->phone->country.$oAgent->phone->phone;
            $agentName = $oAgent->base->first_name.' '.$oAgent->base->last_name;

            if ($isWorkingTime) {
                $message = sprintf("
                    Благодарим за обращение!
                    Вы записаны на бесплатную консультацию.
                    В течение %s минут Вам позвонит помощник юриста %s с номера %s.
                    Пожалуйста, возьмите трубку.",
                    $agentWorkingTime['cb_period'],
                    $agentName,
                    $agentPhone
                );
            }
            else {
                $message = sprintf("Благодарим за обращение!
                        Вы записаны на бесплатную консультацию.
                        В рабочее время Вам позвонит помощник юриста %s с номера %s.
                        Пожалуйста, возьмите трубку.",
                    $agentName,
                    $agentPhone
                );
            }

            return $message;
        }

        return 'undefined_error';
    }
}
