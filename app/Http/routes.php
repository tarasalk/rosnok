<?php
Route::group(['middleware' => ['envFix', 'web', 'checkAuth']], function() {
	/* главная */
	Route::get('/', ['as' => 'main', 'uses' => 'MainController@index']);

    Route::group(['namespace' => 'Articles'], function() {
        Route::get('/itogi', ['as' => 'articleSudAll', 'uses' => 'ViewController@viewByType']);
        Route::get('/itogi/{name}', ['as' => 'articleSud', 'uses' => 'ViewController@viewByName']);

        Route::get('/news', ['as' => 'articleNewsAll', 'uses' => 'ViewController@viewByType']);
        Route::get('/news/{name}', ['as' => 'articleNews', 'uses' => 'ViewController@viewByName']);
    });

	/* авторизация */
	Route::group(['namespace' => 'Auth', 'prefix' => 'auth'], function() {
		Route::get('/', ['as' => 'needAuth', 'uses' => 'AuthController@pageAuth']);
		Route::get('/vk', ['as' => 'authVk', 'uses' => 'AuthController@authVk']);
		Route::get('/vk/callback', ['as' => 'authVkCallback', 'uses' => 'AuthController@authVkCallback']);
		Route::post('/password', ['as' => 'authPassword', 'uses' => 'AuthController@authPassword']);
		Route::get('/logout', 'AuthController@logout');

		/* регистрация */
		Route::group(['prefix' => 'registration'], function() {
			Route::get('/start', ['as' => 'registrationStart', 'uses' => 'RegistrationController@start']);
			Route::post('/sendPhoneCode', ['as' => 'sendPhoneCode', 'uses' => 'RegistrationController@sendPhoneCode']);
			Route::post('/confirmPhoneCode', ['as' => 'confirmPhoneCode', 'uses' => 'RegistrationController@confirmPhoneCode']);
			Route::post('/recoveryPassword', ['as' => 'recoveryPassword', 'uses' => 'RegistrationController@recoveryPassword']);
		});
	});

	/* требуется авторизация */
	Route::group(['middleware' => ['auth']], function() {
		Route::group(['namespace' => 'Articles'], function() {
			Route::group(['prefix' => 'admin', 'middleware' => 'checkRole:admin'], function() {
				Route::get('/articles', ['as' => 'articlesAdminMain', 'uses' => 'AdminController@viewMain']);
				Route::post('/article/create', ['as' => 'articleCreate', 'uses' => 'AdminController@createArticle']);
				Route::post('/article/edit', ['as' => 'articleEdit', 'uses' => 'AdminController@editArticle']);
				Route::post('/article/imgUpload', ['as' => 'articleImgUpload', 'uses' => 'AdminController@imgUpload']);
			});
		});

		Route::group(['prefix' => 'user'], function() {
            // TODO явно не ProfileController
			Route::get('/cabinet/desktop', ['as' => 'cabinetDesktop', 'uses' => 'ProfileController@desktop']);
			Route::get('/cabinet/profile/{user_id}', ['as' => 'cabinetProfile', 'uses' => 'ProfileController@viewProfile']);
		});

		Route::group(['middleware' => 'checkRole:agent', 'namespace' => 'Agent', 'prefix' => 'agent'], function() {
			Route::group(['prefix' => 'clientAdd'], function() {
				Route::get('/', ['as' => 'agentClientAdd', 'uses' => 'AddClientController@viewMain']);
				Route::post('/find', ['as' => 'agentClientAddFind', 'uses' => 'AddClientController@userFind']);
				Route::post('/create', ['as' => 'agentClientAddCreate', 'uses' => 'AddClientController@clientCreate']);
				Route::post('/make', ['as' => 'agentClientAddMade', 'uses' => 'AddClientController@makeUserClient']);
				Route::post('/sendSmsCodeConfirmPhone', ['as' => 'agentClientAddSendCode', 'uses' => 'AddClientController@sendCodeConfirmPhone']);
				Route::post('/confirmPhone', ['as' => 'agentClientAddConfirmPhone', 'uses' => 'AddClientController@clientConfirmPhone']);
                Route::post('/getCreditsView', ['as' => 'agentClientAddGetCreditsView', 'uses' => 'AddClientController@getCreditsView']);
                Route::post('/calculateRate', ['as' => 'agentClientAddCalculateRate', 'uses' => 'AddClientController@calculateRate']);
                Route::post('/confirmRate', ['as' => 'agentClientAddConfirmRate', 'uses' => 'AddClientController@confirmRate']);
                Route::post('/getOffices', ['as' => 'agentClientAddGetOffices', 'uses' => 'AddClientController@getOffices']);
                Route::post('/searchOffices', ['as' => 'agentClientAddSearchOffices', 'uses' => 'AddClientController@searchOffices']);
                Route::post('/confirmChoiceOffice', ['as' => 'agentClientAddConfirmChoiceOffice', 'uses' => 'AddClientController@confirmChoiceOffice']);
                Route::post('/getPassportView', ['as' => 'agentClientAddGetPassportView', 'uses' => 'AddClientController@getPassportView']);
                Route::post('/payRate', ['as' => 'agentClientAddPayRate', 'uses' => 'AddClientController@payRate']);
			});

            Route::group(['prefix' => 'journal'], function() {
                Route::get('/cold', ['as' => 'agentJournalCold', 'uses' => 'JournalController@viewCold']);
                Route::get('/warm', ['as' => 'agentJournalWarm', 'uses' => 'JournalController@viewWarm']);
                Route::get('/hot',  ['as' => 'agentJournalHot', 'uses' => 'JournalController@viewHot']);
                Route::get('/real', ['as' => 'agentJournalReal', 'uses' => 'JournalController@viewReal']);

                Route::post('/addClient', ['as' => 'agentJournalAddClient', 'uses' => 'JournalController@addClient']);
                Route::post('/editClient', ['as' => 'agentJournalEditClient', 'uses' => 'JournalController@editClient']);
            });

			Route::get('/client/cab/{client_id}', ['as' => 'agentClientCab', 'uses' => 'MainController@clientCab']);
			Route::get('/clients/{category?}', ['as' => 'agentClients', 'uses' => 'MainController@clients']);

			Route::get('/landingRequest', ['as' => 'agentLandingRequest', 'uses' => 'MainController@landingRequest']);
			Route::post('/confirmProcessRequest', ['as' => 'agentConfirmProcessRequest', 'uses' => 'MainController@confirmProcessRequest']);


			Route::group(['prefix' => 'inboxDocs'], function() {
				Route::get('/', ['as' => 'agentInboxDocs', 'uses' => 'MainController@viewInboxDocs']);
				Route::post('/add', ['as' => 'agentInboxDocsAdd', 'uses' => 'MainController@inboxDocsAdd']);
			});
		});

		Route::group(['middleware' => 'checkRole:client', 'prefix' => 'client'], function() {
			Route::get('/main', ['as' => 'clientMain', 'uses' => 'ClientController@main']);
			Route::post('/pay/confirm', ['as' => 'clientPayConfirm', 'uses' => 'ClientController@payConfirm']);
			Route::get('/payRate', ['as' => 'clientPayRate', 'uses' => 'ClientController@viewPayRate']);

			Route::get('/credits', ['as' => 'client.credits', 'uses' => 'ClientController@viewCredits']);
			Route::get('/credit/details', ['as' => 'client.credit.details', 'uses' => 'ClientController@detailsCredit']);
			Route::get('/credit/edit', ['as' => 'client.credit.edit', 'uses' => 'ClientController@editCreditView']);

			Route::post('/credit/add/consumer', ['as' => 'client.credit.add.consumer', 'uses' => 'ClientController@addCredit']);
			Route::post('/credit/add/card', ['as' => 'client.credit.add.card', 'uses' => 'ClientController@addCredit']);
			Route::post('/credit/add/micro', ['as' => 'client.credit.add.micro', 'uses' => 'ClientController@addCredit']);

			Route::post('/credit/edit/consumer', ['as' => 'client.credit.edit.consumer', 'uses' => 'ClientController@editCredit']);
			Route::post('/credit/edit/card', ['as' => 'client.credit.edit.card', 'uses' => 'ClientController@editCredit']);
			Route::post('/credit/edit/micro', ['as' => 'client.credit.edit.micro', 'uses' => 'ClientController@editCredit']);
		});

        Route::group(['namespace' => 'Operator'], function() {
            Route::group(['middleware' => 'checkRole:ovs', 'prefix' => 'ovs'], function() {
                Route::get('/inboxDocs/{filter?}', ['as' => 'ovsInboxDocs', 'uses' => 'OVSController@viewInboxDocs']);
                Route::post('/inboxDocs/checked', ['as' => 'ovsInboxDocChecked', 'uses' => 'OVSController@docChecked']);
                Route::post('/inboxDocs/sendLawyer', ['as' => 'ovsInboxDocSendLawyer', 'uses' => 'OVSController@sendLawyer']);
                Route::post('/inboxDoc/get', ['as' => 'ovsGetInboxDocById', 'uses' => 'OVSController@getInboxDocById']);
            });

            Route::group(['middleware' => 'checkRole:oisu', 'prefix' => 'oisu'], function() {
                Route::get('/sentDocs', ['as' => 'oisuSentDocs', 'uses' => 'OISUController@viewSentDocs']);
                Route::post('/sentDocs/edit', ['as' => 'oisuSentDocsEdit', 'uses' => 'OISUController@editSentDoc']);
                Route::post('/sentDocs/hand', ['as' => 'oisuSentDocsHand', 'uses' => 'OISUController@handSentDoc']);
            });

            Route::group(['middleware' => 'checkRole:oiss', 'prefix' => 'oiss'], function() {
                Route::get('/sentDocs', ['as' => 'oissSentDocs', 'uses' => 'OISSController@viewSentDocs']);
                Route::get('/printSentDocs', ['as' => 'oissPrintSentDocs', 'uses' => 'OISSController@viewPrintSentDocs']);
            });
        });

        Route::group(['middleware' => 'checkRole:lawyer', 'prefix' => 'lawyer'], function() {
            Route::get('/inboxDocs/{client_id?}', ['as' => 'lawyerInboxDocs', 'uses' => 'LawyerController@viewInboxDocs']);
            Route::post('/inboxDocs/lawyerRead', ['as' => 'lawyerInboxDocLawyerRead', 'uses' => 'LawyerController@lawyerRead']);

            Route::get('/clients/{category?}', ['as' => 'lawyerClients', 'uses' => 'LawyerController@viewClients']);

            Route::get('/sentDocs/add', ['as' => 'lawyerSentDocsAddView', 'uses' => 'LawyerController@viewSentDocsAdd']);
            Route::post('/sentDocs/add', ['as' => 'lawyerSentDocsAdd', 'uses' => 'LawyerController@SentDocsAdd']);
            Route::post('/search/clients', ['as' => 'lawyerSearchClients', 'uses' => 'LawyerController@searchClients']);
            Route::any('/sentDoc/get/{client_id?}', ['as' => 'lawyerGetClientSentDocs', 'uses' => 'LawyerController@getClientSentDocs']);

            Route::get('/sentDocs', ['as' => 'lawyerSentDocs', 'uses' => 'LawyerController@viewSentDocs']);
        });

		/* документооборот */
		Route::group(['namespace' => 'Docs', 'prefix' => 'docs'], function() {
			Route::get('/get/main', ['as' => 'docsGetMain', 'uses' => 'AccessController@getMainDocs']);
			Route::get('/download/main/{doc_name}', ['as' => 'docsDownloadMain', 'uses' => 'DownloadController@getMainDoc']);
		});

		/* паспортные данные */
		Route::group(['prefix' => 'passport'], function() {
			Route::get('/get', ['as' => 'passportGet', 'uses' => 'PassportController@getData']);
			Route::post('/createOrUpdate', ['as' => 'passportCreateOrUpdate', 'uses' => 'PassportController@createOrUpdate']);
		});

        Route::get('/admin/auth/{user_id}', ['middleware' => 'checkRole:admin', 'uses' => 'AdminController@authByUserId']);
	});
});

Route::any('/system/get/captcha_img', ['as' => 'getCaptchaImg', function() {
	return captcha_src('rosnok');
}]);

Route::group(['prefix' => 'upload'], function() {
	Route::post('/passportScans', ['as' => 'passportScansUpload', 'uses' => 'UploadController@passportScans']);

	Route::post('/inboxDoc', ['as' => 'inboxDocsUpload', 'uses' => 'UploadController@inboxDoc']);
	Route::post('/sentDoc', ['as' => 'sentDocsUpload', 'uses' => 'UploadController@sentDoc']);
	Route::post('/deleleSentDoc', ['as' => 'sentDocsDelete', 'uses' => 'UploadController@deleteSentDoc']);
});

// в конце чтобы не перекрывать другие ссылки
Route::get('/{agentName}', ['uses' => 'MainController@index']);
Route::post('/landing/sendRequest', ['as' => 'landingSendRequest', 'uses' => 'MainController@sendLandingRequest']);