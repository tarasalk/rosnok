<?php

namespace App\Http\Controllers;

use App\Http\Requests;

class DebugController extends Controller
{
    public function getTemplate($name) {
        if (view()->exists('debug.'.$name))
            return view('debug.'.$name);
        else
            abort(404);
    }
}
