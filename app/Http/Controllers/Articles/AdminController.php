<?php

namespace App\Http\Controllers\Articles;

use App\Helpers\JsonWrapper;
use App\Helpers\MainHelper;
use App\Models\Articles;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

/**
 * Админка статей
 */
class AdminController extends Controller {
	const PATH_ARTICLE_IMG = 'upload/articles/';

	public function viewMain() {
		$aArticles = Articles::orderBy('created_at', 'desc')->get();
		return view('cabinet.role.user.articlesMain', ['aArticles' => $aArticles]);
	}

	public function createArticle(Request $request) {
		$oArticle = Articles::create($request->all());

		if ($oArticle)
			return JsonWrapper::success();
		else
			return JsonWrapper::error('undefined_error');
	}

	public function editArticle(Request $request) {
		$oArticle = Articles::find($request->input('id'));

		if ($oArticle) {
			$oArticle->update($request->all());
			return JsonWrapper::success();
		}

		return JsonWrapper::error('undefined_error');
	}

	public function imgUpload(Request $request) {
		if ($request->hasFile('imgSmall'))
			$requestFileName = 'imgSmall';
		elseif ($request->hasFile('imgBig'))
			$requestFileName = 'imgBig';
		else
			return JsonWrapper::error('undefined_file');

		$file = $request->file($requestFileName);

		if (!$file->isValid())
			return JsonWrapper::error('not_valid');

		$normalMimeType = [
			'jpg' => 'image/jpeg',
			'png' => 'image/png',
			'gif' => 'image/gif'
		];

		if (!in_array($file->getMimeType(), $normalMimeType))
			return JsonWrapper::error('undefined_mime');

		$fileName = MainHelper::getRandomFileName($file);
		$file->move(self::PATH_ARTICLE_IMG, $fileName);

		return JsonWrapper::success(['response' => ['title' => $fileName, 'path' => self::PATH_ARTICLE_IMG.$fileName]]);
	}
}
