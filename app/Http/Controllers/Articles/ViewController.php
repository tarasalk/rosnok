<?php

namespace App\Http\Controllers\Articles;

use App\Models\Articles;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

/**
 * Просмотр статей на главной
 */
class ViewController extends Controller {
	/**
	 * показать все статьи по типу
	 */
	public function viewByType(Request $request) {
		$articleInfo = $this->getArticleInfoByRequest($request);
		if (is_null($articleInfo)) return redirect()->route('main');

		$aArticle = Articles::where('type', $articleInfo->type)->orderBy('created_at', 'desc')->get();

		return view('articles.main', ['aArticle' => $aArticle, 'articleInfo' => $articleInfo]);
	}

	/**
	 * показать статью по имени
	 */
	public function viewByName(Request $request, $name) {
		$articleInfo = $this->getArticleInfoByRequest($request);
		if (is_null($articleInfo)) return redirect()->route('main');

		$oArticle = Articles::whereRaw('type = ? && name = ?', [$articleInfo->type, $name])->first();

		if (is_null($oArticle))
			redirect()->route('main');

		return view('articles.article', ['oArticle' => $oArticle, 'articleInfo' => $articleInfo]);
	}

	/**
	 * вспомогательная инфа, определение по url
	 */
	private function getArticleInfoByRequest(Request $request) {
		switch ($request->segment(1)) {
			case 'itogi':
				$type = Articles::TYPE_SUD;
				$title = 'С.У.Д — СБОРНИК УСПЕШНЫХ ДЕЛ';
				$routeReadMore = 'articleSud';
				break;

			case 'news':
				$type = Articles::TYPE_NEWS;
				$title = 'Новости и аналитика';
				$routeReadMore = 'articleNews';
				break;

			default:
				return null;
		}

		return (object) ['type' => $type, 'title' => $title, 'routeReadMore' => $routeReadMore];
	}
}
