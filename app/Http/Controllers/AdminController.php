<?php

namespace App\Http\Controllers;

use App\Helpers\JsonWrapper;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;

class AdminController extends Controller
{
    public function authByUserId($user_id) {
        if (is_numeric($user_id))
            Auth::loginUsingId($user_id, true);

        return redirect()->route('main');
    }
}
