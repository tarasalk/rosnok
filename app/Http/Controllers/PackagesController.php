<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PackagesController extends Controller
{
    public function main(Request $request) {
		return view('cabinet.common.packages.main');
	}
}
