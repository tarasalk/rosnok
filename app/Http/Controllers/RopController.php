<?php

namespace App\Http\Controllers;

use App\Models\Offices;
use App\Models\UserRole;
use App\Http\Requests;

class RopController extends Controller
{
    public function agents() {
		$agents = UserRole::where('agent', '=', 1)->get();
		return view('rop.agents', ['aAgents' => $agents]);
	}

	public function offices() {
		$aOffices = Offices::all();
		return view('rop.offices', ['aOffices' => $aOffices]);
	}
}
