<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\UserRole;

class ProfileController extends Controller
{
    public function desktop() {
        return view("cabinet.role.user.desktop");
	}

    public function viewProfile($user_id) {

        return view("cabinet.role.user.profile");
    }
}
