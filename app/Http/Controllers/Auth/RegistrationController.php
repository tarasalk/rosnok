<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\JsonWrapper;
use App\Models\Api\People;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class RegistrationController extends Controller
{
	/**
	 * принудительная регистрация, например когда человек пытался авторизоваться через ВК
	 */
	protected function start(Request $request) {
        if ($request->has('vk_id'))
			session(['registrationVkId' => $request->input('vk_id')]);

		return view('main.main', [
            'need_register' => [
                'first_name' => $request->input('first_name', ""),
                'last_name' =>$request->input('last_name', "")
            ],
			'setOgp' => false,
			'seeAuth' => true
        ]);
    }

    protected function sendPhoneCode(Request $request, People $peopleApi) {
        $validator = Validator::make($request->all(), [
            'captcha' => 'required|captcha',
            'country' => 'required',
            'phone' => 'required',
            'first_name' => 'required',
            'last_name' => 'required'
        ]);

        if ($validator->fails())
            return JsonWrapper::validation($validator->messages());

        $response = $peopleApi->registrationCreate(
            $request->input('country'),
            $request->input('phone'),
            $request->input('first_name'),
            $request->input('last_name'),
            session('registrationVkId', '')
        );

        if ($response) {
            User::create(['id' => $response->user_id]);
            return JsonWrapper::success();
        }

        return JsonWrapper::error($peopleApi->getLastError());
    }

    protected function confirmPhoneCode(Request $request, People $peopleApi) {
        $validator = Validator::make($request->all(), [
            'country' => 'required',
            'phone' => 'required',
            'code' => 'required'
        ]);

        if ($validator->fails())
            return JsonWrapper::error($validator->messages());

        $response = $peopleApi->confirmPhone(
            $request->input('country'),
            $request->input('phone'),
            $request->input('code')
        );

        if ($response) {
            Auth::loginUsingId($response->user_id, true);
            return JsonWrapper::success();
        }
        else
            return JsonWrapper::error($peopleApi->getLastError());
    }

	protected function recoveryPassword(Request $request, People $peopleApi) {
		$validator = Validator::make($request->all(), [
			'captcha' => 'required|captcha',
			'country' => 'required',
			'phone' => 'required'
		]);

		if ($validator->fails())
			return JsonWrapper::validation($validator->messages());

        $response = $peopleApi->recoveryPassword(
            $request->input('country'),
            $request->input('phone')
        );

		if ($response)
			return JsonWrapper::success();
		else
			return JsonWrapper::error($peopleApi->getLastError());
	}
}
