<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\CurlWrapper;
use App\Helpers\JsonWrapper;
use App\Models\Api\People;
use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    protected function authVk() {
        return redirect(env('URL_PEOPLE').'/auth/vk/?redirect='.route('authVkCallback'));
    }

	protected function authVkCallback(Request $request) {
		$auth = $request->input('auth', false);
		$user_id = $request->input('user_id', false);

		if ($auth && is_numeric($user_id))
			Auth::loginUsingId($user_id, true);
		else {
            if ($request->input('error', false) == 'user_not_found') {
                return redirect()->route('registrationStart', [
                    'first_name' => $request->input('first_name'),
                    'last_name' => $request->input('last_name'),
                    'vk_id' => $request->input('vk_id')
                ]);
            }
        }

		return redirect()->route('main');
	}

	protected function authPassword(Request $request, People $peopleApi) {
		$validator = Validator::make(Input::all(), [
			'country' => 'required',
			'phone' => 'required',
			'password' => 'required'
		]);

		if ($validator->fails())
			return JsonWrapper::validation($validator->messages());

        $response = $peopleApi->userSearch(
            $request->input('country'),
            $request->input('phone'),
            $request->input('password')
        );
		if ($response) {
			$user_id = $response->users['0'];
			if (is_numeric($user_id)) {
				Auth::loginUsingId($user_id, true);
				return JsonWrapper::success();
			}
			else
				return JsonWrapper::error('undefined_error');
		}
        else return JsonWrapper::error($peopleApi->getLastError());
	}

	public function logout() {
		Auth::logout();
		return redirect(env('URL_PEOPLE').'/auth/logout?redirect='.route('main'));
	}

	protected function pageAuth() {
		return "пожалуйста авторизуйтесь";
	}
}