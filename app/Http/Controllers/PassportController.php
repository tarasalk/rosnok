<?php

namespace App\Http\Controllers;

use App\Helpers\JsonWrapper;
use App\Http\Requests;
use App\Models\Api\People;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class PassportController extends Controller
{
    protected function createOrUpdate(Request $request, People $peopleApi) {
		// защита от потери сканов, если они не указаны
		$data = $request->all();
		$this->unsetEmpty($data, 'pass_scan1');
		$this->unsetEmpty($data, 'pass_scan2');
		$this->unsetEmpty($data, 'proxy_scan1');
		$this->unsetEmpty($data, 'proxy_scan2');

		// TODO возможно редактирование чужих данных
		$user_id = Auth::user()->id;

        $response = $peopleApi->createOrEditPassportData($user_id, $data);
		if ($response)
			return JsonWrapper::success();
		else
			return JsonWrapper::error($peopleApi->getLastError());
	}

	public function getData(People $peopleApi) {
		// TODO можно смотреть чужие данные
		$user_id = Auth::user()->id;

        $response = $peopleApi->getPassportData($user_id);
		if ($response)
			return view('cabinet.common.passportEdit', ['passportData' => $response->data]);

		return view('cabinet.common.passportEdit');
	}

	/**
	 * удаляем из массива пустые элементы
	 * @param $data
	 * @param $name
	 */
	private function unsetEmpty(&$data, $name) {
		if (array_key_exists($name, $data) && $data[$name] == '')
			unset($data[$name]);
	}
}
