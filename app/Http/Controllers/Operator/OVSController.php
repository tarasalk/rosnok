<?php

namespace App\Http\Controllers\Operator;

use App\Helpers\JsonWrapper;
use App\Models\InboxDocEvents;
use App\Models\InboxDocs;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\OVSInboxDocRepository as DocRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

// оператор входящей связи
class OVSController extends Controller
{
    public function viewInboxDocs($category = 'new') {
        $aDocs = DocRepository::getDocsByStatus($category);
        $aDocsCategoriesCount = DocRepository::getDocsCategoriesCount();

        return view('cabinet.role.ovs.inboxDocs', [
            'aDocs' => $aDocs,
            'aDocsCategoriesCount' => (object) $aDocsCategoriesCount
        ]);
    }

    /*
     * Проверили док. Выставили статус.
     */
    public function docChecked(Request $request) {
        $doc_id = $request->input('doc_id');
        $name = $request->input('name');
        $notation = $request->input('notation');
        $status= $request->input('status');

        $oDoc = InboxDocs::where('id', $doc_id)->first();
        if ($oDoc) {
            $oDoc->name = $name;
            $oDoc->status = $status;

            if ($oDoc->save()) {
                switch ($status) {
                    case InboxDocs::status_accepted:
                        $event_type_id = 2;
                        break;

                    case InboxDocs::status_rebuke:
                        $event_type_id = 3;
                        break;

                    case InboxDocs::status_denied:
                        $event_type_id = 4;
                        break;

                    default:
                        $event_type_id = -1;
                }

                InboxDocEvents::create([
                    'doc_id' => $doc_id,
                    'event_type_id' => $event_type_id,
                    'notation' => $notation,
                    'initiator_id' => Auth::user()->id
                ]);

                return JsonWrapper::success();
            }

        }

        return JsonWrapper::error();
    }

    public function sendLawyer(Request $request) {
        $doc_id = $request->input('doc_id');

        $oDoc = InboxDocEvents::create([
            'event_type_id' => 5,
            'initiator_id' => Auth::user()->id,
            'doc_id' => $doc_id
        ]);

        if ($oDoc)
            return JsonWrapper::success();

        return JsonWrapper::error();
    }

    public function getInboxDocById(Request $request) {
        $doc_id = $request->input('doc_id');

        $oDoc = DocRepository::getDocById($doc_id);
        if ($oDoc)
            return JsonWrapper::success(compact('oDoc'));

        return JsonWrapper::error('doc_not_found');
    }
}
