<?php

namespace App\Http\Controllers\Operator;

use App\Helpers\JsonWrapper;
use App\Models\SentDocs;
use App\Repositories\OISUSentDocRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

// оператор исходящий связи юридический
class OISUController extends Controller
{
    public function viewSentDocs() {
        $aDocs = OISUSentDocRepository::getDocs();

        return view('cabinet.role.oisu.sentDocs', compact('aDocs'));
    }

    public function editSentDoc(Request $request) {
        $doc_id = $request->input('doc_id');
        $recipient = $request->input('recipient');
        $recipientAddress = $request->input('recipient_address');
        $name = $request->input('name');

        $oDoc = SentDocs::find($doc_id);
        if ($oDoc) {
            $oDoc->recipient = $recipient;
            $oDoc->recipient_address = $recipientAddress;
            $oDoc->name = $name;

            if ($oDoc->save())
                return JsonWrapper::success();
        }

        return JsonWrapper::error();
    }

    public function handSentDoc(Request $request) {
        $doc_id = $request->input('doc_id');

        $oDoc = SentDocs::find($doc_id);
        if ($oDoc) {
            $oDoc->oisu_handed_date = date('Y-m-d H:i:s');

            if ($oDoc->save())
                return JsonWrapper::success(['date' => $oDoc->oisu_handed_date]);
        }

        return JsonWrapper::error();
    }
}
