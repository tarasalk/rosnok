<?php

namespace App\Http\Controllers\Operator;

use App\Repositories\OISSSentDocRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

// оператор исходящий связи шаблонный
class OISSController extends Controller
{
    public function viewSentDocs() {
        $aDocs = OISSSentDocRepository::getDocs();

        return view('cabinet.role.oiss.sentDocs', compact('aDocs'));
    }

    public function viewPrintSentDocs() {
        $aDocs = OISSSentDocRepository::getPrintSentDocs();

        return view('cabinet.role.oiss.printSentDocs', compact('aDocs'));
    }
}
