<?php

namespace App\Http\Controllers;

use App\Helpers\JsonWrapper;
use App\Models\Client;
use App\Models\InboxDocEvents;
use App\Models\SentDocs;
use App\Repositories\LawyerInboxDocRepository as InboxDocRepository;
use App\Repositories\LawyerSentDocRepository as SentDocRepository;
use App\Repositories\LawyerClientsRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LawyerController extends Controller
{
	public function viewInboxDocs($client_id = null) {
        $aDocs = InboxDocRepository::getDocs($client_id);

		return view('cabinet.role.lawyer.inboxDocs', compact('aDocs'));
	}

    public function lawyerRead(Request $request) {
        $doc_id = $request->input('doc_id');

        $oDoc = InboxDocEvents::create([
            'event_type_id' => 6,
            'initiator_id' => Auth::user()->id,
            'doc_id' => $doc_id
        ]);

        if ($oDoc)
            return JsonWrapper::success();

        return JsonWrapper::error();
    }

    public function viewClients($filter = 'all') {
        $aClients = LawyerClientsRepository::getPageClientsByCategory($filter);
        $aClientsCategoriesCount = Client::getClientsCategoriesCount();

        return view('cabinet.role.lawyer.clients', compact('aClients', 'aClientsCategoriesCount'));
    }

    public function viewSentDocsAdd() {
        return view('cabinet.role.lawyer.sentDocs.add');
    }

    public function searchClients(Request $request) {
        $searchFio = $request->input('searchFio');

        $aClients = LawyerClientsRepository::searchClients($searchFio);

        return JsonWrapper::success(compact('aClients'));
    }

    public function getClientSentDocs($client_id) {
        return ['data' => SentDocRepository::getDocs($client_id)];
    }

    public function viewSentDocs() {
        // костыль от 11к+ доков, иначе Allowed memory size...
        ini_set('memory_limit', "512M");

        $aDocs = SentDocRepository::getDocs();

        return view('cabinet.role.lawyer.sentDocs.sentDocs', compact('aDocs'));
    }

    public function sentDocsAdd(Request $request) {
        $oDoc = new SentDocs();
        $oDoc->client_id = $request->input('client_id');
        $oDoc->sender_id = Auth::user()->id;
        $oDoc->sender_notation = $request->input('notation');
        $oDoc->file_path = $request->input('file');

        if ($oDoc->save())
            return JsonWrapper::success();
        else
            return JsonWrapper::error(['undefined_error']);
    }
}
