<?php

namespace App\Http\Controllers;

use App\Helpers\JsonWrapper;
use App\Http\Requests;
use App\Models\Api\Cenomarket;
use App\Models\LandingRequest;
use Illuminate\Http\Request;

class MainController extends Controller {
    public function index($agentSubdomain = null, $material_id = null) {
		$ogp = $this->getOgpInfo($material_id);

        return view('main.main', [
			'ogp' => $ogp->ogp,
			'needOgp' => $ogp->needOgp,
            'agentSubdomain' => $agentSubdomain
		]);
	}

	/**
	 * возвращает текст от агента, никаких заявок не создается
	 * @param Request $request
	 * @return array
	 */
	public function sendLandingRequest(Request $request, Cenomarket $cenomarketApi) {
        $name = $request->input('name', '');
        $phone = $request->input('phone');
        $agentSubdomain = $request->input('agentSubdomain');
        $checkRequestExists = $request->input('checkRequestExists');

        if (empty($phone))
            return JsonWrapper::error('wrong_phone');
        
        if ($checkRequestExists == 'true') {
            $oRequest = LandingRequest::getRequestByPhone($phone, date('Y-m-d H:i:s', strtotime("-1 day")));
            if (isset($oRequest))
                return JsonWrapper::error('request_exists');
        }

        $response = $cenomarketApi->getUserIdBySubdomain($agentSubdomain);
        if ($response) {
            $agent_id = $response;
        }
        else {
            $agent_id = 92542;
        }

        if (LandingRequest::addNewRequest($name, $phone, $agent_id)) {
            $agentCallbackMessage = LandingRequest::getAgentCallbackMessage($agent_id);
            return JsonWrapper::success(compact('agentCallbackMessage'));
        }

        return JsonWrapper::error('undefined_error');
	}

    private function getOgpInfo($material_id) {
        $rosnok_link = 'http://' . $_SERVER['HTTP_HOST'] . strtok($_SERVER["REQUEST_URI"],'?');

        $needOgp = true;
        switch ($material_id) {
            case '1':
                $ogpTitle = 'Вы взяли кредит в банке - а платить нечем?';
                $ogpImage = '/img/refProg/rosnok1.jpg';
                $ogpDescription = "Начались просрочки и звонки от коллекторов? Угрожают со всех сторон? СТОП! Есть законный выход. Помощь опытных юристов, консультация бесплатно. Не тяните, начните решать кредитные проблемы прямо сейчас. Зайдите на сайт $rosnok_link и получите помощь кредитного юриста. Остановим рост долга, прекратим звонки с угрозами, подведем долги к законному списанию.";
                break;

            case '2':
                $ogpTitle = 'Нечем платить за кредит? А вы знали, что кредитные долги можно списать по закону?';
                $ogpImage = '/img/refProg/rosnok2.jpg';
                $ogpDescription = "Хватить бояться банков и коллекторов - получите бесплатную консультацию юриста. И начните жить спокойно! Подробнее здесь: $rosnok_link";
                break;

            case '3':
                $ogpTitle = 'БЕСПЛАТНАЯ ЮРИДИЧЕСКАЯ КОНСУЛЬТАЦИЯ АНТИ-КОЛЛЕКТОРА';
                $ogpImage = '/img/refProg/roslike_2_001.jpg';
                $ogpDescription = "Ваше спасение от кредитных долгов. Законно, надежно, безопасно!";
                break;

            case '4':
                $ogpTitle = 'БЕСПЛАТНАЯ ЮРИДИЧЕСКАЯ КОНСУЛЬТАЦИЯ АНТИ-КОЛЛЕКТОРА';
                $ogpImage = '/img/refProg/roslike_2_002.jpg';
                $ogpDescription = "Ваше спасение от кредитных долгов. Законно, надежно, безопасно!";
                break;

            case '5':
                $ogpTitle = 'БЕСПЛАТНАЯ ЮРИДИЧЕСКАЯ КОНСУЛЬТАЦИЯ АНТИ-КОЛЛЕКТОРА';
                $ogpImage = '/img/refProg/roslike_2_003.jpg';
                $ogpDescription = "Ваше спасение от кредитных долгов. Законно, надежно, безопасно!";
                break;

            case '6':
                $ogpTitle = 'БЕСПЛАТНАЯ ЮРИДИЧЕСКАЯ КОНСУЛЬТАЦИЯ АНТИ-КОЛЛЕКТОРА';
                $ogpImage = '/img/refProg/roslike_2_004.jpg';
                $ogpDescription = "Ваше спасение от кредитных долгов. Законно, надежно, безопасно!";
                break;

            default:
                $ogpTitle = '';
                $ogpImage = '';
                $ogpDescription = '';
                $needOgp = false;
        }

        return (object) [
            'ogp' => (object) [
                'title' => $ogpTitle,
                'image' => $ogpImage,
                'description' => $ogpDescription,
            ],
            'needOgp' => $needOgp,
        ];
    }
}
