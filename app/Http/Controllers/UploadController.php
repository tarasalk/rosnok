<?php

namespace App\Http\Controllers;

use App\Helpers\CurlWrapper;
use App\Helpers\JsonWrapper;
use App\Helpers\MainHelper;
use App\Models\Api\People;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class UploadController extends Controller {
	const PATH_INBOX_DOC = 'upload/inboxDocs/';
	const PATH_SENT_DOC = 'upload/sentDocs/';

	/**
	 * отправа сканов на People
	 * @param Request $request
	 * @return array
	 */
	protected function passportScans(Request $request, People $peopleApi) {
		if ($request->hasFile('pass_scan1'))
			$requestFileName = 'pass_scan1';
		elseif ($request->hasFile('pass_scan2'))
			$requestFileName = 'pass_scan2';
		elseif ($request->hasFile('proxy_scan1'))
			$requestFileName = 'proxy_scan1';
		elseif ($request->hasFile('proxy_scan2'))
			$requestFileName = 'proxy_scan2';
		else
			return JsonWrapper::error('undefined_scan');

		$sendFile = $request->file($requestFileName);

        $response = $peopleApi->uploadPassportScan($sendFile, $requestFileName);
        if ($response->status == 'ok')
            return JsonWrapper::success(['response' => $response->response]);

        return JsonWrapper::error();
	}

	public function inboxDoc(Request $request) {
		if (!$request->hasFile('file'))
			return JsonWrapper::error('undefined_file');

		$file = $request->file('file');

		if (!$file->isValid())
			return JsonWrapper::error('not_valid');

		$normalMimeType = [
			'jpg' => 'image/jpeg',
			'png' => 'image/png',
			'gif' => 'image/gif',
			'pdf' => 'application/pdf',
			'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
			'docx_google' => 'application/vnd.google-apps.document',
			'zip' => 'application/zip',
			'doc' => 'application/msword'
		];

		if (!in_array($file->getMimeType(), $normalMimeType))
			return JsonWrapper::error('undefined_mime');

		$fileName = MainHelper::getRandomFileName($file);
		$file->move(self::PATH_INBOX_DOC, $fileName);

        return $this->success($fileName, self::PATH_INBOX_DOC.$fileName);
	}

    public function sentDoc(Request $request) {
        if (!$request->hasFile('file'))
            return JsonWrapper::error('undefined_file');

        $file = $request->file('file');

        if (!$file->isValid())
            return JsonWrapper::error('not_valid');

        $normalMimeType = [
            'jpg' => 'image/jpeg',
            'png' => 'image/png',
            'gif' => 'image/gif',
            'pdf' => 'application/pdf',
            'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'docx_google' => 'application/vnd.google-apps.document',
            'zip' => 'application/zip',
            'doc' => 'application/msword'
        ];

        if (!in_array($file->getMimeType(), $normalMimeType))
            return JsonWrapper::error('undefined_mime');

        $fileName = MainHelper::getRandomFileName($file);
        $file->move(self::PATH_SENT_DOC, $fileName);

        return $this->success($fileName, self::PATH_SENT_DOC.$fileName);
    }

    public function deleteSentDoc(Request $request) {
        $doc_name = $request->input('doc_name');

        if (empty($doc_name))
            return JsonWrapper::error('empty_doc_name');

        if (@unlink(self::PATH_SENT_DOC.$doc_name))
            return JsonWrapper::success();
        else
            return JsonWrapper::error();
    }

    private function success($fileName, $path) {
        return JsonWrapper::success(['response' => ['title' => $fileName, 'path' => '/'.$path]]);
    }
}
