<?php

namespace App\Http\Controllers\Agent;

use App\Helpers\JsonWrapper;
use App\Http\Controllers\Controller;
use App\Models\Api\People;
use App\Models\Client;
use App\Http\Requests;
use App\Models\InboxDocs;
use App\Models\LandingRequest;
use App\Repositories\AgentClientsRepository;
use App\Repositories\AgentInboxDocRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MainController extends Controller
{
	/**
	 * список клиентов агента
	 */
	public function clients($category = 'all') {
        $agent_id = Auth::user()->id;

        $aClients = AgentClientsRepository::getClientsByCategory($category, $agent_id);
        $aClientsCategoriesCount = Client::getClientsCategoriesCount($agent_id);

		return view('cabinet.role.agent.clients', compact('aClients', 'aClientsCategoriesCount'));
	}

	public function viewInboxDocs(People $peopleApi) {
        $agent_id = Auth::user()->id;
        $aDocs = AgentInboxDocRepository::getDocs($agent_id);

        $client_ids = Client::where('agent_id', $agent_id)->pluck('client_id')->toArray();
        $response = $peopleApi->userGet($client_ids);
        $aClients = $response ? $response : [];

		return view('cabinet.role.agent.inboxDocs', compact('aDocs', 'aClients'));
	}

	/**
     * TODO validation
	 * ajax добавление документа от клиента
	 */
	public function inboxDocsAdd(Request $request) {
        $client_id = $request->input('client_id');
        $agent_id = Auth::user()->id;
        $agent_notation = $request->input('notation');
        $file_path = $request->input('file');

		return AgentInboxDocRepository::add($client_id, $agent_id, $agent_notation, $file_path);
	}

	public function landingRequest() {
		$agent_id = Auth::user()->id;
		$aNewRequests = LandingRequest::getNewRequests($agent_id);
		$aProcessedRequests = LandingRequest::getProcessedRequests($agent_id);

		return view('cabinet.role.agent.landingRequests', [
			'aNewRequests' => $aNewRequests,
			'aProcessedRequests' => $aProcessedRequests
		]);
	}

	public function confirmProcessRequest(Request $request) {
		$request_id = $request->input('request_id');
		$client_type = $request->input('client_type');
		$comment = $request->input('comment');

		$oLandingRequest = LandingRequest::find($request_id);
		$oLandingRequest->client_type = $client_type;
		$oLandingRequest->comment = $comment;
		$oLandingRequest->processed_date = date('Y-m-d H:i:s');

		if ($oLandingRequest->save())
			return JsonWrapper::success();
		else
			return JsonWrapper::error('undefined_error');
	}

	public function clientCab($client_id) {
		return view('cabinet.role.client.main', ['client_id' => $client_id]);
	}
}
