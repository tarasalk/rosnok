<?php

namespace App\Http\Controllers\agent;

use App\Helpers\JsonWrapper;
use App\Models\Api\People;
use App\Models\Client;
use App\Models\JournalClient;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

class JournalController extends Controller {

    public function viewCold() {
        $agent_id = Auth::user()->id;
        $aJournalClients = JournalClient::whereRaw('agent_id = ? && status = ?', [$agent_id, JournalClient::status_cold])->get();

        return view('cabinet.role.agent.journal.cold', ['aJournalClients' => $aJournalClients]);
    }

    public function viewWarm() {
        $agent_id = Auth::user()->id;
        $aJournalClients = JournalClient::whereRaw('agent_id = ? && status = ?', [$agent_id, JournalClient::status_warm])->get();

        return view('cabinet.role.agent.journal.warm', ['aJournalClients' => $aJournalClients]);
    }

    public function viewHot(People $peopleApi) {
        $agent_id = Auth::user()->id;
        $aHotClients = Client::whereRaw('agent_id = ? && pay_status = ?', [$agent_id, 0])->get();

        $client_ids = [];
        foreach($aHotClients as $aClient) $client_ids[] = $aClient['client_id'];

        $response = $peopleApi->userGet($client_ids);
        if ($response) {
            $aClientsPeople = $response;
        }
        else {
            $aHotClients = [];
            $aClientsPeople = [];
        }

        return view('cabinet.role.agent.journal.hot', [
            'aHotClients' => $aHotClients,
            'aClientsPeople' => $aClientsPeople
        ]);
    }

    public function viewReal(People $peopleApi) {
        $agent_id = Auth::user()->id;
        $aRealClients = Client::whereRaw('agent_id = ? && pay_status = ?', [$agent_id, 1])->get();

        $client_ids = [];
        foreach($aRealClients as $aClient) $client_ids[] = $aClient['client_id'];

        $response = $peopleApi->userGet($client_ids);
        if ($response) {
            $aClientsPeople = $response;
        }
        else {
            $aRealClients = [];
            $aClientsPeople = [];
        }

        return view('cabinet.role.agent.journal.real', [
            'aHotClients' => $aRealClients,
            'aClientsPeople' => $aClientsPeople
        ]);
    }

    public function addClient(Request $request) {
        $name = $request->input('name');
        $phone = $request->input('phone');
        $notation = $request->input('notation');
        $status = $request->input('status');

        $agent_id = Auth::user()->id;

        $oJournalClient = JournalClient::create([
            'name' => $name,
            'phone' => $phone,
            'notation' => $notation,
            'status' => $status,
            'agent_id' => $agent_id
        ]);

        if ($oJournalClient)
            return JsonWrapper::success();


        return JsonWrapper::error();
    }

    public function editClient(Request $request) {
        $id = $request->input('id');
        $name = $request->input('name');
        $phone = $request->input('phone');
        $notation = $request->input('notation');
        $status = $request->input('status');

        $oJournalClient = JournalClient::where('id', $id)->first();
        if ($oJournalClient) {
            $oJournalClient->name = $name;
            $oJournalClient->phone = $phone;
            $oJournalClient->notation = $notation;
            $oJournalClient->status = $status;

            $oJournalClient->save();

            return JsonWrapper::success();
        }

        return JsonWrapper::error();
    }
}
