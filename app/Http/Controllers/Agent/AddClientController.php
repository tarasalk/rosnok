<?php

namespace App\Http\Controllers\Agent;

use App\Helpers\ArrayMerge;
use App\Helpers\JsonWrapper;
use App\Models\Api\People;
use App\Models\Client;
use App\Models\Credit;
use App\Models\CreditTaskList;
use App\Models\Offices;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AddClientController extends Controller {
	const ROLE_ID_CLIENT = 2;

	public function viewMain() {
		return view('cabinet.role.agent.clientAdd.main');
	}

	public function userFind(Request $request, People $peopleApi) {
		$phone = $request->input('phone');

		$response = $peopleApi->userSearch(7, $phone);
		if ($response) {
			$users = $response->users;
			if (count($users) > 0) {
				$client_id = $users[0];

                $response = $peopleApi->userGet($client_id, ['avatar']);
                if ($response) {
                    $oUser = $response;
                    if (isset($oUser->roles->{self::ROLE_ID_CLIENT}))
                        return JsonWrapper::success(['message' => 'user_already_client', 'user' => $oUser]);

                    return JsonWrapper::success(['message' => 'user_found', 'user' => $oUser]);
                }
			}
			else return JsonWrapper::success(['message' => 'user_not_found', 'phone' => $phone]);
		}

        return JsonWrapper::error($peopleApi->getLastError());
	}

	/**
	 * сделать юзера клиентом
	 */
	public function makeUserClient(Request $request, People $peopleApi) {
		$user_id = $request->input('user_id');

		$response = $peopleApi->setRole($user_id, self::ROLE_ID_CLIENT);
		if ($response) {
			$oClient = Client::create([
				'client_id' => $user_id,
				'agent_id' => Auth::user()->id
			]);

			$response = $peopleApi->userGet($user_id);
            if ($response) {
                $oUser = $response;
                return JsonWrapper::success(['user' => $oUser]);
            }
		}

		return JsonWrapper::error($peopleApi->getLastError());
	}

	/**
	 * создание аккаунта клиента
	 */
	public function clientCreate(Request $request, People $peopleApi) {
		$validator = Validator::make($request->all(), [
			'phone' => 'required',
			'first_name' => 'required',
			'last_name' => 'required'
		]);

		if ($validator->fails())
			return JsonWrapper::validation($validator->messages());

		$response = $peopleApi->registrationCreate(
			7,
			$request->input('phone'),
			$request->input('first_name'),
			$request->input('last_name')
		);
		if ($response) {
			$client_id = $response->user_id;

			$response = $peopleApi->setRole($client_id, self::ROLE_ID_CLIENT);
			if ($response) {
				User::create(['id' => $client_id]);
				Client::create(['client_id' => $client_id, 'agent_id' => Auth::user()->id]);

				return JsonWrapper::success(['client_id' => $client_id]);
			}
		}

        return JsonWrapper::error($peopleApi->getLastError());
	}

	public function sendCodeConfirmPhone(Request $request, People $peopleApi) {
		$phone = $request->input('phone');

		$response = $peopleApi->sendCodeConfirmPhone(7, $phone);
		if ($response)
			return JsonWrapper::success(['phone' => $phone]);

        return JsonWrapper::error($peopleApi->getLastError());
	}

	public function clientConfirmPhone(Request $request, People $peopleApi) {
		$phone = $request->input('phone');
		$code = $request->input('code');

		$validator = Validator::make($request->all(), [
			'phone' => 'required',
			'code' => 'required'
		]);

		if ($validator->fails())
			return JsonWrapper::validation($validator->messages());

		$response = $peopleApi->confirmPhone(7, $phone, $code);
		if ($response)
			return JsonWrapper::success();

        return JsonWrapper::error($peopleApi->getLastError());
	}

    public function getCreditsView(Request $request) {
        $client_id = $request->input('client_id');

        if (!is_numeric($client_id))
            return JsonWrapper::error('wrong_client_id');

        return view('cabinet.role.client.credits.main', [
            'aTasks' => CreditTaskList::all(),
            'aCredits' => Credit::getAllCreditsByClientId($client_id)
        ]);
    }

    public function calculateRate(Request $request) {
		$client_id = $request->input('client_id');
		if (empty($client_id))
			return JsonWrapper::error('wrong_user_id');

		$rate = Credit::calculateRate($client_id);
		if ($rate != 'undefined_rate')
			return JsonWrapper::success(['rate' => $rate]);
		else
			return JsonWrapper::error('undefined_rate');
    }

    public function confirmRate(Request $request) {
        $client_id = $request->input('client_id');
        $rate = $request->input('rate');
        $social_status = $request->input('social_status');
        $service_live = $request->input('service_live');

        $oClient = Client::where('client_id', $client_id)->first();
        if ($oClient) {
            $oClient->rate_pay = $rate;
            $oClient->social_status = $social_status;
            $oClient->service_live = $service_live;
            $oClient->save();

            return JsonWrapper::success();
        }

        return JsonWrapper::error('undefined_error');
    }

    public function getOffices(People $peopleApi) {
        $offices = Offices::all()->toArray();
		$holder_ids = [];
		foreach($offices as $oOffice) $holder_ids[] = $oOffice['holder_id'];

        $response = $peopleApi->userGet($holder_ids);
		if ($response) {
			$aUsersPeople = $response;

			$aOffices = ArrayMerge::mergeByKey(
                $offices,
                $aUsersPeople,
				'holder_id',
				'user_id'
			);
		}
		else $aOffices = [];
        return JsonWrapper::success(['offices' => $aOffices]);
    }

    public function confirmChoiceOffice(Request $request) {
        $client_id = $request->input('client_id');
        $office_id = $request->input('office_id');

        $oClient = Client::where('client_id', $client_id)->first();
        if ($oClient) {
            $oClient->office_id = $office_id;
            $oClient->save();

            return JsonWrapper::success();
        }

        return JsonWrapper::error('undefined_error');
    }

	public function getPassportView(Request $request, People $peopleApi) {
        $client_id = $request->input('client_id');

		$response = $peopleApi->getPassportData($client_id);
		if ($response && isset($response->data))
			return view('cabinet.common.include.passport', ['passportData' => $response->data]);

		return view('cabinet.common.include.passport');
	}

    public function payRate(Request $request) {
        $client_id = $request->input('client_id');

        $oClient = Client::where('client_id', $client_id)->first();

        if ($oClient) {
            $oClient->pay_status = 1; // типа активирован
            $oClient->save();

            return JsonWrapper::success();
        }

        return JsonWrapper::error('undefined_error');
    }

    public function searchOffices(Request $request) {
        $officeName = $request->input('office_name');

        $aOffices = Offices::where('office_name', 'like', '%'.$officeName.'%')->get();

        return JsonWrapper::success(compact('aOffices'));
    }
}
