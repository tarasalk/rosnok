<?php

namespace App\Http\Controllers\Docs;

use App\Helpers\PhpWordWrapper;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class DownloadController extends Controller
{
    public function getMainDoc($doc_name) {
		$phpWord = new PhpWordWrapper(PhpWordWrapper::doctype_main);

		$testString = "Tarasalk";

		$phpWord->generate($doc_name, array(
			['applicant_full_name', $testString],
			['applicant_name', $testString],
			['birthday', $testString],
			['place_of_birth', $testString],
			['passport_num', $testString],
			['passport_issued', $testString],
			['passport_issued_date', $testString],
			['passport_issued_key', $testString],
			['registration_address', $testString],
			['mobile', $testString],
			['rate_pay', $testString],
			['rate_pay_all', $testString],
			['rate_pay_all_text', $testString],
			['date', $testString],
			['gender_oznakomlen', $testString]
		));

		$filename = "Заявление_" . $testString;
		$phpWord->transferDocument($filename);
	}
}
