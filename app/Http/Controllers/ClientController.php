<?php

namespace App\Http\Controllers;

use App\Helpers\JsonWrapper;
use App\Models\Client;
use App\Models\Credit;
use App\Models\CreditTaskList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClientController extends Controller
{
	public function main() {
		$client_id = Auth::user()->id;

		return view('cabinet.role.client.main', ['client_id' => $client_id]);
	}

	/**
	 * оплата услуг
	 */
	public function payConfirm() {
		$client_id = Auth::user()->id;
		Client::where('client_id', '=', $client_id)->update(['pay_status' => Client::PAY_STATUS_PAY]);

		return response()->json(['success' => true]);
	}

	public function viewPayRate() {
		return view('cabinet.role.client.payRate');
	}

	/*
	 * Кредиты
	 */

	public function viewCredits() {
		return view('cabinet.role.client.credits.mainContainer', [
			'aTasks' => CreditTaskList::all(),
			'aCredits' => Credit::getAllCreditsByClientId(Auth::user()->id),
			'role' => Auth::user()->getRoles()
		]);
	}

	public function detailsCredit(Request $request) {
		$type = $request->type;
		$credit_id = $request->id;

		return view('cabinet.role.client.credits.details', [
			'aCredit' => Credit::getCreditInfo($type, $credit_id),
			'type' => $type
		]);
	}

	public function editCreditView(Request $request) {
		$type = $request->type;
		$credit_id = $request->id;

		return view('cabinet.role.client.credits.edit', [
			'aTasks' => CreditTaskList::all(),
			'aCredit' => Credit::getCreditInfo($type, $credit_id),
			'type' => $type
		]);
	}

	public function addCredit(Request $request) {
		// TODO может быть и другой юзер
		$client_id = Auth::user()->id;
		$type = $request->segment(4);

		$result = Credit::addCredit($client_id, $request->all(), $type);

		if ($result['success']) return JsonWrapper::success();
		else return JsonWrapper::error($result['error']);
	}

	public function editCredit(Request $request) {
		$type = $request->segment(4);
		$credit_id = $request->input('credit_id');

		$result = Credit::editCredit($request->all(), $type, $credit_id);

		if ($result['success']) return JsonWrapper::success();
		else return JsonWrapper::error($result['error']);
	}
}