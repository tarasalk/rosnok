<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class CheckAuth
{
    // авторизовывает чувака если это возможно
    public function handle($request, Closure $next)
    {
        if (Auth::check() || $this->isAnonymous() || $request->ajax())
            return $next($request);

        $error = $request->input('error', false);
        if ($error == 'user_not_found') {
            $this->setAnonymous();
            return redirect($request->url());
        }

        $user_id = $request->input('auth_user_id');
        if (isset($user_id)) {
            $oUser = User::where('id', $user_id)->first();
            if (!isset($oUser))
                $oUser = User::create(['id' => $user_id]);

			Auth::loginUsingId($oUser->id);

			return redirect($request->url());
        }
        else {
            return redirect(env('URL_PEOPLE').'/auth/check?redirect='.$request->url());
        }
    }

    private function isAnonymous() {
        return isset($_COOKIE['auth']) && $_COOKIE['auth'] == 'anonymous';
    }

    private function setAnonymous() {
        setcookie('auth', 'anonymous', time() + 600);
    }
}
