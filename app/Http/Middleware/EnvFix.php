<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

/*
 *  В фреймворке есть проблема с конфигами.
 *  При частом обновлении страницы данные с файла .env не считываются.
 *  В этом случае посредник обновляет страницу.
 *
 *  TODO может возникнуть проблема с бесконечной перезагрузкой
 */
class EnvFix
{
    public function handle(Request $request, Closure $next) {
        if (empty($_ENV)) {
            return redirect()->refresh();
        }

        return $next($request);
    }
}
