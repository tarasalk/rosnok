<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    public function handle(Request $request, Closure $next, $role = null) {
        if (is_null($role))
            abort(404);

        if (env('cancel_middleware_check_role'))
            return $next($request);

        $aUserRole = Auth::user()->getRoles();
        foreach ($aUserRole as $oRole) {
            if ($oRole->name == $role)
                return $next($request);
        }

        // TODO страница нет доступа
        return redirect()->route('main');
    }
}
