<?php namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Lavary\Menu\Facade as Menu;

class CabinetSidebarComposer {
	public function compose(View $view)	{
        Cache::remember('cabinetMenu'.Auth::user()->id, 1, function() use ($view) {
            $aUserRoles = Auth::user()->getRoles();

            Menu::make('cabinetMenu', function($menu) use ($aUserRoles) {
                $this->generateGroup($menu, [
                    'group_id' => 'userMenu',
                    'items' => [
                        [
                            'title' => 'Рабочий стол',
                            'route' => 'cabinetDesktop',
                            'class_icon' => 'desktop'
                        ],
                        [
                            'title' => 'Паспорт',
                            'route' => 'passportGet',
                            'class_icon' => 'passport'
                        ]
                    ]
                ]);

                foreach ($aUserRoles as $oRole) {
                    switch ($oRole->name) {
                        case 'client':
                            $this->generateGroup($menu, [
                                'group_id' => 'clientMenu',
                                'items' => [
                                    [
                                        'title' => 'Карта клиента',
                                        'route' => 'clientMain'
                                    ],
                                    [
                                        'title' => 'Мои кредиты',
                                        'route' => 'client.credits'
                                    ],
                                    [
                                        'title' => 'Оплата тарифа',
                                        'route' => 'clientPayRate'
                                    ]
                                ]
                            ]);
                            break;

                        case 'agent':
                            $this->generateGroup($menu, [
                                'group_id' => 'agentMenu',
                                'items' => [
                                    [
                                        'title' => 'Клиенты',
                                        'route' => 'agentClients',
                                        'class_icon' => 'clients'
                                    ],
                                    [
                                        'title' => 'Входящие документы',
                                        'route' => 'agentInboxDocs'
                                    ],
                                    [
                                        'title' => 'Заявки с лендинга',
                                        'route' => 'agentLandingRequest'
                                    ],
                                    [
                                        'title' => 'Журнал клиентов',
                                        'route' => 'agentJournalCold',
                                        'class_icon' => 'clients'
                                    ]
                                ]
                            ]);
                            break;

                        case 'ovs':
                            $this->generateGroup($menu, [
                                'group_id' => 'ovsMenu',
                                'items' => [
                                    [
                                        'title' => 'Входящие документы',
                                        'route' => 'ovsInboxDocs'
                                    ],
                                ]
                            ]);
                            break;

                        case 'ois_u':
                            $this->generateGroup($menu, [
                                'group_id' => 'oisuMenu',
                                'items' => [
                                    [
                                        'title' => 'Исходящие документы',
                                        'route' => 'oisuSentDocs'
                                    ],
                                ]
                            ]);
                            break;

                        case 'ois_s':
                            $this->generateGroup($menu, [
                                'group_id' => 'oissMenu',
                                'items' => [
                                    [
                                        'title' => 'Печать исходящих',
                                        'route' => 'oissPrintSentDocs'
                                    ],
                                    [
                                        'title' => 'Исходящие документы',
                                        'route' => 'oissSentDocs'
                                    ],
                                ]
                            ]);
                            break;

                        case 'lawyer':
                            $this->generateGroup($menu, [
                                'group_id' => 'lawyerMenu',
                                'items' => [
                                    [
                                        'title' => 'Клиенты',
                                        'route' => 'lawyerClients',
                                        'class_icon' => 'clients'
                                    ],
                                    [
                                        'title' => 'Входящие документы',
                                        'route' => 'lawyerInboxDocs'
                                    ],
                                    [
                                        'title' => 'Исходящие документы',
                                        'route' => 'lawyerSentDocs'
                                    ],
                                    [
                                        'title' => 'Исходящие документы добавление',
                                        'route' => 'lawyerSentDocsAddView'
                                    ],
                                ]
                            ]);
                            break;

                        case 'admin':
                            $this->generateGroup($menu, [
                                'group_id' => 'adminMenu',
                                'items' => [
                                    [
                                        'title' => 'Статьи админка',
                                        'route' => 'articlesAdminMain'
                                    ],
                                ]
                            ]);
                            break;
                    }
                }
            });

            $view->with('aUserRole', $aUserRoles);
        });
	}

    private function generateGroup($menu, array $params) {
        $group = $menu->add($params['group_id']);
        $group->group_id = $params['group_id'];

        foreach ($params['items'] as $item) {
            $groupItem = $group->add($item['title'],  ['route' => $item['route']]);

            if (isset($item['class_icon']))
                $groupItem->class_icon = $item['class_icon'];
        }
    }
}