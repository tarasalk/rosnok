<?php namespace App\Http\ViewComposers;

use App\Facades\PeopleApiFacade;
use App\Models\Client;
use App\Models\Logic\Docs\Docs;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class ClientComposer {
	public function compose(View $view)	{
		$viewData = (object) $view->getData();
		if (isset($viewData->client_id))
			$client_id = $viewData->client_id;
		else
			$client_id = Auth::user()->id;

		$oClient = Client::where('client_id', $client_id)->with('office')->first();
		$agent_id = $oClient->agent_id;

		$aMainDocs = Docs::getAvailableMainDocs($client_id);

        $response = PeopleApiFacade::userGet([$client_id, $agent_id], ['avatar']);
		if ($response) {
			$users = $response;
			if (isset($users->$client_id) && isset($users->$agent_id)) {
                $oClientPeople = $users->$client_id;
				$oAgent = $users->$agent_id;

				$view->with('oClient', $oClient);
				$view->with('oClientPeople', $oClientPeople);
				$view->with('oAgent', $oAgent);
				$view->with('aMainDocs', $aMainDocs);
			}
		}
        else abort(404);
	}
}