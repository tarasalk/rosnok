<?php

namespace App\Repositories;

use App\Facades\PeopleApiFacade;
use App\Models\SentDocs;

class OISSSentDocRepository extends BaseRepository {

    public static function getDocs() {
        $aDocs = SentDocs::where('template_type', 'lawyer')->get();

        $aClientsIdsPeople = $aDocs->pluck('client_id')->toArray();
        $response = PeopleApiFacade::userGet($aClientsIdsPeople);
        if ($response) {
            $aClientsPeople = $response;
        }
        else {
            $aDocs = [];
            $aClientsPeople = [];
        }

        foreach ($aDocs as $oDoc) {
            $oDoc->fio = parent::getFioByPeopleUser($aClientsPeople->{$oDoc->client_id});
        }

        return $aDocs;
    }

    // TODO неверно, надо заюзать mydoc.pobedim.com
    public static function getPrintSentDocs() {
        $aDocs = SentDocs::where('template_type', 'template')->get();

        $aClientsIdsPeople = $aDocs->pluck('client_id')->toArray();
        $response = PeopleApiFacade::userGet($aClientsIdsPeople);
        if ($response) {
            $aClientsPeople = $response;
        }
        else {
            $aDocs = [];
            $aClientsPeople = [];
        }

        foreach ($aDocs as $oDoc) {
            $oDoc->fio = parent::getFioByPeopleUser($aClientsPeople->{$oDoc->client_id});
        }

        return $aDocs;
    }
}
