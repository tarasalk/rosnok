<?php

namespace App\Repositories;

use App\Facades\PeopleApiFacade;
use App\Helpers\JsonWrapper;
use App\Models\InboxDocEvents;
use App\Models\InboxDocs;
use Illuminate\Support\Facades\Log;

class AgentInboxDocRepository extends BaseRepository {

    public static function getDocs($sender_id = null) {
        if ($sender_id)
            $aDocs = InboxDocs::where('sender_id', $sender_id);
        else
            $aDocs = InboxDocs::select('*');

        $aDocs = $aDocs->with('events')->get();

        $aUsersIdsPeople = $aDocs->pluck('client_id')->merge($aDocs->pluck('sender_id'))->unique()->toArray();
        $response = PeopleApiFacade::userGet($aUsersIdsPeople);
        if ($response) {
            $aUsersPeople = $response;
        }
        else {
            $aDocs = [];
            $aUsersPeople = [];
        }

        foreach ($aDocs as $oDoc) {
            $oDoc->client_name = parent::getFioByPeopleUser($aUsersPeople->{$oDoc->client_id});

            $operatorCheckEvent = $oDoc->events->whereInLoose('event_type_id', [2,3,4])->sortByDesc('created_at')->first();
            if ($operatorCheckEvent) {
                $oDoc->operator_notation = $operatorCheckEvent->notation;
                $oDoc->date_operator_check = $operatorCheckEvent->created_at;
            }

            $oDocEventAdd = $oDoc->events->whereLoose('event_type_id', 1)->first();
            $oDoc->sender_notation = isset($oDocEventAdd->notation) ? $oDocEventAdd->notation : '';
        }

        return $aDocs;
    }

    public static function add($client_id, $agent_id, $agent_notation, $file_path) {
        $oDoc = new InboxDocs();
        $oDoc->client_id = $client_id;
        $oDoc->sender_id = $agent_id;
        $oDoc->file_path = $file_path;
        $oDoc->status = InboxDocs::status_new;

        if ($oDoc->save()) {
            $oDocEvent = new InboxDocEvents();
            $oDocEvent->doc_id = $oDoc->id;
            $oDocEvent->event_type_id = 1;
            $oDocEvent->initiator_id = $agent_id;
            $oDocEvent->notation = $agent_notation;
            
            if ($oDocEvent->save())
                return JsonWrapper::success();
            else
                Log::error('error_agent_save_inbox_doc_event');
        }
        
        return JsonWrapper::error(['undefined_error']);
    }
}
