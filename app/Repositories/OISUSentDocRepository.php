<?php

namespace App\Repositories;

use App\Facades\PeopleApiFacade;
use App\Models\InboxDocs;
use App\Models\SentDocs;

class OISUSentDocRepository extends BaseRepository {

    public static function getDocs() {
        $aDocs = SentDocs::where('template_type', 'lawyer')->get();

        $aClientsIdsPeople = $aDocs->pluck('client_id')->toArray();
        $response = PeopleApiFacade::userGet($aClientsIdsPeople);
        if ($response) {
            $aClientsPeople = $response;
        }
        else {
            $aDocs = [];
            $aClientsPeople = [];
        }

        foreach ($aDocs as $oDoc) {
            $oDoc->fio = parent::getFioByPeopleUser($aClientsPeople->{$oDoc->client_id});
        }

        return $aDocs;
    }
}
