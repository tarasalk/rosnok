<?php

namespace App\Repositories;

class BaseRepository {
    /**
     * @param \stdClass $oUser Юзер полученный из people/user/get
     * @return string
     */
    public static function getFioByPeopleUser($oUser) {
        if (isset($oUser->passport))
            $fio = sprintf('%s %s %s',
                $oUser->passport->last_name,
                $oUser->passport->first_name,
                $oUser->passport->middle_name
            );
        elseif (isset($oUser->base))
            $fio = sprintf('%s %s %s',
                $oUser->base->last_name,
                $oUser->base->first_name,
                isset($oUser->base->middle_name) ? $oUser->base->middle_name: ''
            );
        else
            $fio = '';

        return $fio;
    }

    /**
     * @param \stdClass $oUser Юзер полученный из people/user/get
     * @param $oUser
     */
    public static function mergeWithPeopleUser($object, $oUser) {
        $object->fio = self::getFioByPeopleUser($oUser);

        if (isset($oUser->base))
            $object->base = $oUser->base;

        if (isset($oUser->phone))
            $object->phone = $oUser->phone;
    }
}
