<?php

namespace App\Repositories;

use App\Facades\PeopleApiFacade;
use App\Helpers\PaginatorWrapper;
use App\Models\Client;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class LawyerClientsRepository extends BaseRepository  {

    public static function getPageClientsByCategory($category, $agent_id = null) {
        if ($agent_id)
            $aClients = Client::where('agent_id', $agent_id);
        else
            $aClients = Client::select('*');

        $aClients = $aClients->with('office')->with(['inboxDocs' => function($q) {
            $q->whereHas('events', function ($q) {
                $q->whereIn('event_type_id', [5, 6]);
            })->with(['events' => function ($q) {
                $q->whereIn('event_type_id', [5, 6]);
            }]);
        }]);

        switch ($category) {
            case 'all':
                break;

            case 'without_delay':
                $aClients = $aClients->withoutDelay();
                break;

            case 'not_pay_rate':
                $aClients = $aClients->notPayRate();
                break;

            case 'soon_pay':
                $aClients = $aClients->soonPay();
                break;

            case 'delay_less_1_month':
                $aClients = $aClients->delayLess1Month();
                break;

            case 'delay_more_1_month':
                $aClients = $aClients->delayMore1Month();
                break;

            default:
                return [];
        }

        $aClientsCount = $aClients->count();

        $pageClients = $aClients->simplePaginate(15);

        $client_ids = $pageClients->pluck('client_id')->toArray();

        $response = PeopleApiFacade::userGet($client_ids, ['avatar']);
        if ($response) {
            $aClientsPeople = $response;
        }
        else $aClientsPeople = [];

        $aClients = new PaginatorWrapper($pageClients->items(), 15, $aClientsCount);

        foreach ($aClients as $oClient) {
            $oClient->pay_day_diff = Carbon::now()->diff(new Carbon($oClient->date_end_paid_month))->format('%r%a');

            $in_work = Carbon::now()->diff(new Carbon($oClient->date_start_work));
            $oClient->in_work_month = $in_work->m + $in_work->y*12;
            $oClient->in_work_day = $in_work->d;

            $temp = 0;
            $oClient->inboxDocs->each(function($item, $key) use (&$temp) {
                if ($item->events->where('event_type_id', 6)->count() > 0)
                    $temp++;
            });
            $oClient->inboxDocsLawyerReadCount = $temp;

            parent::mergeWithPeopleUser($oClient, $aClientsPeople->{$oClient->client_id});
        }

        return $aClients;
    }

    

    /**
     * TODO поиск через sql
     */
    public static function searchClients($searchFio) {
        $aCacheClients = Cache::remember('lawyerSearchClients', 5, function() {
            $aClients = Client::select('client_id')->get();
            $client_ids = $aClients->pluck('client_id')->toArray();

            $response = PeopleApiFacade::userGet($client_ids);
            if ($response)
                $aClientsPeople = $response;
            else
                $aClientsPeople = [];

            return compact('aClients', 'aClientsPeople');
        });

        $aClients = $aCacheClients['aClients'];
        $aClientsPeople = $aCacheClients['aClientsPeople'];

        foreach ($aClients as $key => $oClient) {
            $fio = self::getFioByPeopleUser($aClientsPeople->{$oClient->client_id})." ($oClient->client_id)";

            if (mb_stripos($fio, $searchFio) !== false)
                $oClient->fio = $fio;
            else
                unset($aClients[$key]);
        }

        return $aClients;
    }
}
