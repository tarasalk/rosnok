<?php

namespace App\Repositories;

use App\Facades\PeopleApiFacade;
use App\Models\InboxDocs;

class LawyerInboxDocRepository extends BaseRepository {

    public static function getDocs($client_id = null) {
        if ($client_id)
            $aDocs = InboxDocs::where('client_id', $client_id);
        else
            $aDocs = InboxDocs::select('*');

        $aDocs = $aDocs->with(['event' => function($q) {
                $q->where('event_type_id', 5);
            }])
            ->whereHas('events', function($q) {
                $q->where('event_type_id', 5);
            })
            ->get();

        $aUsersIdsPeople = $aDocs->pluck('client_id')->merge($aDocs->pluck('sender_id'))->unique()->toArray();
        $response = PeopleApiFacade::userGet($aUsersIdsPeople);
        if ($response) {
            $aUsersPeople = $response;
        }
        else {
            $aDocs = [];
            $aUsersPeople = [];
        }

        foreach ($aDocs as $oDoc) {
            $oDoc->agent_name = parent::getFioByPeopleUser($aUsersPeople->{$oDoc->sender_id});
            $oDoc->client_name = parent::getFioByPeopleUser($aUsersPeople->{$oDoc->client_id});

            $oDoc->sender_notation = $oDoc->events->whereLoose('event_type_id', 1)->first()->notation;
            $oDoc->lawyer_read = $oDoc->events->whereLoose('event_type_id', 6)->first() ? 1:0;
        }

        return $aDocs;
    }
}
