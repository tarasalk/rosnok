<?php

namespace App\Repositories;

use App\Facades\PeopleApiFacade;
use App\Helpers\PaginatorWrapper;
use App\Models\Client;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class AgentClientsRepository extends BaseRepository  {

    public static function getClientsByCategory($category, $agent_id) {
        $aAgentClients = Client::where('agent_id', $agent_id)->with('office');

        switch ($category) {
            case 'all':
                break;

            case 'without_delay':
                $aAgentClients = $aAgentClients->withoutDelay();
                break;

            case 'not_pay_rate':
                $aAgentClients = $aAgentClients->notPayRate();
                break;

            case 'soon_pay':
                $aAgentClients = $aAgentClients->soonPay();
                break;

            case 'delay_less_1_month':
                $aAgentClients = $aAgentClients->delayLess1Month();
                break;

            case 'delay_more_1_month':
                $aAgentClients = $aAgentClients->delayMore1Month();
                break;

            default:
                return [];
        }

        $aClientId =  $aAgentClients->pluck('client_id')->toArray();

        $response = PeopleApiFacade::userGet($aClientId, ['avatar']);
        if ($response) {
            $aClientsPeople = $response;
        }
        else $aClientsPeople = [];

        $aClients = $aAgentClients->get();

        foreach ($aClients as $oClient) {
            $oClient->pay_day_diff = Carbon::now()->diff(new Carbon($oClient->date_end_paid_month))->format('%r%a');

            $in_work = Carbon::now()->diff(new Carbon($oClient->date_start_work));
            $oClient->in_work_month = $in_work->m + $in_work->y*12;
            $oClient->in_work_day = $in_work->d;

            parent::mergeWithPeopleUser($oClient, $aClientsPeople->{$oClient->client_id});
        }

        return $aClients;
    }
}
