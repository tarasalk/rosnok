<?php

namespace App\Repositories;

use App\Facades\PeopleApiFacade;
use App\Models\InboxDocs;
use App\Models\SentDocs;

class LawyerSentDocRepository extends BaseRepository {

    public static function getDocs($client_id = null) {
        if ($client_id)
            $aDocs = SentDocs::where('client_id', $client_id);
        else
            $aDocs = SentDocs::select('*');

        $aDocs = $aDocs->get();

        $aClientsIdsPeople = $aDocs->pluck('client_id')->unique()->toArray();
        $response = PeopleApiFacade::userGet($aClientsIdsPeople);
        if ($response) {
            $aClientsPeople = $response;
        }
        else {
            $aDocs = [];
            $aClientsPeople = [];
        }

        foreach ($aDocs as $oDoc) {
            if (isset($aClientsPeople->{$oDoc->client_id}))
                $oDoc->fio = parent::getFioByPeopleUser($aClientsPeople->{$oDoc->client_id});
            else
                $oDoc->fio = '';
        }

        return $aDocs;
    }
}
