<?php

namespace App\Repositories;

use App\Models\InboxDocs;
use Illuminate\Support\Facades\Cache;

class OVSInboxDocRepository extends BaseRepository  {

    // FIXME очень медленно
    public static function getDocsByStatus($status) {
        $aResult = InboxDocs::getDocsByStatus($status);
        $aUsersPeople = $aResult['aUsersPeople'];

        foreach ($aResult['aDocs'] as $key => $oDoc) {
            if (isset($aUsersPeople->{$oDoc->sender_id}))
                $oDoc->agent_name = parent::getFioByPeopleUser($aUsersPeople->{$oDoc->sender_id});

            if (isset($aUsersPeople->{$oDoc->client_id}))
                $oDoc->client_name = parent::getFioByPeopleUser($aUsersPeople->{$oDoc->client_id});

            $oDoc->sender_notation = $oDoc->events->whereLoose('event_type_id', 1)->first()->notation;
            $oDoc->send_lawyer = $oDoc->events->whereLoose('event_type_id', 5)->first() ? 1:0;

            // TODO может вытаскивать эвент по текущему статусу
            $operatorCheckEvent = $oDoc->events->whereInLoose('event_type_id', [2,3,4])->sortByDesc('created_at')->first();
            if ($operatorCheckEvent) {
                $oDoc->operator_notation = $operatorCheckEvent->notation;
                $oDoc->date_operator_check = $operatorCheckEvent->created_at;
            }
        }

        return $aResult['aDocs'];
    }

    public static function getDocsCategoriesCount() {
        return Cache::remember('ovsInboxDocsCategoriesCount', 2, function() {
            return [
                'all'       =>  InboxDocs::count(),
                'accepted'  =>  InboxDocs::where('status', InboxDocs::status_accepted)->count(),
                'rebuke'    =>  InboxDocs::where('status', InboxDocs::status_rebuke)->count(),
                'denied'    =>  InboxDocs::where('status', InboxDocs::status_denied)->count(),
                'new'       =>  InboxDocs::where('status', InboxDocs::status_new)->count(),
            ];
        });
    }

    public static function getDocById($doc_id) {
        $oDoc = InboxDocs::with('events')->where('id', $doc_id)->first();
        if ($oDoc) {
            $eventDocCheck = $oDoc->events->whereInLoose('event_type_id', [2,3,4])->sortByDesc('created_at')->first();
            if ($eventDocCheck)
                $oDoc->notation = $eventDocCheck->notation;
        }

        return $oDoc;
    }


}
