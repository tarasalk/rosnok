<?php

namespace App\Helpers;

class SmsHelper {
    protected $host;
    protected $enableSend;
    protected $curl;

    const SENDER_CENOBOY = "Cenoboy";
    const SENDER_ROSNOK = "POCHOK";

    const COUNTRY_RU = "RU";

    public function __construct($host, $enableSend = false, CurlWrapper $curl) {
        $this->host = $host;
        $this->enableSend = $enableSend;
        $this->curl = $curl;
    }

    /**
     * @param $phone
     * @param $message
     * @param string $sender
     * @param string $countryCode
     * @return array
     */
    public function sendSms($phone, $message, $sender = self::SENDER_ROSNOK, $countryCode = self::COUNTRY_RU) {
        if ($this->enableSend) {
            try {
                $url = $this->host.'/index.php/send_sms';

                $postParams = [
                    'query' => [
                        'phone' => $phone,
                        'message' => $message,
                        'sender' => $sender,
                        'country' => $countryCode
                    ]
                ];

                $response = $this->curl->send('post', $url, $postParams, true);
                if ($response->status == 'ok') {
                    $output = ['status' => 'ok', 'response' => $response->response];
                }
                else {
                    throw new \Exception(implode(' ', $response->errors));
                }
            } catch (\Exception $e) {
                $output = ['status' => 'error', 'phone' => $phone, 'error' => $e->getMessage()];
            }
        }
        else {
            $output = ['status' => 'ok', 'response' => 'fake_send'];
        }

        return $output;
    }
}
