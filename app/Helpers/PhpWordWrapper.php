<?php
namespace App\Helpers;

use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\TemplateProcessor;

/**
 * Class PhpWordWrapper
 * @package DocsGenerator\Helpers
 */
class PhpWordWrapper extends PhpWord {
    /**
     * путь к шаблонам
     * @var string
     */
    private $pathTemplate;
    /**
     * путь к сгенерированным докам(свалка)
     * @var string
     */
    private $pathGenerate;
    /**
     * используемый путь к шаблону
     * @var string
     */
    private $pathCurrent;

    /**
     * объект шаблона библиотеки PhpWord
     * @var TemplateProcessor
     */
    private $oDocument = null;

    /**
     * базовый путь к документам
     */
    const basePatch = 'docs';

    /**
     * прочие шаблоны
     */
    const doctype_other = 0;
    /**
     * основные шаблоны
     */
    const doctype_main = 1;
    /**
     * шаблоны для периодических доков
     */
    const doctype_period = 2;
    /**
     * шаблоны по банкам
     */
    const doctype_bank = 3;

    /**
     * перенос строки для ворда
     */
    const br = "<w:br/>";

    /**
     * @param int $doctype тип шаблона
     */
    public function __construct($doctype) {
        \PhpOffice\PhpWord\Settings::setTempDir('docs/temp');

        $this->pathTemplate = self::basePatch . '/template/';
        $this->pathGenerate = self::basePatch . '/generate/';

        switch ($doctype) {
            case self::doctype_other:
                $this->pathCurrent = $this->pathTemplate . 'other/';
                break;

            case self::doctype_main:
                $this->pathCurrent = $this->pathTemplate . 'main/';
                break;

            case self::doctype_period:
                $this->pathCurrent = $this->pathTemplate . 'period/';
                break;

            case self::doctype_bank:
                $this->pathCurrent = $this->pathTemplate . 'bank/';
                break;

            default:
                exit('попытка сгенерировать неизвестный тип шаблона');
        }

        parent::__construct();
    }

    /**
     * сгенерировать шаблон
     * @param string $templateName имя шаблона в системе
     * @param array $aValue параметры для замены в шаблоне
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function generate($templateName, array $aValue) {
		$templateName.= '.docx';
        $document = $this->loadTemplate($this->pathCurrent . $templateName);

        foreach ($aValue as $value)
            $document->setValue($value[0], $value[1]);

        $this->oDocument = $document;
    }

    /**
     * скачать документ
     * @param $fileName
     */
    public function transferDocument($fileName) {
        if (!is_null($this->oDocument)) {
            $fileName .= '.docx';

            $this->oDocument->saveAs($this->pathGenerate . $fileName);

            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . $fileName);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($this->pathGenerate . $fileName));

            // читаем файл и отправляем его пользователю
            readfile($this->pathGenerate . $fileName);
            exit;
        }
    }
}
