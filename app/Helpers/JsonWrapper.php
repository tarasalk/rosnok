<?php

namespace App\Helpers;


class JsonWrapper {
	const SUCCESS = 'ok';
	const ERROR = 'error';

	public static function success($content = []) {
		$response['status'] = JsonWrapper::SUCCESS;

		return array_merge($response, $content);
	}

	public static function error($content = 'undefined_error') {
		$response = [
			'status' => JsonWrapper::ERROR,
			'errors' => $content
		];

		return $response;
	}

	/**
	 * ошибки валидации
	 * @param $content
	 * @return array
	 */
	public static function validation($content) {
		$response = [
			'status' => JsonWrapper::ERROR,
			'validation' => $content
		];

		return $response;
	}
}