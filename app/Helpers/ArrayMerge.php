<?php
namespace App\Helpers;

class ArrayMerge {
	/*
	 * ===================
	 * ===== EXAMPLE mergeByKey =====
	 * ===================
	 *
	 * $arr1 = [ ['user_id' => 1, 'agent_id' => 2], ['user_id' => 2, 'agent_id' => 3]];
	 * $arr2 = [ ['user_id' => 1, 'lawyer_id' => 3], ['user_id' => 2, 'lawyer_id' => 4]];
	 *
	 * $result = [
	 *      1 => ['user_id = 1', 'agent_id' => 2, 'lawyer_id => 3'],
	 *      2 => ['user_id = 1', 'agent_id' => 2, 'lawyer_id => 3']
	 * ];
	 */

	/**
	 * объединяет два массива/объекта, связь по ключам
	 * @param $arr1
	 * @param $arr2
	 * @param $key1
	 * @param $key2
	 * @return \stdClass
	 */
	public static function mergeByKey($arr1, $arr2, $key1, $key2 = null) {
		if (is_null($key2)) $key2 = $key1;
		if (is_object($arr1)) $arr1 = self::stdToArray($arr1);
		if (is_object($arr2)) $arr2 = self::stdToArray($arr2);

		$newArr1 = []; $newArr2 = [];
		foreach ($arr1 as $item1) {
			if (isset($item1[$key1]))
				$newArr1[$item1[$key1]] = $item1;
		}
		foreach ($arr2 as $item2) {
			if (isset($item2[$key2])) {
				$endKey = $item2[$key2];

				if (isset($newArr1[$endKey]))
					$newArr2[$endKey] = $newArr1[$endKey] + $item2;
			}
		}

		return self::arrayToStd($newArr2);
	}

	public static function mergeByKey2($arr1, $arr2, $key1, $key2 = null, $mergeKey = null) {
		if (is_null($key2)) $key2 = $key1;
		if (is_object($arr1)) $arr1 = self::stdToArray($arr1);
		if (is_object($arr2)) $arr2 = self::stdToArray($arr2);

		$newArr1 = [];
		foreach ($arr1 as $key => $item1) {
			foreach ($arr2 as $item2) {
				if ($item1[$key1] == $item2[$key2]) {

					if (!is_null($mergeKey)) {
						$newArr1[$key] = $item1;
						$newArr1[$key][$mergeKey] = $item2;
					}
					else $newArr1[] = $item1 + $item2;
					break;
				}
			}
		}


		return self::arrayToStd($newArr1);
	}

	public static function stdToArray($obj){
		$rc = (array)$obj;
		foreach($rc as $key => &$field){
			if(is_object($field))$field = self::stdToArray($field);
		}
		return $rc;
	}

	public static function arrayToStd($ar) {
		foreach ($ar as &$i) {
			if (is_array($i) && array_values($i) !== $i) {
				$i = self::arrayToStd($i);
			}
		}
		return (object)$ar;
	}
}