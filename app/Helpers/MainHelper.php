<?php

namespace App\Helpers;


class MainHelper {
	public static function getRandomFileName(\Symfony\Component\HttpFoundation\File\UploadedFile $file) {
		return sha1($file->getFilename().rand(1000, 9999)).'.'.$file->getClientOriginalExtension();
	}
}