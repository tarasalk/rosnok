<?php

namespace App\Helpers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Request;

class PaginatorWrapper extends Paginator {
	public $maxItemsCount;

	public function __construct($items, $perPage, $maxItemsCount, $currentPage = null, array $options = []) {
		parent::__construct($items, $perPage, $currentPage, $options);

		$this->maxItemsCount = $maxItemsCount;

        $this->setPath(Request::url());
	}

	public function hasMorePages() {
		return $this->maxItemsCount > count($this->items);
	}

	/**
	 * TODO не должно тут быть html
	 * @return string
	 */
	public function getLinks() {
		$prev = ($this->currentPage > 1) ? true:false;
		$next = ($this->perPage()*$this->currentPage < $this->maxItemsCount) ? true:false;

		if (!($next || $prev)) return '';

		$html = "<ul>";
		$html.= $prev ? sprintf("<a href=%s>Назад</a>", parent::previousPageUrl()) : '';
		$html.= $next ? sprintf(" <a href=%s>Далее</a>", parent::nextPageUrl()) : '';
		$html.= '</ul>';

		return $html;
	}
}