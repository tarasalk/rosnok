<?php

namespace App\Helpers;


use App\Models\Log\Curl;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class CurlWrapper {
    protected $client;

    public function __construct() {
        $this->client = new Client();
    }

	public static function getInstance() {
		return new Client();
	}

    public function send($requestType, $url, $params, $logging = true) {
        try {
            $response = $this->client->request($requestType, $url, $params);
            $response = json_decode($response->getBody()->getContents());
        }
        catch (\GuzzleHttp\Exception\ClientException $e) {
            Log::error($e);
            return (object) JsonWrapper::error(['undefined_error']);
        }

        if(Auth::check())
            $sender_id = Auth::user()->id;
        else
            $sender_id = 0;

		if (env('curl_log') && $logging) {
            $oCurl = Curl::create([
                'url' => $url,
                'sender_id' => $sender_id,
                'params' => json_encode($params),
                'response' => json_encode($response)
            ]);
        }

        return (object) $response;
    }

    /**
     * может даже файлы отправлять
     * @param @params array(array('name'=> '', 'contents' => ''))
     * @return object
     */
    public function sendMultipart($url, $params, $logging = true) {
        try {
            $response = $this->client->post($url, ['multipart'=> $params]);
            $response =  json_decode($response->getBody()->getContents());
        }
        catch (\GuzzleHttp\Exception\ClientException $e) {
            Log::error($e);
            return (object) JsonWrapper::error(['undefined_error']);
        }

        if(Auth::check())
            $sender_id = Auth::user()->id;
        else
            $sender_id = 0;

        if (env('curl_log') && $logging) {
            $oCurl = Curl::create([
                'url' => $url,
                'sender_id' => $sender_id,
                'params' => json_encode($params),
                'response' => json_encode($response)
            ]);
        }

        return (object) $response;
    }
}