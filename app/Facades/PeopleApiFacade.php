<?php

namespace App\Facades;

use App\Models\Api\People;
use Illuminate\Support\Facades\Facade;

class PeopleApiFacade extends Facade {

	protected static function getFacadeAccessor() { return People::class; }

}