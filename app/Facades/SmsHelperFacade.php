<?php

namespace App\Facades;

use App\Helpers\SmsHelper;
use Illuminate\Support\Facades\Facade;

class SmsHelperFacade extends Facade {

	protected static function getFacadeAccessor() { return SmsHelper::class; }

}