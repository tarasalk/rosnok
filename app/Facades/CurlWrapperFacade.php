<?php

namespace App\Facades;

use App\Helpers\CurlWrapper;
use Illuminate\Support\Facades\Facade;

class CurlWrapperFacade extends Facade {

	protected static function getFacadeAccessor() { return CurlWrapper::class; }

}