<?php

namespace App\Console\Commands;

use App\Models\LandingRequest;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;

class Migrate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrateCenoboy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'migrate DB from cenomarket and other';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->loadDbConnections();
        $this->prepareDB();

        $this->migrateUsers();
        $this->migratePhones();
        $this->migratePassport();
        $this->migrateClients();
        $this->migrateOffices();
        $this->migrateVkOauth();
        $this->migrateRoles();
        $this->migrateInboxDocs();
        $this->migrateCreditConsumers();
        $this->migrateCreditCards();
        $this->migrateCreditMicro();
        $this->migrateLandingRequest();
        $this->migrateSentDocs();
    }

    private function migrateUsers() {
        $aUsers = DB::connection('cenomarket')->select('select * from prefix_user');

        foreach($aUsers as $oUser) {
            DB::insert('
                INSERT INTO
					users (id, created_at, updated_at)
			 	VALUES
			 		(?, ?, ?)'
                , [
                    $this->clearValue($oUser->user_id),
                    $this->clearValue($oUser->user_date_register),
                    date('Y-m-d H:i:s'),
                ]
            );

            $name = explode(' ', $oUser->user_profile_name);
            $oUser->first_name = isset($name[0]) ? $name[0]:'';
            $oUser->last_name = isset($name[1]) ? $name[1]:'';

            switch ($oUser->user_profile_sex) {
                case 'man':
                    $oUser->gender = 'male';
                    break;

                case 'woman':
                    $oUser->gender = 'female';
                    break;

                default:
                    $oUser->gender = 'undefined';
            }

            DB::connection('people')->insert('
                INSERT INTO
					users (id, password, first_name, last_name,
                        gender, created_at, updated_at, avatar,
					    balance, frozen_balance, click_balance)
			 	VALUES
			 		(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
                , [
                    $this->clearValue($oUser->user_id),
                    $this->clearValue($oUser->user_password),
                    $this->clearValue($oUser->first_name),
                    $this->clearValue($oUser->last_name),
                    $this->clearValue($oUser->gender),
                    $this->clearValue($oUser->user_date_register),
                    date('Y-m-d H:i:s'),
                    $this->clearValue($oUser->user_profile_avatar),
                    $this->clearValue($oUser->profit),
                    $this->clearValue($oUser->frozen_profit),
                    $this->clearValue($oUser->balance)
                ]
            );
        }
    }

    private function migratePhones() {
        $aPhones = DB::connection('cenomarket')->select('select * from prefix_confirm_phone');

        foreach($aPhones as $oPhone) {
            DB::connection('people')->insert('
                INSERT INTO
					phones (id, user_id, country, country_id, phone,
					    activate_code, activate_status, date_activate)
			 	VALUES
			 		(?, ?, ?, ?, ?, ?, ?, ?)'
                , [
                    $this->clearValue($oPhone->confirm_id),
                    $this->clearValue($oPhone->user_id),
                    $this->clearValue($oPhone->country),
                    $this->clearValue($oPhone->country_id),
                    $this->clearValue($oPhone->phone),
                    $this->clearValue($oPhone->code),
                    $this->clearValue($oPhone->phone_activate),
                    $this->clearValue($oPhone->activate_date),
                ]
            );
        }
    }

    private function migratePassport() {
        $aPassport = DB::connection('docflow')->select('select * from users');

        foreach($aPassport as $oPassport) {
            DB::connection('people')->insert('
                INSERT INTO
					user_passport (id, user_id, first_name, last_name, middle_name,
                        birthday, pass_series, pass_number, pass_issued, pass_issued_date,
                        unit_code, gender, pass_scan1, pass_scan2, proxy_scan1, proxy_scan2,
                        birth_republic, birth_city, registration_republic, registration_city,
                        registration_street, registration_house, registration_corps,
                        registration_room, registration_date, mail_republic, mail_city,
                        mail_street, mail_house, mail_corps, mail_room, created_at)
			 	VALUES
			 		(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
			 		 ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
                , [
                    $this->clearValue($oPassport->id),
                    $this->clearValue($oPassport->user_id),
                    $this->clearValue($oPassport->name),
                    $this->clearValue($oPassport->last_name),
                    $this->clearValue($oPassport->father_name),
                    $this->clearValue($oPassport->birthday),
                    $this->clearValue($oPassport->pass_series),
                    $this->clearValue($oPassport->pass_number),
                    $this->clearValue($oPassport->pass_description),
                    $this->clearValue($oPassport->pass_date),
                    $this->clearValue($oPassport->unit_code),
                    $this->clearValue($oPassport->sex),
                    $this->clearValue($oPassport->pass_scan_1),
                    $this->clearValue($oPassport->pass_scan_2),
                    $this->clearValue($oPassport->proxy_scan_1),
                    $this->clearValue($oPassport->proxy_scan_2),
                    $this->clearValue($oPassport->birth_region),
                    $this->clearValue($oPassport->birth_city),
                    $this->clearValue($oPassport->address_region),
                    $this->clearValue($oPassport->address_city),
                    $this->clearValue($oPassport->address_street),
                    $this->clearValue($oPassport->address_home),
                    $this->clearValue($oPassport->address_corps),
                    $this->clearValue($oPassport->address_flat),
                    $this->clearValue($oPassport->address_date),
                    $this->clearValue($oPassport->post_region),
                    $this->clearValue($oPassport->post_city),
                    $this->clearValue($oPassport->post_street),
                    $this->clearValue($oPassport->post_home),
                    $this->clearValue($oPassport->post_corps),
                    $this->clearValue($oPassport->post_flat),
                    $this->clearValue($oPassport->created),
                ]
            );
        }
    }

    private function migrateClients() {
        $aClients = DB::connection('cenomarket')->select('select * from findef_clients');

        foreach($aClients as $oClient) {
            DB::insert('
                INSERT INTO
                    clients (id, client_id, agent_id, rate_pay, office_id,
					    month, service_live, date_end_paid_month, 
					    date_start_work, created_at)
			 	VALUES
			 		(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
                , [
                    $this->clearValue($oClient->id),
                    $this->clearValue($oClient->client_id),
                    $this->clearValue($oClient->agent_id),
                    $this->clearValue($oClient->rate_pay),
                    $this->clearValue($oClient->office_id),
                    $this->clearValue($oClient->month),
                    12,
                    $this->clearValue($oClient->last_time_paid),
                    $this->clearValue($oClient->activated_date),
                    $this->clearValue($oClient->added_date),
                ]
            );

            DB::connection('people')->insert('
                INSERT INTO
					user_roles (user_id, role_id)
			 	VALUES
			 		(?, ?)'
                , [
                    $this->clearValue($oClient->client_id),
                    2
                ]
            );
        }
    }

    private function migrateOffices() {
        $aOffices = DB::connection('cenomarket')->select('select * from findef_offices');

        foreach($aOffices as $oOffice) {
            DB::insert('
                INSERT INTO
					offices (id, holder_id, office_name, has_payments,
					    alias, attorney_number, attorney_date)
			 	VALUES
			 		(?, ?, ?, ?, ?, ?, ?)'
                , [
                    $this->clearValue($oOffice->id),
                    $this->clearValue($oOffice->holder_id),
                    $this->clearValue($oOffice->office_name),
                    $this->clearValue($oOffice->has_payments),
                    $this->clearValue($oOffice->alias),
                    $this->clearValue($oOffice->attorney_number),
                    $this->clearValue($oOffice->attorney_date),
                ]
            );
        }
    }

    private function migrateVkOauth() {
        $aVkOauth = DB::connection('cenomarket')->select('select * from prefix_user_oauth');

        foreach($aVkOauth as $oVkOauth) {
            DB::connection('people')->update('
                UPDATE
                    users
                SET
                    vk_id = ?
                WHERE
                    id = ?'
                , [
                    $this->clearValue($oVkOauth->social_id),
                    $this->clearValue($oVkOauth->user_id),
                ]
            );
        }
    }

    private function migrateRoles() {
        $aUsers = DB::connection('cenomarket')->select("select * from prefix_user where role != '2'");

        foreach($aUsers as $oUser) {
            $role_id = false;
            switch ($oUser->role) {
                case 1:
                    // TODO саппорт
                    break;

                case 2:
                    // просто пользователь ценомаркета
                    break;

                case 3:
                    // агент
                    $role_id = 3;
                    break;

                case 4:
                    // наблюдатели (хз зачем нужны, наверно можно забить)
                    break;

                case 5:
                    // админ (нет таких)
                    break;

                case 6:
                    // секретарь
                    $role_id = 5;
                    break;

                case 7:
                    // оператор входящие (нет таких)
                    break;

                case 8:
                    // TODO оператор исходящие
                    break;

                case 9:
                    // TODO оператор full
                    break;

                case 10:
                    // TODO супер секретарь
                    break;

                case 11:
                    // TODO оператор колл-центра
                    break;

                case 12:
                    // TODO бухгалтер
                    break;
            }

            if ($role_id == false)
                continue;

            $oUser->role_id = $role_id;

            DB::connection('people')->insert('
                INSERT INTO
					user_roles (user_id, role_id)
			 	VALUES
			 		(?, ?)'
                , [
                    $this->clearValue($oUser->user_id),
                    $this->clearValue($oUser->role_id),
                ]
            );
        }
    }

    private function migrateInboxDocs() {
        $funEventBySender = function($oDoc) {
            return [
                'doc_id' => $oDoc->id,
                'event_type_id' => 1,
                'initiator_id' => $oDoc->lawyer_id,
                'notation' => $oDoc->sender_notation,
                'created_at' => $oDoc->added_date,
            ];
        };

        $funEventByStatus = function($oDoc) {
            $aEventIds = [
                '1' => 3, // принят с замечаниями
                '2' => 2, // принят
                '-1' => 4 // отклонен
            ];

            $event_id = isset($aEventIds[$oDoc->status]) ? $aEventIds[$oDoc->status] : null;
            if (!is_null($event_id)) {
                return [
                    'doc_id' => $oDoc->id,
                    'event_type_id' => $event_id,
                    'initiator_id' => 0,
                    'notation' => $oDoc->secretary_notation,
                    'created_at' => $oDoc->secretary_update_date,
                ];
            }

            return null;
        };

        $funEventByLawyerSend = function($oDoc) {
            if ($oDoc->lawyer_accept == 2) {
                return [
                    'doc_id' => $oDoc->id,
                    'event_type_id' => 5,
                    'initiator_id' => 0,
                    'notation' => '',
                    'created_at' => $oDoc->lawyer_send_date,
                ];
            }

            return null;
        };

        $aInboxDocs = DB::connection('cenomarket')->select('select * from findef_inbox_docs');

        foreach($aInboxDocs as $key => $oDoc) {
            $temp = explode('|', $oDoc->images);
            if (count($temp) >= 1) {
                $file_path = $temp[0];
                $file_path = str_replace('/uploads/images/fddocs/', '/upload/inboxDocs/', $file_path);
            }
            else {
                $file_path = '';
            }

            $oDoc->file_path = $file_path;

            DB::insert('
                INSERT INTO
					inbox_docs (id, client_id, sender_id, name, status,
					    file_path, created_at)
			 	VALUES
			 		(?, ?, ?, ?, ?, ?, ?)'
                , [
                    $this->clearValue($oDoc->id),
                    $this->clearValue($oDoc->client_id),
                    $this->clearValue($oDoc->lawyer_id),
                    $this->clearValue($oDoc->name),
                    $this->clearValue($oDoc->status),
                    $this->clearValue($oDoc->file_path),
                    $this->clearValue($oDoc->added_date),
                ]
            );

            $aEvent = $funEventBySender($oDoc);
            $aEvents = [];
            if (isset($aEvent))
                $aEvents[] = (object) $aEvent;

            $aEvent = $funEventByStatus($oDoc);
            if (isset($aEvent))
                $aEvents[] = (object) $aEvent;

            $aEvent = $funEventByLawyerSend($oDoc);
            if (isset($aEvent))
                $aEvents[] = (object) $aEvent;

            foreach ($aEvents as $oEvent) {
                DB::insert('
                INSERT INTO
					inbox_doc_events (doc_id, event_type_id, initiator_id,
					 notation, created_at)
			 	VALUES
			 		(?, ?, ?, ?, ?)'
                    , [
                        $this->clearValue($oDoc->id),
                        $this->clearValue($oEvent->event_type_id),
                        $this->clearValue($oEvent->initiator_id),
                        $this->clearValue($oEvent->notation),
                        $this->clearValue($oEvent->created_at),
                    ]
                );
            }
        }
    }

    private function migrateCreditConsumers() {
        $aCredits = DB::connection('docflow')
            ->select('
                    SELECT
                        c.*, b.name as bank_name
                    FROM
                        credits as c
                    LEFT JOIN
                        banks as b
                    ON
                        c.bank = b.id');

        foreach($aCredits as $oCredit) {
            DB::insert('
                INSERT INTO
					credit_consumers (id, client_id, bank_name, contract_number, date_contract_create,
					credit_sum, account_number, credit_commitments_name, last_pay_sum, date_last_pay,
					date_credit_end, average_monthly_pay, notation, created_at, updated_at)
			 	VALUES
			 		(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
                , [
                    $this->clearValue($oCredit->id),
                    $this->clearValue($oCredit->user_id),
                    $this->clearValue($oCredit->bank_name),
                    $this->clearValue($oCredit->doc_number),
                    $this->clearValue($oCredit->date),
                    $this->clearValue($oCredit->amount),
                    $this->clearValue($oCredit->account),
                    $this->clearValue($oCredit->type),
                    $this->clearValue($oCredit->last_amount),
                    $this->clearValue($oCredit->last_date),
                    $this->clearValue($oCredit->credit_time),
                    $this->clearValue($oCredit->average_amount),
                    $this->clearValue($oCredit->comment),
                    $this->clearValue($oCredit->date_created),
                    $this->clearValue($oCredit->date_updated),
                ]
            );
        }
    }

    private function migrateCreditCards() {
        $aCredits = DB::connection('docflow')
            ->select('
                    SELECT
                        c.*, b.name as bank_name
                    FROM
                        credit_cards as c
                    LEFT JOIN
                        banks as b
                    ON
                        c.bank = b.id');

        foreach($aCredits as $oCredit) {
            DB::insert('
                INSERT INTO
					credit_cards (id, client_id, bank_name, contract_number, card_number, date_contract_create,
					    credit_sum, account_number, credit_commitments_name, last_pay_sum, date_last_pay,
					    date_credit_end, average_monthly_pay, notation, created_at)
			 	VALUES
			 		(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
                , [
                    $this->clearValue($oCredit->id),
                    $this->clearValue($oCredit->user_id),
                    $this->clearValue($oCredit->bank_name),
                    $this->clearValue($oCredit->doc_number),
                    $this->clearValue($oCredit->card_number),
                    $this->clearValue($oCredit->date),
                    $this->clearValue($oCredit->amount),
                    $this->clearValue($oCredit->account),
                    $this->clearValue($oCredit->type),
                    $this->clearValue($oCredit->last_amount),
                    $this->clearValue($oCredit->last_date),
                    $this->clearValue($oCredit->credit_time),
                    $this->clearValue($oCredit->average_amount),
                    $this->clearValue($oCredit->comment),
                    $this->clearValue($oCredit->date_created),
                ]
            );
        }
    }

    private function migrateCreditMicro() {
        $aCredits = DB::connection('docflow')
            ->select('
                    SELECT
                        c.*, b.name as bank_name
                    FROM
                        credit_cards as c
                    LEFT JOIN
                        banks as b
                    ON
                        c.bank = b.id');

        foreach($aCredits as $oCredit) {
            DB::insert('
                INSERT INTO
					credit_micro (id, client_id, mfo_id, contract_number, date_contract_create,
					    credit_sum, date_credit_end, notation, created_at)
			 	VALUES
			 		(?, ?, ?, ?, ?, ?, ?, ?, ?)'
                , [
                    $this->clearValue($oCredit->id),
                    $this->clearValue($oCredit->user_id),
                    $this->clearValue($oCredit->bank),
                    $this->clearValue($oCredit->doc_number),
                    $this->clearValue($oCredit->date),
                    $this->clearValue($oCredit->amount),
                    $this->clearValue($oCredit->credit_time),
                    $this->clearValue($oCredit->comment),
                    $this->clearValue($oCredit->date_created),
                ]
            );
        }
    }

    private function migrateLandingRequest() {
        $aRequests = DB::connection('cenomarket')->select('select * from findef_landing_requests');

        foreach($aRequests as $oRequest) {
            switch ($oRequest->client_type) {
                case 'REAL':
                    $oRequest->client_type = LandingRequest::CLIENT_TYPE_REAL;
                    break;

                case 'POTENTIAL':
                    $oRequest->client_type = LandingRequest::CLIENT_TYPE_POTENTIAL;
                    break;

                case 'NOT_A_CLIENT':
                    $oRequest->client_type = LandingRequest::CLIENT_TYPE_NOT_A_CLIENT;
                    break;

                case 'UNKNOWN':
                    $oRequest->client_type = LandingRequest::CLIENT_TYPE_NEW;
                    break;

                default:
                    $oRequest->client_type = -2; // TODO проверять, что таких нет
            }

            DB::insert('
                INSERT INTO
					landing_requests (id, user_id, agent_id, user_name, user_phone,
					 processed_date, comment, client_type, created_at, updated_at)
			 	VALUES
			 		(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
                , [
                    $this->clearValue($oRequest->id),
                    $this->clearValue($oRequest->user_id),
                    $this->clearValue($oRequest->landing_owner_id),
                    $this->clearValue($oRequest->name),
                    $this->clearValue($oRequest->phone),
                    $this->clearValue($oRequest->processed_date),
                    $this->clearValue($oRequest->comment),
                    $this->clearValue($oRequest->client_type),
                    $this->clearValue($oRequest->added_date),
                    date('Y-m-d H:i:s'),
                ]
            );
        }
    }

    private function migrateSentDocs() {
        $aDocs = DB::connection('cenomarket')->select('
            select 
                findef_sent_docs_types.name, 
                findef_sent_docs.* 
            from 
                findef_sent_docs 
            left outer join 
                findef_sent_docs_types 
                    on 
                    findef_sent_docs.doc_type = findef_sent_docs_types.id
        ');

        foreach($aDocs as $oDoc) {
            $temp = explode('|', $oDoc->images);
            if (count($temp) >= 1) {
                $file_path = $temp[0];
                $file_path = str_replace('/uploads/images/fddocs/', '/upload/sentDocs/', $file_path);
            }
            else {
                $file_path = '';
            }

            $oDoc->file_path = $file_path;

            DB::insert('
                INSERT INTO
					sent_docs (id, client_id, sender_id, name, template_type, recipient,
					recipient_address, sender_notation, file_path, oisu_handed_date, created_at, updated_at)
			 	VALUES
			 		(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
                , [
                    $this->clearValue($oDoc->id),
                    $this->clearValue($oDoc->client_id),
                    $this->clearValue($oDoc->lawyer_id),
                    $this->clearValue($oDoc->name),
                    $this->clearValue($oDoc->doc_template_type),
                    $this->clearValue($oDoc->city),
                    $this->clearValue($oDoc->address),
                    $this->clearValue($oDoc->notation),
                    $this->clearValue($oDoc->file_path),
                    $this->clearValue($oDoc->handed_date),
                    $this->clearValue($oDoc->added_date),
                    date('Y-m-d H:i:s'),
                ]
            );
        }
    }

    private function clearValue($value) {
        return is_null($value) ? '' : $value;
    }

    private function prepareDB() {
        Artisan::call('migrate:refresh', ['--seed' => 'default']);

        $cmd = ('php C:\OpenServer\domains\people.local\artisan migrate:refresh --seed');
        system($cmd);
    }

    private function loadDbConnections() {
        $conn = [
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => 'social',
            'username'  => 'root',
            'password'  => '',
            'charset'   => 'utf8',
            'collation' => 'utf8_general_ci',
            'prefix'    => '',
        ];

        Config::set('database.connections.cenomarket', $conn);

        $conn = [
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => 'people',
            'username'  => 'root',
            'password'  => '',
            'charset'   => 'utf8',
            'collation' => 'utf8_general_ci',
            'prefix'    => '',
        ];

        Config::set('database.connections.people', $conn);

        $conn = [
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => 'docflow',
            'username'  => 'root',
            'password'  => '',
            'charset'   => 'utf8',
            'collation' => 'utf8_general_ci',
            'prefix'    => '',
        ];

        Config::set('database.connections.docflow', $conn);
    }
}
