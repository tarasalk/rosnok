<script>
    $(function() {
        $('#current_role').click(function() {
            @if(isset($aUserRole) && count($aUserRole) > 0)
            $('#role_list').show();
            @endif
        });

        $('.user_bar_overlay').click(function() {
            $('.user_bar').hide();
            $('.user_bar_overlay').hide();
            $('#role_list').hide();
        });

        $('.icon_wrapper').click(function() {
            if ($('.user_bar').css('display') == 'none') {
                $('.user_bar').css('display', 'flex');
                $('.user_bar_overlay').show();
            }
            else {
                $('.user_bar').hide();
                $('.user_bar_overlay').hide();
            }
        });

        currentRoleUserId = $.cookie('currentRoleUserId');
        if (currentRoleUserId == '{{Auth::user()->id}}')
            changeMenu($.cookie('currentRole'));
        else
            changeMenu('');
    });

    function changeMenu(role) {
        var menu;
        switch (role) {
            case 'Агент':
                menu = "#agentMenu";
                break;

            case 'Админ':
                menu = "#adminMenu";
                break;

            case 'Клиент':
                menu = "#clientMenu";
                break;

            case 'ОИС-Ю':
                menu = "#oisuMenu";
                break;

            case 'ОИС-Ш':
                menu = "#oissMenu";
                break;

            case 'ОВС':
                menu = "#ovsMenu";
                break;

            case 'Юрист':
                menu = "#lawyerMenu";
                break;

            default:
                menu = '#userMenu';
                role = 'Пользователь';
        }
        $('#role_list').hide();
        $('.menu div[data-visibility=hide]').hide();

        $(menu).show();

        $.cookie('currentRole', role, {path: '/'});
        $.cookie('currentRoleUserId', '{{Auth::user()->id}}', {path: '/'});

        $('.current_role .name').text(role);
    }
</script>
<div class="user_bar_overlay"></div>
<div class="top">
    <div class="icon_wrapper">
        <div class="icon"></div>
    </div>
    <div class="user_bar">
        <div class="user_info">
            <div class="name">{{Auth::user()->getName()}}</div>
            <div class="role">
                <div class="role_wrapper">
                    <div class="current_role" id="current_role"><span class="name">Пользователь</span><span class="icon">✓</span></div>
                    <div class="role_list" id="role_list">
                        <div onclick="changeMenu();">Пользователь</div>
                        @if(isset($aUserRole) && count($aUserRole) > 0)
                            @foreach($aUserRole as $oUserRole)
                                <div onclick="changeMenu('{{$oUserRole->rus_name}}');">{{$oUserRole->rus_name}}</div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="id">ID: {{Auth::user()->id}}</div>
            </div>
        </div>
        <a class="link profile" onclick="toastr.info('В разработке');">
            <span></span>
            <div>профиль</div>
        </a>
        <a class="link exit" onclick="window.location.href='/auth/logout'">
            <span></span>
            <div>выйти</div>
        </a>
    </div>
</div>
<div class="menu">
    @foreach($cabinetMenu->roots() as $item)
        <div data-menu="cabinet_menu" @if($item->group_id != 'userMenu') data-visibility="hide" style="display: none;" @endif id="{{$item->group_id}}">
            @foreach($item->children() as $children_item)
                <div class="level1">
                    <a href="{!! $children_item->url() !!}">
                        <div class="icon {{$children_item->class_icon}}"></div>
                        <div class="title">
                            {!! $children_item->title !!}
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    @endforeach
</div>