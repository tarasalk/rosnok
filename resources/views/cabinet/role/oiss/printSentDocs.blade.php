@extends('cabinet.extends.main')
@section('head')
    <link rel="stylesheet" href="{{ URL::asset('thema/css/page/operator.css') }}">
@endsection
@section('content_wrapper')
    <div class="title">
        Печать исходящих
    </div>
    <div class="content">
        <script>
            var currentEditDocId;

            $(function() {
                $('#operatorSentDocs').dataTable({
                    "order": [[ 0, "desc" ]]
                });

                $(".fancybox").fancybox();
            });

            function openSenderNotation(notation) {
                $sender_notation_modal = $('#sender_notation_modal');
                $sender_notation_modal.find('div[name=notation]').text(notation);
                $sender_notation_modal.show();
            }

            function openEditDoc(doc_id) {
                currentEditDocId = doc_id;
                $('.operatorEditSentDoc').show();
            }

            function confirmEditDoc() {
                $operatorEditSentDoc = $('.operatorEditSentDoc');

                recipient = $operatorEditSentDoc.find('input[name=recipient]').val();
                recipient_address = $operatorEditSentDoc.find('input[name=recipient_recipient]').val();
                name = $operatorEditSentDoc.find('input[name=name]').val();

                $.ajax({
                    url: "{{URL::route('oisuSentDocsEdit')}}",
                    type: "POST",
                    data: {
                        doc_id: currentEditDocId,
                        recipient: recipient,
                        recipient_address: recipient_address,
                        name: name
                    },
                    success: function (r) {
                        if (r.status = 'ok') {
                            toastr.success('Успешно');
                            $operatorEditSentDoc.hide();
                        }
                        else {
                            toastr.error('ошибка');
                        }
                    }
                });
            }

            function handDoc(doc_id, element) {
                $.ajax({
                    url: "{{URL::route('oisuSentDocsHand')}}",
                    type: "POST",
                    data: {
                        doc_id: doc_id
                    },
                    success: function (r) {
                        if (r.status = 'ok') {
                            toastr.success('Успешно');

                            $(element).parent().html(r.date);
                        }
                        else {
                            toastr.error('ошибка');
                        }
                    }
                });
            }
        </script>
        <div class="operatorEditSentDoc" style="display: none">
            <div class="row addressBlock">
                <div>
                    <div>Кому</div>
                    <div><input type="text" name="recipient" placeholder="Название получателя"></div>
                </div>
                <div>
                    <div>Куда</div>
                    <div><input type="text" name="recipient_recipient" placeholder="Адрес получателя"></div>
                </div>
            </div>
            <div class="row docNameBlock">
                <div>Название документа</div>
                <div><input type="text" name="name" placeholder="Тип документа"></div>
            </div>
            <div class="row buttons">
                <button class="btn btn_middle biz" onclick="confirmEditDoc()">Сохранить</button>
                <button class="btn btn_middle money" onclick="$('.operatorEditSentDoc').hide();">Отмена</button>
            </div>
        </div>
        <div style="margin-top: 10px;">
            <table id="operatorSentDocs" class="gray">
                <thead>
                    <tr>
                        <th>Документ</th>
                        <th>Клиент</th>
                        <th>Получатель</th>
                        <th>Адрес получателя</th>
                        <th>Юрист</th>
                        <th>Добавлено</th>
                        <th>Обработка</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($aDocs as $oDoc)
                        <tr>
                            <td>{{$oDoc->name != '' ? $oDoc->name: 'не указан'}}</td>
                            <td>{{$oDoc->fio}}</td>
                            <td>{{$oDoc->recipient}}</td>
                            <td>{{$oDoc->recipient_address}}</td>
                            <td>{{$oDoc->sender_id}}</td>
                            <td>{{$oDoc->created_at}}</td>
                            <td>
                                @if ($oDoc->oisu_handed_date != '0000-00-00 00:00:00')
                                    {{$oDoc->oisu_handed_date}}
                                @else
                                    <a onclick="openEditDoc({{$oDoc->id}})">обработать</a><br>
                                    <a onclick="handDoc({{$oDoc->id}}, this)">отправить</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div id="sender_notation_modal" class="modal_overlay" onclick="closeModal(this, event);">
            <div class="modal_window">
                <button class="m_close" onclick="closeModal(this);">&times;</button>
                <div class="modal_header border_biz">Примечание</div>
                <div class="modal_body">
                    <div name="notation"></div>
                    <div class="modal_footer">
                        <button class="btn btn_middle biz" onclick="closeModal(this);">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="download_doc_modal" class="modal_overlay" onclick="closeModal(this, event);">
            <div class="modal_window">
                <button class="m_close" onclick="closeModal(this);">&times;</button>
                <div class="modal_header border_biz">Файлы</div>
                <div class="modal_body">
                    <div name="files"></div>
                    <div class="modal_footer">
                        <button class="btn btn_middle biz" onclick="closeModal(this);">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

