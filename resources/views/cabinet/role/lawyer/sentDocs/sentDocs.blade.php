@extends('cabinet.extends.main')
@section('head')
    <link rel="stylesheet" href="{{ URL::asset('thema/css/page/lawyer.css') }}">
@endsection
@section('content_wrapper')
    <div class="title">
        Исходящие документы
    </div>
    <div class="content">
        <script>
            $(function() {
                $('#lawyerSentDocs').dataTable({
                    "order": [[ 6, "desc" ]]
                });

                $(".fancybox").fancybox();
            });

            function openSenderNotation(notation) {
                $('#sender_notation_modal div[name=notation]').text(notation);
                $('#sender_notation_modal').show();
            }
        </script>
        <div>
            <table id="lawyerSentDocs" class="gray">
                <thead>
                    <tr>
                        <th>№</th>
                        <th>Клиент</th>
                        <th>Документ</th>
                        <th>Получатель</th>
                        <th>Адрес получателя</th>
                        <th>Тип</th>
                        <th>Добавлено</th>
                        <th>Отправлено</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($aDocs as $oDoc)
                    <tr>
                        <td class="num">
                            <div>{{$oDoc->id}}</div>
                            @if (!empty($oDoc->file_path))
                                @if(strripos($oDoc->file_path, '.jpg'))
                                    <div class="icon_info_blue fancybox pointer" title="файлы" href="{{$oDoc->file_path}}"></div>
                                @else
                                    <div class="icon_info_blue pointer" title="файлы">
                                        <a class="maximumLink" target='blank' href="{{$oDoc->file_path}}"></a>
                                    </div>
                                @endif
                            @endif
                        </td>
                        <td>{{$oDoc->fio}}</td>
                        <td>{{$oDoc->name}}</td>
                        <td>{{$oDoc->recipient}}</td>
                        <td>{{$oDoc->recipient_address}}</td>
                        <td>
                            @if ($oDoc->template_type == 'lawyer')
                                Ю
                            @elseif ($oDoc->template_type == 'template')
                                Ш
                            @else
                                ХЗ
                            @endif
                        </td>
                        <td>{{$oDoc->created_at}}</td>
                        <td>{{$oDoc->oisu_handed_date}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div id="sender_notation_modal" class="modal_overlay" onclick="closeModal(this, event);">
            <div class="modal_window">
                <button class="m_close" onclick="closeModal(this);">&times;</button>
                <div class="modal_header border_biz">Примечание агента</div>
                <div class="modal_body">
                    <div name="notation"></div>
                    <div class="modal_footer">
                        <button class="btn btn_middle biz" onclick="closeModal(this);">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="download_doc_modal" class="modal_overlay" onclick="closeModal(this, event);">
            <div class="modal_window">
                <button class="m_close" onclick="closeModal(this);">&times;</button>
                <div class="modal_header border_biz">Файлы</div>
                <div class="modal_body">
                    <div name="files"></div>
                    <div class="modal_footer">
                        <button class="btn btn_middle biz" onclick="closeModal(this);">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

