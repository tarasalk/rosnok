@extends('cabinet.extends.main')
@section('head')
    <link rel="stylesheet" href="{{ URL::asset('thema/css/page/lawyer.css') }}">
@endsection
@section('content_wrapper')
    <div class="title">
        Добавление исходящего документа
    </div>
    <div class="content">
        <style>
            .ui-autocomplete {
                min-width: 450px !important;
            }

            table {
                width: 100%;
            }
        </style>
        <script>
            var currentClientId = null;
            var client_table = null;

            $(function() {
                $('input[name="searchClientName"]').each(function () {
                    $(this).autocomplete({
                        minLength: 1,
                        source: function (req, response) {
                            $.ajax({
                                url: "{{URL::route('lawyerSearchClients')}}",
                                type: "POST",
                                data: {
                                    searchFio: req.term
                                },
                                success: function(r) {
                                    response($.map(r.aClients, function(item) {
                                        return {
                                            id: item.client_id,
                                            value: item.fio
                                        };
                                    }));
                                }
                            });
                        },
                        select: function (ev, ui) {
                            $(this).val(ui.item.value);
                            currentClientId = ui.item.id;

                            $client_table = $('#clientSendDocsTable');

                            if (client_table != null) {
                                client_table.fnDestroy();
                                client_table = null;
                            }

                            client_table = $client_table.dataTable({
                                "processing": true,
                                "ajax": {
                                    "url": '{{URL::route('lawyerGetClientSentDocs')}}' + '/' + ui.item.id
                                },
                                "columns": [
                                    { "data": "id" },
                                    { "data": "name" },
                                    { "data": "recipient" },
                                    { "data": "recipient_address" },
                                    { "data": "template_type" },
                                    { "data": "created_at" },
                                    { "data": "oisu_handed_date" }
                                ],
                                "order": [[ 5, "desc" ]],
                                "iDisplayLength": -1,
                                "createdRow": function ( row, data, index ) {
                                    template_type = data.template_type;
                                    switch (template_type) {
                                        case 'template':
                                            template_type_rus = 'Ш';
                                            break;

                                        case 'lawyer':
                                            template_type_rus = 'Ю';
                                            break;

                                        default:
                                            template_type_rus = 'ХЗ';
                                    }

                                    $('td', row).eq(4).html(template_type_rus);
                                }
                            });

                            $('.clientSendDocs').removeClass('hide');

                            // отменить очистку поля поиска
                            ev.preventDefault();
                        }
                    });
                });

                $('input[type=file]').fileupload({
                    dataType: 'json',
                    url: "{{URL::route('sentDocsUpload')}}",
                    done: function (e, data) {
                        $deleteFileBlock = $('#deleteFileBlock');
                        $deleteFileBlock.empty();

                        r = data.result;

                        if (r.status == 'ok') {
                            $('#' + $(this).attr('name')).val(r.response.path);

                            uploadFileTitle = r.response.title;
                            $deleteFileBlock.append(
                                '<a class="row" style="font-size: 15px" onclick="deleteFile('+"uploadFileTitle"+')">Удалить этот документ, чтобы прикрепить другой</a>'
                            );
                        }
                        else if (r.errors == 'undefined_mime') {
                            toastr.error('Неподдерживаемый тип документа');
                        }
                        else {
                            toastr.error('Ошибка');
                        }
                    }
                });
            });

            function addDoc() {
                notation = $('textarea[name=notation]').val();
                file = $('#file').val();

                if (currentClientId == null) {
                    toastr.info('Пожалуйста, выберите клиента из выпадающего списка.');
                    return;
                }

                if (file == '') {
                    toastr.info('Выберите и прикрепите юридический документ перед отправкой.');
                    return;
                }

                $.ajax({
                    url: "{{URL::route('lawyerSentDocsAdd')}}",
                    type: "POST",
                    data: {
                        client_id: currentClientId,
                        notation: notation,
                        file: file
                    },
                    success: function (r) {
                        toastr.success('Успешно');
                        setTimeout(function() {location.reload()}, 2000);
                    }
                });
            }

            function deleteFile(uploadFileTitle) {
                $.ajax({
                    url: "{{URL::route('sentDocsDelete')}}",
                    type: "POST",
                    data: {
                        doc_name: uploadFileTitle
                    },
                    success: function (r) {
                        if (r.status == 'ok')
                            toastr.success('Успешно');
                        else
                            toastr.error('Ошибка');

                        $('#deleteFileBlock').empty();
                    }
                });
            }
        </script>
        <div class="row big lawyerSentDocsAddContainer">
            <div class="docBlock">
                <div class="clientSearchBlock">
                    <div>
                        <div>Поиск</div>
                        <div><input type="text" name="searchClientName" placeholder="Имя Фамилия"></div>
                    </div>
                    <div>
                        <div>Файл документа</div>
                        <div>
                            <input type="file" name="file">
                            <input type="hidden" id="file" name="file">
                        </div>
                    </div>
                    <div id="deleteFileBlock"></div>
                </div>
                <div class="notationBlock">
                    <div>Примечание</div>
                    <div><textarea name="notation"></textarea></div>
                </div>
            </div>
            <div class="buttons"><button class="btn btn_middle biz" onclick="addDoc()">Сохранить</button></div>
        </div>
        <div class="row clientSendDocs hide">
            <h3>Исходящие клиента</h3>
            <table id="clientSendDocsTable" class="gray">
                <thead>
                    <tr>
                        <th>№</th>
                        <th>Документ</th>
                        <th>Получатель</th>
                        <th>Адрес получателя</th>
                        <th>Тип</th>
                        <th>Добавлено</th>
                        <th>Отправлено</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

