
@extends('cabinet.extends.main')
@section('head')
    <link rel="stylesheet" href="{{ URL::asset('thema/css/page/lawyer.css') }}">
@endsection
@section('content_wrapper')
    <script>
        function redirectInboxDocs(client_id) {
            location.href = "{{URL::route('lawyerInboxDocs')}}" + '/' + client_id;
        }
    </script>
    <div class="title">
        Клиенты
    </div>
    <div class="content">
        <div class="lawyerClientList">
            <div class="row menu_horizontal">
                <a href="{{URL::route('lawyerClients', ['category' => 'all'])}}">Все ({{$aClientsCategoriesCount->all}})</a>
                <a href="{{URL::route('lawyerClients', ['category' => 'without_delay'])}}">Без просрочки ({{$aClientsCategoriesCount->withoutDelay}})</a>
                <a href="{{URL::route('lawyerClients', ['category' => 'not_pay_rate'])}}">Неактивированные ({{$aClientsCategoriesCount->notPayRate}})</a>
                <a href="{{URL::route('lawyerClients', ['category' => 'soon_pay'])}}">Скоро платить ({{$aClientsCategoriesCount->soonPay}})</a>
                <a href="{{URL::route('lawyerClients', ['category' => 'delay_less_1_month'])}}">Просрочка < 1 мес. ({{$aClientsCategoriesCount->delayLess1Month}})</a>
                <a href="{{URL::route('lawyerClients', ['category' => 'delay_more_1_month'])}}">Просрочка > 1 мес. ({{$aClientsCategoriesCount->delayMore1Month}})</a>
            </div>
            <div class="row">
                {!! $aClients->getLinks() !!}
            </div>
            @foreach($aClients as $oClient)
                <div class="clientBlock @if ($oClient->pay_day_diff < 0) red @elseif ($oClient->pay_day_diff < 7) yellow @endif">
                    <div class="avatar"><img src="{{$oClient->base->avatar or ''}}"></div>
                    <div class="info">
                        <div>
                            <div>
                                <a href="{{URL::route('agentClientCab', ['user_id' => $oClient->client_id])}}">
                                    {{$oClient->fio}}
                                </a>
                            </div>
                            @if(isset($oClient->phone))
                                <div>Тел.: {{$oClient->phone->country}}{{$oClient->phone->phone}}</div>
                            @endif
                            <div>ID: {{$oClient->client_id}}</div>
                        </div>
                        <div>
                            <div>Тариф: {{$oClient->rate_pay}} руб.</div>
                            <div>В работе: {{$oClient->in_work_month}} мес. {{$oClient->in_work_day}} дн.</div>
                            <div>Кооп. участок: {{$oClient->office->office_name or '-'}}</div>
                        </div>
                        <div class="TODO">
                            Следующий по очереди взнос: 2-й До окончания оплаченного месяца: 6 дн
                        </div>
                    </div>
                    <div class="buyBtn">
                        <div>
                            @if($oClient->pay_day_diff > 0)
                                Осталось: {{$oClient->pay_day_diff}} дн.
                            @else
                                Просрочка: {{abs($oClient->pay_day_diff)}} дн.
                            @endif
                        </div>
                        @if ($oClient->inboxDocs->count() > 0)
                            <button class="btn btn_middle money" onclick="redirectInboxDocs({{$oClient->client_id}})">
                                Входящие {{$oClient->inboxDocs->count() - $oClient->inboxDocsLawyerReadCount}}/{{$oClient->inboxDocs->count()}}
                            </button>
                        @endif
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

