{{--*/ use \App\Models\InboxDocs /*--}}

@extends('cabinet.extends.main')
@section('head')
    <link rel="stylesheet" href="{{ URL::asset('thema/css/page/lawyer.css') }}">
@endsection
@section('content_wrapper')
    <style>
        #lawyerInboxDocs > TBODY > TR > TD {
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            max-width: 12em;
            vertical-align: middle;
        }

        .childTable TD {
            vertical-align: middle;
            white-space: normal;
        }
    </style>
    <div class="title">
        Входящие документы
    </div>
    <div class="content">
        <script>
            $(function() {
                var $lawyerInboxDocs = $('#lawyerInboxDocs');
                var table = $lawyerInboxDocs.DataTable({
                    "order": [[5, 'desc']]
                });

                $lawyerInboxDocs.find('tbody').on('click', 'TR > TD:first-child', function () {
                    var tr = $(this).closest('tr');
                    var row = table.row( tr );

                    if ( row.child.isShown() ) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    }
                    else {
                        // Open this row
                        row.child( format(row.data()) ).show();
                        tr.addClass('shown');
                    }
                } );

                $(".fancybox").fancybox();
            });

            function format ( d ) {
                // `d` is the original data object for the row
                return '<table class="childTable gray" style="padding-left:50px;">'+
                            '<tr>'+
                                '<td>Название:</td>'+
                                '<td>'+d[4]+'</td>'+
                            '</tr>'+
                        '</table>';
            }

            function openSenderNotation(notation) {
                var $senderNotationModal = $('#sender_notation_modal');
                $senderNotationModal.find('div[name=notation]').text(notation);
                $senderNotationModal.show();
            }

            function sendLawyerRead(doc_id, thiselement) {
                $(thiselement).prop('disabled', true);

                sendStatus = Number($(thiselement).is(':checked'));

                $.ajax({
                    url: "{{URL::route('lawyerInboxDocLawyerRead')}}",
                    type: "POST",
                    data: {
                        doc_id: doc_id,
                        sendStatus: sendStatus
                    },
                    success: function(r) {
                        if (r.status == 'ok')
                            toastr.info('Успешно');
                        else
                            toastr.error('Ошибка');
                    }
                });
            }
        </script>
        <div>
            <table id="lawyerInboxDocs" class="gray">
                <thead>
                    <tr>
                        <th></th>
                        <th>№</th>
                        <th>Агент</th>
                        <th>Клиент</th>
                        <th>Название</th>
                        <th>Передан</th>
                        <th>Прочитано</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($aDocs as $oDoc)
                    <tr>
                        <td class="details-control"
                            @if     ($oDoc->status == InboxDocs::status_denied)
                                style="box-shadow: -5px 0 0 0 #e31e25;"
                            @elseif ($oDoc->status == InboxDocs::status_rebuke)
                                style="box-shadow: -5px 0 0 0 #ef7f1b;"
                            @elseif ($oDoc->status == InboxDocs::status_accepted)
                                style="box-shadow: -5px 0 0 0 #61b239;"
                            @endif>
                        </td>
                        <td class="num">
                            <div>{{$oDoc->id}}</div>
                            @if (!empty($oDoc->sender_notation))
                                <div class="icon_glaz_blue pointer" title="примечание агента" onclick="openSenderNotation('{{$oDoc->sender_notation}}');"></div>
                            @endif
                            @if (!empty($oDoc->file_path))
                                @if(strripos($oDoc->file_path, '.jpg'))
                                    <div class="icon_info_blue fancybox pointer" title="файлы" href="{{$oDoc->file_path}}"></div>
                                @else
                                    <div class="icon_info_blue pointer" title="файлы">
                                        <a class="maximumLink" target='blank' href="{{$oDoc->file_path}}"></a>
                                    </div>
                                @endif
                            @endif
                        </td>
                        <td>{{$oDoc->agent_name}}</td>
                        <td>
                            <a href="{{URL::route('agentClientCab', ['client_id' => $oDoc->client_id])}}">
                                {{$oDoc->client_name}}
                            </a>
                        </td>
                        <td>{{$oDoc->name}}</td>
                        <td>{{$oDoc->event->created_at}}</td>
                        <td>
                            <input type="checkbox" onchange="sendLawyerRead('{{$oDoc->id}}', this)" @if($oDoc->lawyer_read) checked disabled @endif>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div id="sender_notation_modal" class="modal_overlay" onclick="closeModal(this, event);">
            <div class="modal_window">
                <button class="m_close" onclick="closeModal(this);">&times;</button>
                <div class="modal_header border_biz">Примечание агента</div>
                <div class="modal_body">
                    <div name="notation"></div>
                    <div class="modal_footer">
                        <button class="btn btn_middle biz" onclick="closeModal(this);">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

