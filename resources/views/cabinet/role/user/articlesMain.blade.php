@extends('cabinet.extends.main')

@section('head')
    <link rel="stylesheet" href="{{ URL::asset('thema/css/page/articles.css') }}">
@endsection

@section('content_wrapper')
    <div class="title">
        Админка. Статьи
    </div>
    <div class="content">
        <script>
            $(function() {
                $("div").find("input[type='hidden']").hide();


                $("a.fancybox").fancybox();

                $('input[type=file]').fileupload({
                    dataType: 'json',
                    url: "{{URL::route('articleImgUpload')}}",
                    done: function (e, data) {
                        element = $(this).closest(".row").find("input[type='hidden']");
                        element.val(data.result.response.path);
                    }
                });
            });

            function openCreateArticle() {
                $('#createArticleBlock').show();
                $('#btnOpenCreateArticle').hide();
            }

            function closeCreateArticle() {
                $('#createArticleBlock').hide();
                $('#btnOpenCreateArticle').show();
            }

            function createArticle() {
                $.ajax({
                    url: "{{URL::route('articleCreate')}}",
                    type: "POST",
                    data: $('#formCreateArticle').serialize(),
                    success: function(r) {
                        window.location.reload();
                    }
                });
            }

            function openEditArticle(id) {
                $('#eab' + id).show();
                $('#ab' + id).hide();
            }

            function closeEditArticle(id) {
                $('#eab' + id).hide();
                $('#ab' + id).show();
            }

            function editArticle(id) {
                $.ajax({
                    url: "{{URL::route('articleEdit')}}",
                    type: "POST",
                    data: $('#formEditArticle' + id).serialize(),
                    success: function(r) {
                        window.location.reload();
                    }
                });
            }
        </script>
        <div style="margin-bottom: 10px;">
            <button id="btnOpenCreateArticle" class="btn btn_middle biz" onclick="openCreateArticle();">Добавить</button>
        </div>
        <div class="adminMainArticles">
            <div class="createArticleBlock" id="createArticleBlock" style="display: none;">
                <form id="formCreateArticle">
                    <div class="row full">
                        <div>Имя</div>
                        <div><input type="text" name="name"></div>
                    </div>
                    <div class="row full">
                        <div>Заголовок</div>
                        <div><input type="text" name="title"></div>
                    </div>
                    <div class="row full">
                        <div>текст</div>
                        <div><textarea name="text"></textarea></div>
                    </div>
                    <div class="row">
                        <div>превью</div>
                        <div>
                            <input type="file" name="imgSmall">
                            <input type="hidden" name="imgSmall">
                        </div>
                    </div>
                    <div class="row">
                        <div>картинка</div>
                        <div>
                            <input type="file" name="imgBig">
                            <input type="hidden" name="imgBig">
                        </div>
                    </div>
                    <div class="row radio">
                        <div>тип</div>
                        <div>
                            <label for="SUD"><input id="SUD" name="type" value="1" type="radio" checked>С.У.Д</label>
                            <label for="news"><input id="news" name="type" value="2" type="radio">Новости и аналитика</label>
                        </div>
                    </div>
                    <div class="buttons">
                        <button type="button" class="btn btn_middle biz" onclick="createArticle();">Добавить</button>
                        <button type="button" class="btn btn_middle biz" onclick="closeCreateArticle();">Отмена</button>
                    </div>
                </form>
            </div>
            <div class="articlesList">
                @foreach($aArticles as $oArticle)
                    <div class="articleBlock" id="ab{{$oArticle->id}}">
                        <div class="row">
                            <div>Имя</div>
                            <div>{{$oArticle->name}}</div>
                        </div>
                        <div class="row">
                            <div>Заголовок</div>
                            <div>{{$oArticle->title}}</div>
                        </div>
                        <div class="row small">
                            <div>текст</div>
                            <div>{!!str_replace("\n", "<br><br>", $oArticle->text)!!}</div>
                        </div>
                        <div class="row">
                            <div>Превью</div>
                            <div>
                                <a class="fancybox" href="{{"/".$oArticle->imgSmall}}">
                                    <img src="{{"/".$oArticle->imgSmall}}">
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div>Картинка</div>
                            <div>
                                <a class="fancybox" href="{{"/".$oArticle->imgBig}}">
                                    <img src="{{"/".$oArticle->imgBig}}">
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div>Тип</div>
                            <div>
                                @if ($oArticle->type == 1)
                                    С.У.Д.
                                @elseif($oArticle->type == 2)
                                    Новости и аналитика
                                @else
                                    Неизвестно
                                @endif
                            </div>
                        </div>
                        <div class="buttons">
                            <button class="btn btn_middle biz" onclick="openEditArticle({{$oArticle->id}})">Редактировать</button>
                        </div>
                    </div>
                    <div class="editArticleBlock" id="eab{{$oArticle->id}}" style="display: none;">
                        <form id="formEditArticle{{$oArticle->id}}">
                            <div class="row full">
                                <div>Имя</div>
                                <div>
                                    <input type="text" name="name" value="{{$oArticle->name}}">
                                    <input type="hidden" name="id" value="{{$oArticle->id}}">
                                </div>
                            </div>
                            <div class="row full">
                                <div>Заголовок</div>
                                <div><input type="text" name="title" value="{{$oArticle->title}}"></div>
                            </div>
                            <div class="row full">
                                <div>текст</div>
                                <div><textarea name="text">{{$oArticle->text}}</textarea></div>
                            </div>
                            <div class="row">
                                <div>превью</div>
                                <div>
                                    <input type="file" name="imgSmall">
                                    <input type="hidden" name="imgSmall" value="{{$oArticle->imgSmall}}">
                                </div>
                            </div>
                            <div class="row">
                                <div>картинка</div>
                                <div>
                                    <input type="file" name="imgBig">
                                    <input type="hidden" name="imgBig" value="{{$oArticle->imgBig}}">
                                </div>
                            </div>
                            <div class="row radio">
                                <div>тип</div>
                                <div>
                                    <label for="SUD{{$oArticle->id}}"><input id="SUD{{$oArticle->id}}" name="type" value="1" type="radio" @if ($oArticle->type == 1) checked @endif>С.У.Д</label>
                                    <label for="news{{$oArticle->id}}"><input id="news{{$oArticle->id}}" name="type" value="2" type="radio" @if ($oArticle->type == 2) checked @endif>Новости и аналитика</label>
                                </div>
                            </div>
                            <div class="buttons">
                                <button type="button" class="btn btn_middle biz" onclick="editArticle({{$oArticle->id}});">Сохранить</button>
                                <button type="button" class="btn btn_middle biz" onclick="closeEditArticle({{$oArticle->id}});">Отмена</button>
                            </div>
                        </form>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection