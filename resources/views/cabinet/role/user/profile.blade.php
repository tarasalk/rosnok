@extends('cabinet.extends.main')

@section('head')
    <link rel="stylesheet" href="{{ URL::asset('thema/css/page/profile.css') }}">
@endsection

@section('content_wrapper')
    <div class="title">
        Профиль
    </div>
    <div class="content">
        <div class="profileContainer">
            <div class="row">
                <div>Имя</div>
                <div><input type="text"></div>
            </div>
            <div class="row">
                <div>Фамилия</div>
                <div><input type="text"></div>
            </div>
            <div class="row">
                <div>Отчество</div>
                <div><input type="text"></div>
            </div>
            <div class="row">
                <div>Дата рождения </div>
                <div><input type="text"></div>
            </div>
            <div class="row">
                <div>Пол</div>
                <div><input type="text"></div>
            </div>
        </div>
    </div>
@endsection