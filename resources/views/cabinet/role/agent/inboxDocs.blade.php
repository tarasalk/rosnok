{{--*/ use \App\Models\InboxDocs /*--}}

@extends('cabinet.extends.main')
@section('head')
    <link rel="stylesheet" href="{{ URL::asset('thema/css/page/agent.css') }}">
@endsection
@section('content_wrapper')
    <div class="title">
        Входящие документы
    </div>
    <div class="content">
        <div class="agentInboxDocs">
            <script>
                $(function() {
                    $('#agentDocsFromClients').dataTable({
                        "order": [[ 4, "desc" ]],
                    });

                    $(".fancybox").fancybox();

                    $('input[type=file]').fileupload({
                        dataType: 'json',
                        url: "{{URL::route('inboxDocsUpload')}}",
                        done: function (e, data) {
                            if (data.result.status == 'ok') {
                                $('#' + $(this).attr('name')).val(data.result.response.path);
                            }
                            else {
                                if (data.result.errors == 'undefined_mime')
                                    toastr.error('Неподдерживаемый тип документа');
                            }
                        }
                    });
                });

                function addDoc() {
                    var $docAddBlock = $('.docAddBlock');
                    var client_id = $docAddBlock.find('select[name=myClients] option:selected').val();
                    var notation = $docAddBlock.find('textarea[name=notation]').val();
                    var file = $('#file').val();

                    if (!$.isNumeric(client_id)) {
                        toastr.error('Ошибка. Попробуйте позднее');
                        return;
                    }

                    if (file == '') {
                        toastr.info('Загрузите документ');
                        return;
                    }

                    $.ajax({
                        url: "{{URL::route('agentInboxDocsAdd')}}",
                        type: "POST",
                        data: {
                            client_id: client_id,
                            notation: notation,
                            file: file
                        },
                        success: function(r) {
                            if (r.status == 'ok') {
                                toastr.success('Документ успешно добавлен');
                                setTimeout(function() {location.reload();}, 1000);
                            }
                            else {
                                toastr.error('Ошибка');
                                console.log(r.error);
                            }
                        }
                    });
                }

                function cancelAddDoc() {
                    $('.docAddBlock').hide();
                    $('#btnOpenDocAdd').show();
                }

                function openDocAdd() {
                    $('.docAddBlock').show();
                    $('#btnOpenDocAdd').hide();
                }

                function openOperatorNotation(notation) {
                    var $operatorNotationModal = $('#operatorNotationModal');
                    $operatorNotationModal.find('div[name=notation]').text(notation);
                    $operatorNotationModal.show();
                }
            </script>
            <div><button class="btn btn_middle money" id="btnOpenDocAdd" onclick="openDocAdd()">Добавить</button></div>
            <div class="docAddBlock">
                <div class="title">Добавление</div>
                <div>
                    <select name="myClients">
                        @foreach($aClients as $aClient)
                            <option value="{{$aClient->user_id}}">{{$aClient->base->last_name}} {{$aClient->base->first_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div><textarea name="notation" placeholder="примечание"></textarea></div>
                <div>
                    <input type="file" name="file" class="smallInput">
                    <input type="hidden" id="file" name="file">
                </div>
                <div class="buttons">
                    <button class="btn btn_middle biz" onclick="addDoc()">Добавить</button>
                    <button class="btn btn_middle money" onclick="cancelAddDoc()">Отмена</button>
                </div>
            </div>
            <div class="agentDocsFromClients">
                <table id="agentDocsFromClients" class="gray">
                    <thead>
                        <tr>
                            <th>№</th>
                            <th>ФИО пайщика</th>
                            <th>Название</th>
                            <th>Примечание</th>
                            <th>Добавлен</th>
                            <th>Обработан</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($aDocs as $oDoc)
                            <tr>
                                <td class="num"
                                    @if     ($oDoc->status == InboxDocs::status_denied)
                                        style="box-shadow: -5px 0 0 0 #e31e25;"
                                    @elseif ($oDoc->status == InboxDocs::status_rebuke)
                                        style="box-shadow: -5px 0 0 0 #ef7f1b;"
                                    @elseif ($oDoc->status == InboxDocs::status_accepted)
                                        style="box-shadow: -5px 0 0 0 #61b239;"
                                    @endif
                                >
                                    <div>{{$oDoc->id}}</div>
                                    @if (!empty($oDoc->operator_notation))
                                        <div class="icon_glaz_blue pointer" title="примечание оператора" onclick="openOperatorNotation('{{$oDoc->operator_notation}}');"></div>
                                    @endif
                                    @if (!empty($oDoc->file_path))
                                        @if(strripos($oDoc->file_path, '.jpg'))
                                            <div class="icon_info_blue fancybox pointer" title="файлы" href="{{$oDoc->file_path}}"></div>
                                        @else
                                            <div class="icon_info_blue pointer" title="файлы">
                                                <a class="maximumLink" target='blank' href="{{$oDoc->file_path}}"></a>
                                            </div>
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    <a href="{{URL::route('agentClientCab', ['client_id' => $oDoc->client_id])}}">
                                        {{$oDoc->client_name}}
                                    </a>
                                </td>
                                <td>
                                    @if ($oDoc->status == InboxDocs::status_denied)
                                        <div style="color: #e31e25;">ДОКУМЕНТ ОТКЛОНЁН</div>
                                    @else
                                        {{$oDoc->name}}
                                    @endif
                                </td>
                                <td>{{$oDoc->sender_notation}}</td>
                                <td>{{$oDoc->created_at}}</td>
                                <td>{{$oDoc->date_operator_check}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div id="operatorNotationModal" class="modal_overlay" onclick="closeModal(this, event);">
            <div class="modal_window">
                <button class="m_close" onclick="closeModal(this);">&times;</button>
                <div class="modal_header border_biz">Примечание оператора</div>
                <div class="modal_body">
                    <div name="notation"></div>
                    <div class="modal_footer">
                        <button class="btn btn_middle biz" onclick="closeModal(this);">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

