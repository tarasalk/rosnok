@extends('cabinet.extends.main')
@section('head')
    <link rel="stylesheet" href="{{ URL::asset('thema/css/page/agent.css') }}">

    <link rel="stylesheet" href="{{ URL::asset('thema/css/jquery/jquery.time-to.css') }}">
    <script src="{{ URL::asset('thema/js/jquery/jquery.time-to.min.js') }}"></script>

    <script>
        $(function() {
            $('#newRequestsTable').dataTable({
                "order": [[ 0, "desc" ]]
            });
            $('#processedRequestsTable').dataTable({
                "order": [[ 0, "desc" ]]
            });

            $('.js-timer-cell').each(function() {
                timeleftseconds = $(this).data('timeleftseconds');

                if (timeleftseconds < 0)
                    $(this).text('Время вышло');
                else {
                    $(this).timeTo({
                        seconds: timeleftseconds,
                        fontSize: 14
                    }, function() {
                        // можно что-нибудь вывести
                    });
                }
            });
        });

        var request_id = null;

        function openProcessRequest(local_request_id) {
            request_id = local_request_id;
            $('#processRequestModal').show();
        }

        function confirmProcessRequest() {
            if (request_id == null)
                return;
            var client_type = $('input[name=client_type]:checked').val();
            var comment = $('textarea[name=comment]').val();

            $.ajax({
                url: "{{URL::route('agentConfirmProcessRequest')}}",
                type: "POST",
                data: {
                    request_id: request_id,
                    client_type: client_type,
                    comment: comment
                },
                success: function(r) {
                    if (r.status == 'ok') {
                        window.location.reload();
                    }
                }
            });
        }
    </script>
@endsection
@section('content_wrapper')
    <div class="title">
        Заявки с лендингов
    </div>
    <div class="content">
        <div>
            <table id="newRequestsTable" class="gray">
                <thead>
                <tr>
                    <th>id</th>
                    <th>Дата добавления</th>
                    <th>Имя заявителя</th>
                    <th>Телефон</th>
                    <th>Осталось времени</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($aNewRequests as $oRequest)
                    <tr>
                        <td class="text_center">{{$oRequest->id}}</td>
                        <td>{{date('d.m.Y H:i:s', strtotime($oRequest->created_at))}}</td>
                        <td>{{$oRequest->user_name}}</td>
                        <td>{{$oRequest->user_phone}}</td>
                        <td class="js-timer-cell" data-timeLeftSeconds="{{$oRequest->timeLeftSeconds}}"></td>
                        <td><button class="btn btn_middle money" onclick="openProcessRequest('{{$oRequest->id}}');">Обработать</button></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div style="margin-top: 60px;">
            <table id="processedRequestsTable" class="gray">
                <thead>
                <tr>
                    <th>id</th>
                    <th>Имя заявителя</th>
                    <th>Телефон</th>
                    <th>Время обращения</th>
                    <th>Время обработки</th>
                    <th>Результат обработки</th>
                    <th>Комментарий обработки</th>
                </tr>
                </thead>
                <tbody>
                @foreach($aProcessedRequests as $oRequest)
                    <tr>
                        <td>{{$oRequest->id}}</td>
                        <td>{{$oRequest->user_name}}</td>
                        <td>{{$oRequest->user_phone}}</td>
                        <td>{{date('d.m.Y H:i:s', strtotime($oRequest->created_at))}}</td>
                        <td>{{date('d.m.Y H:i:s', strtotime($oRequest->processed_date))}}</td>
                        <td>
                            @if ($oRequest->client_type == \App\Models\LandingRequest::CLIENT_TYPE_POTENTIAL)
                                Потенциальный клиент
                            @elseif ($oRequest->client_type == \App\Models\LandingRequest::CLIENT_TYPE_REAL)
                                Реальный клиент
                            @elseif ($oRequest->client_type == \App\Models\LandingRequest::CLIENT_TYPE_NOT_A_CLIENT)
                                Не клиент
                            @endif
                        </td>
                        <td>{{$oRequest->comment}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div id="processRequestModal" class="modal_overlay" onclick="closeModal(this, event);">
            <div class="modal_window">
                <button class="m_close" onclick="closeModal(this);">&times;</button>
                <div class="modal_header border_biz">Обработка заявки</div>
                <div class="modal_body">
                    <div class="row">Результат обработки:</div>
                    <div class="row">
                        <div><label><input type="radio" name="client_type" value="2" checked>Реальный клиент</label></div>
                        <div><label><input type="radio" name="client_type" value="1">Потенциальный клиент</label></div>
                        <div><label><input type="radio" name="client_type" value="-1">Не клиент</label></div>
                    </div>
                    <div class="row">Комментарий обработки</div>
                    <div class="row"><textarea name="comment" style="width: 100%;height: 100px;"></textarea></div>
                    <div class="modal_footer">
                        <button class="btn btn_middle money" onclick="confirmProcessRequest();">Завершить</button>
                        <button class="btn btn_middle biz" onclick="closeModal(this);">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

