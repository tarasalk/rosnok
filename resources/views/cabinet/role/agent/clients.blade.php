@extends('cabinet.extends.main')
@section('head')
    <link rel="stylesheet" href="{{ URL::asset('thema/css/page/agent.css') }}">
@endsection
@section('content_wrapper')
    <script>
        function redirectAddClient() {
            window.location.href = "{{URL::route('agentClientAdd')}}";
        }
    </script>
    <div class="title">
        Мои клиенты
    </div>
    <div class="content">
        <div class="row big"><button class="btn btn_middle money" onclick="redirectAddClient();">Добавить клиента</button></div>
        <div class="row menu_horizontal">
            <a href="{{URL::route('agentClients', ['category' => 'all'])}}">Все ({{$aClientsCategoriesCount->all}})</a>
            <a href="{{URL::route('agentClients', ['category' => 'without_delay'])}}">Без просрочки ({{$aClientsCategoriesCount->withoutDelay}})</a>
            <a href="{{URL::route('agentClients', ['category' => 'not_pay_rate'])}}">Неактивированные ({{$aClientsCategoriesCount->notPayRate}})</a>
            <a href="{{URL::route('agentClients', ['category' => 'soon_pay'])}}">Скоро платить ({{$aClientsCategoriesCount->soonPay}})</a>
            <a href="{{URL::route('agentClients', ['category' => 'delay_less_1_month'])}}">Просрочка < 1 мес. ({{$aClientsCategoriesCount->delayLess1Month}})</a>
            <a href="{{URL::route('agentClients', ['category' => 'delay_more_1_month'])}}">Просрочка > 1 мес. ({{$aClientsCategoriesCount->delayMore1Month}})</a>
        </div>
        <div class="row agent_client_list">
            @foreach($aClients as $oClient)
                <div class="client_block @if ($oClient->pay_day_diff < 0) red @elseif ($oClient->pay_day_diff < 7) yellow @endif">
                    <div class="avatar"><img src="{{$oClient->base->avatar}}"></div>
                    <div class="info">
                        <div>
                            <div>
                                <a href="{{URL::route('agentClientCab', ['user_id' => $oClient->client_id])}}">
                                    {{$oClient->base->last_name}} {{$oClient->base->first_name}}
                                </a>
                            </div>
                            @if(isset($oClient->phone))
                                <div>Тел.: {{$oClient->phone->country}}{{$oClient->phone->phone}}</div>
                            @endif
                            <div>ID: {{$oClient->client_id}}</div>
                        </div>
                        <div>
                            <div>Тариф: {{$oClient->rate_pay}} руб.</div>
                            <div>В работе: {{$oClient->in_work_month}} мес. {{$oClient->in_work_day}} дн.</div>
                            <div>Кооп. участок: {{$oClient->office->office_name or '-'}}</div>
                        </div>
                        <div class="TODO">
                            Следующий по очереди взнос: 2-й До окончания оплаченного месяца: 6 дн
                        </div>
                    </div>
                    <div class="buy_btn">
                        <div>
                            @if($oClient->pay_day_diff > 0)
                                Осталось: {{$oClient->pay_day_diff}} дн.
                            @else
                                Просрочка: {{abs($oClient->pay_day_diff)}} дн.
                            @endif
                        </div>
                        <button class="btn btn_middle money TODO">Оплатить</button>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

