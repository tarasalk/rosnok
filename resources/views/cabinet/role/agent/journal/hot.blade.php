@extends('cabinet.role.agent.journal.main')

@section('journal_content')
    <div class="hotContainer">
        <table class="gray">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Имя</th>
                    <th>Телефон</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($aHotClients as $oHotClient)
                    {{--*/ $oClient = $aClientsPeople->{$oHotClient->client_id} /*--}}
                    <tr>
                        <td>{{$oHotClient->client_id}}</td>
                        <td>{{$oClient->base->last_name}} {{$oClient->base->first_name}}</td>
                        <td>
                            @if (isset($oClient->phone->phone))
                                {{$oClient->phone->country}}{{$oClient->phone->phone}}
                            @else
                                нет
                            @endif
                        </td>
                        <td>
                            <button class="btn btn_middle biz TODO">Перейти к оплате</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection