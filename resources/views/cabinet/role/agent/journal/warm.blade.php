@extends('cabinet.role.agent.journal.main')

@section('journal_content')
    <div class="warmContainer">
        <table class="gray">
            <thead>
            <tr>
                <th>№</th>
                <th>Имя</th>
                <th>Телефон</th>
                <th>Примечание</th>
                <th>Добавлен</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($aJournalClients as $oJournalClient)
                <tr>
                    <td>{{$oJournalClient->id}}</td>
                    <td>{{$oJournalClient->name}}</td>
                    <td>{{$oJournalClient->phone}}</td>
                    <td>{{$oJournalClient->notation}}</td>
                    <td>{{$oJournalClient->created_at}}</td>
                    <td onclick="showEditClient(
                            '{{$oJournalClient->id}}',
                            '{{$oJournalClient->name}}',
                            '{{$oJournalClient->phone}}',
                            '{{$oJournalClient->notation}}',
                            '{{$oJournalClient->status}}')"
                    >
                        <span class="icon_pencil" style="cursor:pointer;"></span>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection