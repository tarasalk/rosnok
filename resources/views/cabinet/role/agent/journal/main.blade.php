{{--*/ use \App\Models\JournalClient /*--}}

@extends('cabinet.extends.main')
@section('head')
    <link rel="stylesheet" href="{{ URL::asset('thema/css/page/agent.css') }}">
    <script>
        $(function() {
            $("#ac_phone").mask('+7 (999) 999-99-99');
            $("#ec_phone").mask('+7 (999) 999-99-99');
        });

        function showAddClient() {
            $('#add_client_modal').show();
        }

        function addClient() {
            var name = $('#ac_name').val();
            var phone = $('#ac_phone').val().replace(/\D/g, '');
            var notation = $('#ac_notation').val();
            var status = '{{JournalClient::status_cold}}';

            $.ajax({
                url: "{{URL::route('agentJournalAddClient')}}",
                type: "POST",
                data: {
                    name: name,
                    phone: phone,
                    notation: notation,
                    status: status
                },
                success: function(r) {
                    if (r.status == 'ok') {
                        toastr.info('Успешно');
                        setTimeout(location.reload(), 2000);
                    }
                    else
                        console.log(r.error);
                }
            });
        }

        function showEditClient(id, name, phone, notation, status) {
            $('#ec_id').val(id);
            $('#ec_name').val(name);
            $('#ec_phone').val(phone);
            $('#ec_notation').val(notation);

            $("input[value='"+status+"']").prop('checked', true);

            $('#edit_client_modal').show();
        }

        function editClient() {
            var id = $('#ec_id').val();
            var name = $('#ec_name').val();
            var phone = $('#ec_phone').val().replace(/\D/g, '');
            var notation = $('#ec_notation').val();
            var status = $('input[name=ec_status]:checked').val();

            $.ajax({
                url: "{{URL::route('agentJournalEditClient')}}",
                type: "POST",
                data: {
                    id: id,
                    name: name,
                    phone: phone,
                    notation: notation,
                    status: status
                },
                success: function(r) {
                    if (r.status == 'ok') {
                        toastr.info('Успешно');
                        setTimeout(location.reload(), 2000);
                    }
                    else
                        console.log(r.error);
                }
            });
        }
    </script>
@endsection
@section('content_wrapper')
    <div class="title">
        Журнал клиентов
    </div>
    <div class="content">
        <div class="row big menu_horizontal">
            <a href="{{URL::route('agentJournalCold')}}">Холодные</a>
            <a href="{{URL::route('agentJournalWarm')}}">Теплые</a>
            <a href="{{URL::route('agentJournalHot')}}">Горячие</a>
            <a href="{{URL::route('agentJournalReal')}}">Реальные</a>
        </div>
        @yield('journal_content')
    </div>

    <div id="add_client_modal" class="modal_overlay" onclick="closeModal(this, event);">
        <div class="modal_window">
            <button class="m_close" onclick="closeModal(this);">&times;</button>
            <div class="modal_header border_biz">Добавление клиента</div>
            <div class="modal_body">
                <div>
                    <div class="row big">
                        <h4>Имя</h4>
                        <div><input type="text" id="ac_name" style="width: 100%;"></div>
                    </div>
                    <div class="row big">
                        <h4>Телефон</h4>
                        <div><input type="text" id="ac_phone" class="fixInputMask" style="width: 100%;"></div>
                    </div>
                    <div class="row big">
                        <h4>Примечание</h4>
                        <div><textarea id="ac_notation" style="width: 100%;height: 100px;"></textarea></div>
                    </div>
                </div>
                <div class="modal_footer">
                    <button class="btn btn_middle biz" onclick="addClient();">Добавить</button>
                    <button class="btn btn_middle money" onclick="closeModal(this);">Отмена</button>
                </div>
            </div>
        </div>
    </div>

    <div id="edit_client_modal" class="modal_overlay" onclick="closeModal(this, event);">
        <div class="modal_window">
            <button class="m_close" onclick="closeModal(this);">&times;</button>
            <div class="modal_header border_biz">Редактирование клиента</div>
            <div class="modal_body">
                <div>
                    <input type="hidden" id="ec_id">
                    <div class="row big">
                        <h4>Имя</h4>
                        <div><input type="text" id="ec_name" style="width: 100%;"></div>
                    </div>
                    <div class="row big">
                        <h4>Телефон</h4>
                        <div><input type="text" id="ec_phone" class="fixInputMask" style="width: 100%;"></div>
                    </div>
                    <div class="row big">
                        <h4>Примечание</h4>
                        <div><textarea id="ec_notation" style="width: 100%;height: 100px;"></textarea></div>
                    </div>
                    <div class="row big">
                        <h4>Статус</h4>
                        <div>
                            <label><input type="radio" name="ec_status" value="{{JournalClient::status_cold}}">Холодный</label>
                            <label><input type="radio" name="ec_status" value="{{JournalClient::status_warm}}">Теплый</label>
                        </div>
                    </div>
                </div>
                <div class="modal_footer">
                    <button class="btn btn_middle biz" onclick="editClient();">Сохранить</button>
                    <button class="btn btn_middle money" onclick="closeModal(this);">Отмена</button>
                </div>
            </div>
        </div>
    </div>
@endsection

