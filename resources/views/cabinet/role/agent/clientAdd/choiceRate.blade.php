<script>
    $(function() {
        $('#choiceFillCreditModal').show();

        $('select[name=rate_pay]').change(function() {
            aClient.rate = this.value;
        });

        $('#choiceRateModal').find('.rates > label').click(function() {
            $('#choiceRateModal').find('.rates > label').removeClass('active');
            $(this).addClass('active');
        });
    });

    function openFillCredits() {
        $.ajax({
            url: "{{URL::route('agentClientAddGetCreditsView')}}",
            type: "POST",
            data: {client_id: aClient.id},
            success: function(r) {
                $('.stepChoiceRate .credits').html(r);
                $('#choiceFillCreditModal').hide();

                $('.credits').removeClass('hide');
                $('.choiceServiceLive').removeClass('hide');
                $('.choiceSocialStatus').removeClass('hide');
                $('#calculationRateBlock').removeClass('hide');
            }
        });
    }

    function calculationRate() {
        $.ajax({
            url: "{{URL::route('agentClientAddCalculateRate')}}",
            type: "POST",
            data: {
                client_id: aClient.id
            },
            success: function(r) {
                aClient.rate = r.rate;
                $('#autoRateValue').text(aClient.rate);
                $('#confirmAutoRateModal').show();
            }
        });
    }

    function openChoiceRate() {
        $('#choiceFillCreditModal').hide();
        $('#choiceRateModal').show();
    }
    
    function confirmRate() {
        aClient.rate = $('#choiceRateModal').find('.rates > label.active').data('rate');

        $.ajax({
            url: "{{URL::route('agentClientAddConfirmRate')}}",
            type: "POST",
            data: {
                client_id: aClient.id,
                rate: aClient.rate
            },
            success: function(r) {
                if (r.status == 'ok') {
                    goStepChoiceOffice();
                }
                else toastr.error('Ошибка');
            }
        });
    }

    function confirmAutoRate() {
        $('#confirmAutoRateModal').hide();

        service_live = $('input[name=service_live]:checked').val();
        social_status = $('input[name=social_status]:checked').val();

        $.ajax({
            url: "{{URL::route('agentClientAddConfirmRate')}}",
            type: "POST",
            data: {
                client_id: aClient.id,
                service_live: service_live,
                social_status: social_status,
                rate: aClient.rate
            },
            success: function(r) {
                if (r.status == 'ok') {
                    goStepChoiceOffice();
                }
                else toastr.error('Ошибка');
            }
        });
    }

    function goStepChoiceOffice() {
        changeCurrentStep('stepChoiceOffice');
    }
</script>
<div class="stepContainer stepChoiceRate">
    <div class="row big credits hide"></div>
    <div class="row big choiceServiceLive hide">
        <div>Срок обслуживания</div>
        <div class="choiceBlock">
            <label><input type="radio" name="service_live" value="7">7</label>
            <label><input type="radio" name="service_live" value="12" checked>12</label>
        </div>
    </div>
    <div class="row big choiceSocialStatus hide">
        <div>Выберите социальный статус</div>
        <div class="choiceBlock">
            <label><input type="radio" name="social_status" value="Пенсионер">Пенсионер</label>
            <label><input type="radio" name="social_status" value="Официально трудоустроен" checked>Официально трудоустроен</label>
            <label><input type="radio" name="social_status" value="Нет официального дохода">Нет официального дохода</label>
        </div>
    </div>
    <div class="row hide" id="calculationRateBlock">
        <button class="btn btn_middle biz" onclick="calculationRate();">Рассчитать тариф</button>
    </div>

    <div id="confirmAutoRateModal" class="modal_overlay" onclick="closeModal(this, event);">
        <div class="modal_window">
            <button class="m_close" onclick="closeModal(this);">&times;</button>
            <div class="modal_header border_biz">Подсчет тарифа</div>
            <div class="modal_body">
                <div class="rateBlock">
                    <div>
                        Тариф:&nbsp;<span id="autoRateValue"></span>&nbsp;рублей
                    </div>
                </div>
                <div class="modal_footer">
                    <button class="btn btn_middle biz" onclick="confirmAutoRate()">Далее</button>
                    <button class="btn btn_middle" onclick="openChoiceRate()">Изменить тариф</button>
                </div>
            </div>
        </div>
    </div>

    <div id="choiceFillCreditModal" class="modal_overlay" onclick="closeModal(this, event);">
        <div class="modal_window">
            <button class="m_close" onclick="closeModal(this);">&times;</button>
            <div class="modal_header border_biz">Предупреждение системы</div>
            <div class="modal_body">
                <div class="row">
                    {{Auth::user()->getFirstName()}}, будьте внимательны! Вы можете произвести автоматический подбор тарифа
                    обслуживания либо установить тариф вручную.
                </div>
                <div class="row">
                    Для автоамтического подбора убедитесь, что все сведения по договорам займа правильно введены в систему.</div>
                <div class="row">
                    Внимание: если Вы устанавливаете тариф вручную, Вы принимаете на себя
                    полную ответственность за правильность указания тарифа. В случае обнаружения расхождений все действия по урегулированию
                    будут возложены на Вас.
                </div>
                <div class="modal_footer">
                    <button class="btn btn_middle biz" onclick="openFillCredits()">Заполнить данные по кредитам</button>
                    <button class="btn btn_middle biz" onclick="openChoiceRate()">Установить тариф без заполнения</button>
                </div>
            </div>
        </div>
    </div>

    <div id="choiceRateModal" class="modal_overlay" onclick="closeModal(this, event);">
        <div class="modal_window">
            <button class="m_close" onclick="closeModal(this)">&times;</button>
            <div class="modal_header border_biz">Выбор тарифа</div>
            <div class="modal_body">
                <div>
                    <div class="row rates">
                        <label data-rate="5700"><input name="rate_pay" type="radio"><span>Тариф: 5700 рублей</span></label>
                        <label data-rate="9700"><input name="rate_pay" type="radio"><span>Тариф: 9700 рублей</span></label>
                    </div>
                    <div class="row rates">
                        <label data-rate="10700" class="active"><input name="rate_pay" type="radio" checked><span>Тариф: 10700 рублей</span></label>
                        <label data-rate="12000"><input name="rate_pay" type="radio"><span>Тариф: 12000 рублей</span></label>
                    </div>
                </div>
                <div class="modal_footer">
                    <button class="btn btn_middle biz" onclick="confirmRate()">Далее</button>
                    <button class="btn btn_middle" onclick="closeModal(this)">Отменить</button>
                </div>
            </div>
        </div>
    </div>
</div>