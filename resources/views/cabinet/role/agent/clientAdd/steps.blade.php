<script>
    function changeCurrentStep(dataStep) {
        $('.stepContainer').hide();
        $('.' + dataStep).show();

        var steps = $('#steps');
        //steps.find('.step').removeClass('active');
        steps.find(".step[data-step='" + dataStep + "']").addClass('active');
    }
</script>
<div class="row big steps" id="steps">
    <div class="step" data-step='stepClientAdd'>Добавить клиента</div>
    <div class="step" data-step='stepChoiceRate'>Выбрать тариф</div>
    <div class="step" data-step='stepChoiceOffice'>Выбрать офис</div>
    <div class="step" data-step='stepFillPassport'>Заполнить паспорт</div>
    <div class="step" data-step='stepPayment'>Оплатить взносы</div>
</div>