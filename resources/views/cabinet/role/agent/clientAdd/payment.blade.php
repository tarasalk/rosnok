<script>
    $(function() {
        $('#confirmClientModal').show();
    });
    function payRate() {
        $.ajax({
            url: "{{URL::route('agentClientAddPayRate')}}",
            type: "POST",
            data: {
                client_id: client_id,
                rate: rate
            },
            success: function(r) {
                if (r.status == 'ok') {
                    toastr.info('Клиент активирован');
                    window.location.href = "{{URL::route('agentClientCab', ['user_id' => ''])}}" + '/' + client_id;
                }
            }
        });
    }
</script>
<div class="stepContainer stepPayment">
    <div id="confirmClientModal" class="modal_overlay" onclick="closeModal(this, event);">
        <div class="modal_window">
            <button class="m_close" onclick="closeModal(this)">&times;</button>
            <div class="modal_header border_biz">Электронное поручение на взнос</div>
            <div class="modal_body">
                <div class="row">
                    {{Auth::user()->getFirstName()}}, проверьте указанные данные перед внесением взноса:
                </div>
                <div class="row">
                    <div>Пайщик: <span>Константин Константинов</span></div>
                    <div>Взнос за период: с <span>1</span> по <span>30</span><span></span></div>
                    <div>Кооперативный участок: Ижевск-1</div>
                </div>
                <div class="row">
                    <div>Взнос в паевы фонд: <span></span> руб.</div>
                    <div>Вступительный взнос: <span></span> руб.</div>
                    <div>Членский взнос: <span></span> руб.</div>
                    <div>Всего взносов на сумму: <span></span> руб.</div>
                </div>
                <div class="modal_footer">
                    <button class="btn btn_middle biz TODO" onclick="toastr.info('todo');">Подтверждаю данные</button>
                    <button class="btn btn_middle TODO" onclick="">Нет, изменить данные</button>
                </div>
            </div>
        </div>
    </div>
</div>