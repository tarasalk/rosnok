<style>
    .ui-autocomplete {
        min-width: 450px !important;
    }
</style>
<script>
    $(function() {
        $('input[name="office_name"]').each(function () {
            $(this).autocomplete({
                minLength: 1,
                source: function (req, response) {
                    $.ajax({
                        url: "{{URL::route('agentClientAddSearchOffices')}}",
                        type: "POST",
                        data: {
                            office_name: req.term
                        },
                        success: function (r) {
                            response($.map(r.aOffices, function (item) {
                                return {
                                    id: item.id,
                                    value: item.office_name
                                };
                            }));
                        }
                    });
                },
                select: function (ev, ui) {
                    $(this).val(ui.item.value);
                    aClient.office_id =  ui.item.id;

                    // отменить очистку поля поиска
                    ev.preventDefault();
                }
            });
        });
    });

    function confirmChoiceOffice() {
        if (aClient.office_id == undefined) {
            toastr.info("Сначала необходимо выбрать офис");
            return;
        }

        $.ajax({
            url: "{{URL::route('agentClientAddConfirmChoiceOffice')}}",
            type: "POST",
            data: {
                client_id: aClient.id,
                office_id: aClient.office_id
            },
            success: function(r) {
                if (r.status == 'ok') {
                    toastr.info('Успешно');
                    goStepFillPassport();
                }
            }
        });
    }

    function goStepFillPassport() {
        $.ajax({
            url: "{{URL::route('agentClientAddGetPassportView')}}",
            type: "POST",
            data: { client_id: aClient.id },
            success: function(r) {
                $('.stepFillPassport .passport').html(r);
                changeCurrentStep('stepFillPassport');
            }
        });
    }
</script>
<div class="stepContainer stepChoiceOffice">
    <div class="row">
        <h4>Введите название офиса</h4>
        <div class="row horizontal"><input name="office_name" type="text"><button class="btn btn_middle biz" onclick="confirmChoiceOffice()">Далее</button></div>
    </div>
</div>