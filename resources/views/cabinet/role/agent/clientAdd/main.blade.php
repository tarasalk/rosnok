@extends('cabinet.extends.main')
@section('head')
    <link rel="stylesheet" href="{{ URL::asset('thema/css/page/agent.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('thema/css/page/client.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('thema/css/page/passportEdit.css') }}">
@endsection
@section('content_wrapper')
    <script>
        var aClient = [];

        $(function() {
            $("input[name='clientPhone']").mask('(999) 999-99-99');

            $.protip({
                defaults: {
                    position: 'top',
                    trigger: 'sticky',
                    autoHide: 3000,
                    size: 'small'
                }
            });

            changeCurrentStep('stepClientAdd');
        });
    </script>
    <div class="title">
        Добавление клиента
    </div>
    <div class="content">
        <div class="agentClientAdd">
            @include('cabinet.role.agent.clientAdd.steps')
            @include('cabinet.role.agent.clientAdd.clientAdd')
            @include('cabinet.role.agent.clientAdd.choiceRate')
            @include('cabinet.role.agent.clientAdd.choiceOffice')
            @include('cabinet.role.agent.clientAdd.fillPassport')
            @include('cabinet.role.agent.clientAdd.payment')
        </div>
    </div>
@endsection

