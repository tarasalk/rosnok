<script>
    $(function() {
        $('#enterPhoneMessageModal').show();
    });

    function findUser() {
        var $phone = $('input[name=clientPhone]');
        var phone = $phone.cleanVal();

        if (phone == '') {
            $phone.protipShow({title: 'обязательное поле'});
            $phone.mask('+7 (999) 999-99-99');

            return;
        }

        aClient.phone = phone;

        $.ajax({
            url: "{{URL::route('agentClientAddFind')}}",
            type: "POST",
            data: {phone: aClient.phone},
            success: function(r) {
                if (r.status == 'ok') {
                    if (r.message == 'user_not_found') {
                        $('#clientNotFoundModal').show();
                        $('#cnfmPhone').text(phoneFormat(aClient.phone));
                    }
                    else {
                        aClient.id          = r.user.user_id;
                        aClient.first_name  = r.user.base.first_name;
                        aClient.last_name   = r.user.base.last_name;
                        aClient.avatar      = r.user.base.avatar;

                        $('.clientSearchBlock').addClass('hide');

                        $('#foundUser').removeClass('hide');
                        $('#fu_fio').text(aClient.last_name + ' ' + aClient.first_name);
                        $('#fu_id').text(aClient.id);
                        $('#fu_phone').text(phoneFormat(r.user.phone.phone));
                        $('#fu_avatar').attr('src', aClient.avatar);

                        if (r.message == 'user_already_client') {
                            toastr.info('Уже клиент');

                            if (r.user.phone.activate_status == 1) {
                                toastr.info('Телефон уже привязан');
                                goStepChoiceRate();
                            }
                            else {
                                openBindPhoneUserModal();
                            }
                        }
                        else if (r.message == 'user_found') {
                            $('button.madeUserClient').removeClass('hide');
                        }
                        else { console.log('undefined_message'); }
                    }
                }
                else { console.log('status_error'); }
            }
        });
    }

    function openBindPhoneUserModal() {
        $('#bpum_phone').text(phoneFormat(aClient.phone));
        $('#bpum_fio').text(aClient.first_name + ' ' + aClient.last_name);
        $('#bindPhoneUserModal').show();
    }

    function goStepChoiceRate() {
        changeCurrentStep('stepChoiceRate');
    }

    function madeUserClient() {
        if (aClient.id == null) return;

        $.ajax({
            url: "{{URL::route('agentClientAddMade')}}",
            type: "POST",
            data: {
                user_id: aClient.id
            },
            success: function(r) {
                if (r.status == 'ok') {
                    toastr.info('Пользователь успешно стал клиентом');

                    if (r.user.phone.activate_status == 1) {
                        toastr.info('Телефон уже активирован');
                        goStepChoiceRate();
                    }
                    else {
                        openBindPhoneUserModal();
                    }
                }
            }
        });
    }

    function sendCodeConfirmClientPhone() {
        $.ajax({
            url: "{{URL::route('agentClientAddSendCode')}}",
            type: "POST",
            data: {phone: aClient.phone},
            success: function(r) {
                if (r.status == 'ok') {
                    $('#confirmClientPhoneModal').show();
                    $('#bindPhoneUserModal').hide();
                }
            }
        });
    }

    function sendSmsCodeRepeat() {
        sendCodeConfirmClientPhone();
    }

    function confirmPhoneClient() {
        var code = $('#confirmClientPhoneModal').find('input[name=code]').val();

        $.ajax({
            url: "{{URL::route('agentClientAddConfirmPhone')}}",
            type: "POST",
            data: {
                phone: aClient.phone,
                code: code
            },
            success: function(r) {
                console.log(r);
                if (r.status == 'ok') {
                    toastr.info('Пользователь успешно активирован');
                    goStepChoiceRate();
                }
                else if (r.errors == 'incorrect_code') {
                    toastr.warning('неверный код');
                }
            }
        });
    }

    function openCreateClient() {
        $('#clientNotFoundModal').addClass('hide');
        $('.clientSearchBlock').addClass('hide');
        $('#cncb_phone').text(phoneFormat(aClient.phone));
        $('.createNewClientBlock').removeClass('hide');
    }

    function createClient() {
        var $createNewClientBlock = $('.createNewClientBlock');

        var first_name  = $createNewClientBlock.find('input[name=first_name]').val();
        var last_name   = $createNewClientBlock.find('input[name=last_name]').val();

        if (first_name == '' || last_name == '')
            return;

        $.ajax({
            url: "{{URL::route('agentClientAddCreate')}}",
            type: "POST",
            data: {
                first_name: first_name,
                last_name: last_name,
                phone: aClient.phone
            },
            success: function(r) {
                if (r.status == 'ok') {
                    toastr.info('Пользователь успешно сохранен');
                    $createNewClientBlock.addClass('hide');

                    aClient.id = r.client_id;
                    aClient.first_name = first_name;
                    aClient.last_name = last_name;
                    openBindPhoneUserModal();
                }
            }
        });
    }
</script>
<div class="stepContainer stepClientAdd">
    <div class="row clientInfoBlock hide" id="foundUser">
        <div class="avatarBlock"><img id="fu_avatar" src=""></div>
        <div class="infoBlock">
            <div><span id="fu_fio"></span> ID: <span id="fu_id"></span></div>
            <div>+7 <span id="fu_phone"></span></div>
            <button class="btn btn_middle money madeUserClient hide" onclick="madeUserClient()">Добавить</button>
        </div>
    </div>
    <div class="row clientSearchBlock">
        <h4>Укажите номер телефона клиента</h4>
        <div class="row horizontal">
            <div>+7</div>
            <input name="clientPhone" type="text">
            <button class="btn btn_middle biz" onclick="findUser()">Далее</button>
        </div>
        <div class="row phoneRestrictionsMessage">
            Внимание! Принимаются только Российские номера.
        </div>
    </div>
    <div class="row createNewClientBlock hide">
        <div class="row horizontal">
            <div class="column">
                <h4>Фамилия</h4>
                <div><input type="text" name="last_name"></div>
            </div>
            <div class="column">
                <h4>Имя</h4>
                <div><input type="text" name="first_name"></div>
            </div>
        </div>
        <div class="row">+7 <span id="cncb_phone"></span></div>
        <div class="row">
            <button class="btn btn_middle biz" onclick="createClient()">Сохранить в базе</button>
        </div>
    </div>

    <div id="enterPhoneMessageModal" class="modal_overlay">
        <div class="modal_window">
            <button class="m_close" onclick="closeModal(this);">&times;</button>
            <div class="modal_header border_biz">Добавление клиента</div>
            <div class="modal_body">
                <div>
                    {{Auth::user()->getFirstName()}}, введите номер мобильного телефона
                </div>
                <div class="modal_footer">
                    <button class="btn btn_middle biz" onclick="closeModal(this)">Мне понятно</button>
                </div>
            </div>
        </div>
    </div>

    <div id="clientNotFoundModal" class="modal_overlay">
        <div class="modal_window">
            <button class="m_close" onclick="closeModal(this);">&times;</button>
            <div class="modal_header border_biz">Внимание!</div>
            <div class="modal_body">
                <div class="row">
                    Данный номер +7 <span id="cnfmPhone"></span> не принадлежит ни одному человеку из базы данных.
                </div>
                <div class="row">
                    Хотите добавить нового человека?
                </div>
                <div class="modal_footer">
                    <button class="btn btn_middle biz" onclick="openCreateClient()">Добавить</button>
                    <button class="btn btn_middle" onclick="closeModal(this)">Отмена</button>
                </div>
            </div>
        </div>
    </div>

    <div id="bindPhoneUserModal" class="modal_overlay">
        <div class="modal_window">
            <button class="m_close" onclick="closeModal(this);">&times;</button>
            <div class="modal_header border_biz">Внимание!</div>
            <div class="modal_body">
                <div>
                    Хотите привязать номер +7 <span id="bpum_phone"></span> к пользователю <span id="bpum_fio"></span>
                </div>
                <div class="modal_footer">
                    <button class="btn btn_middle biz" onclick="sendCodeConfirmClientPhone()">Отправить СМС</button>
                    <button class="btn btn_middle" onclick="goStepChoiceRate()">Нет, закрыть</button>
                </div>
            </div>
        </div>
    </div>

    <div id="confirmClientPhoneModal" class="modal_overlay">
        <div class="modal_window">
            <button class="m_close" onclick="closeModal(this);">&times;</button>
            <div class="modal_header border_biz">Подтверждение кода активации</div>
            <div class="modal_body">
                <div class="row big">{{Auth::user()->getFirstName()}}, введите полученный СМС-код</div>
                <div class="row big horizontal"><input type="text" name="code">Срок действия СМС-кода 5 минут!</div>
                <div class="row big">Вы не получили СМС-код? <a onclick="sendSmsCodeRepeat()">Отправить код повторно</a></div>
                <div class="modal_footer">
                    <button class="btn btn_middle biz" onclick="confirmPhoneClient()">Подтвердить</button>
                    <button class="btn btn_middle" onclick="goStepChoiceRate()">Отмена</button>
                </div>
            </div>
        </div>
    </div>
</div>