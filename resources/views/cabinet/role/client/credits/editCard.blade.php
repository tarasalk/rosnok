<script>
    $(function() {
        $('#cardEditForm').submit(function(event) {
            event.preventDefault();
            $.ajax({
                url: "{{ URL::route('client.credit.edit.card') }}",
                type: "POST",
                data: $(this).serialize(),
                success: function(r) {
                    alert('Отредактировано');
                    goBack();
                }
            });
        });
    });
</script>
<form id="cardEditForm" class="credit-form">
    <h3>Кредитная карта</h3>
    <div class="row">
        <label>
            <input type="checkbox" name="credit_task[]" id="credit-task-2" value="{{$aTasks[1]->id}}">{{$aTasks[1]->task}}
        </label>
    </div>
    <div class="row">
        <div>Банк</div>
        <div><input type="text" name="bank_name" class="full_width" value="{{$aCredit->bank_name}}"></div>
    </div>
    <div class="row flex">
        <div>
            <div>Номер карты</div>
            <div><input type="text" name="card_number" value="{{$aCredit->card_number}}"></div>
        </div>
        <div>
            <div>Дата выдачи карты</div>
            <div><input type="text" name="date_contract_create" class="datepicker-here" value="{{$aCredit->date_contract_create}}"></div>
        </div>
        <div>
            <div>Сумма кредита</div>
            <div><input type="text" name="credit_sum" value="{{$aCredit->credit_sum}}"> руб.</div>
        </div>
        <div>
            <div>Номер договора</div>
            <div><input type="text" name="contract_number" value="{{$aCredit->contract_number}}"></div>
        </div>
    </div>
    <div class="row flex">
        <div>
            <div>Номер счета</div>
            <div><input type="text" name="account_number" value="{{$aCredit->account_number}}"></div>
        </div>
        <div>
            <div>Наименование кредитного обязательства</div>
            <div>
                <select name="credit_commitments_name" class="credit_commitments_name full_width" id="credit-select">
                    <option value="">Отсутствует</option>
                    <option value="loan">Договор займа</option>
                    <option value="contract">Кредитный договор</option>
                    <option value="credit">Заявление о выдаче кредита</option>
                    <option value="accept">Соглашение на предоставление кредита</option>
                    <option value="statement">Заявление клиента о заключении договора кредитования</option>
                    <option value="agreement">Заявление на заключение Соглашения о кредитовании</option>
                    <option value="card">Заявление для оформления банковской карты</option>
                    <option value="notarget">Индивидуальные условия нецелевого кредита</option>
                    <option value="individual">Индивидуальные условия предоставления нецелевого кредита</option>
                    <option value="credit_card">Договор о выпуске и использовании кредитной карты</option>
                    <option value="form">Анкета заявление</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row flex">
        <div>
            <div>Сумма последнего платежа</div>
            <div><input type="text" name="last_pay_sum" value="{{$aCredit->last_pay_sum}}"></div>
        </div>
        <div>
            <div>Дата последнего платежа</div>
            <div><input type="text" name="date_last_pay" class="datepicker-here" value="{{$aCredit->date_last_pay}}"></div>
        </div>
        <div>
            <div>Срок кредита</div>
            <div><input type="text" name="date_credit_end" class="datepicker-here" value="{{$aCredit->date_credit_end}}"></div>
        </div>
        <div>
            <div>Средний ежемесячный платеж</div>
            <div><input type="text" name="average_monthly_pay" value="{{$aCredit->average_monthly_pay}}"></div>
        </div>
    </div>
    <h3>Задачи</h3>
    <div class="row credit-task">
        <label>
            <input type="checkbox" name="credit_task[]" id="credit-task-1" value="{{$aTasks[0]->id}}">{{$aTasks[0]->task}}
        </label>
        <label>
            <input type="checkbox" name="credit_task[]" id="credit-task-3" value="{{$aTasks[2]->id}}">{{$aTasks[2]->task}}
        </label>
        <label>
            <input type="checkbox" name="credit_task[]" id="credit-task-4" value="{{$aTasks[3]->id}}">{{$aTasks[3]->task}}
        </label>
        <label>
            <input type="checkbox" name="credit_task[]" id="credit-task-5" value="{{$aTasks[4]->id}}">{{$aTasks[4]->task}}
        </label>
        <label>
            <input type="checkbox" name="credit_task[]" id="credit-task-6" value="{{$aTasks[5]->id}}">{{$aTasks[5]->task}}
        </label>
        <label>
            <input type="checkbox" name="credit_task[]" id="credit-task-7" value="{{$aTasks[6]->id}}">{{$aTasks[6]->task}}
        </label>
        <label>
            <input type="checkbox" name="credit_task[]" id="credit-task-8" value="{{$aTasks[7]->id}}">{{$aTasks[7]->task}}
        </label>
        <label>
            <input type="checkbox" name="credit_task[]" id="credit-task-9" value="{{$aTasks[8]->id}}">{{$aTasks[8]->task}}
        </label>
    </div>
    <div class="row">
        <div>Примечание</div>
        <textarea name="notation" class="notation">{{$aCredit->notation}}</textarea>
    </div>
    <div class="btn-block">
        <input type="hidden" name="credit_id" value="{{$aCredit->id}}">
        <input type="submit" class="btn btn_middle money" value="Сохранить">
        <button type="button" class="btn btn_middle" onclick="goBack()">Отмена</button>
    </div>
</form>