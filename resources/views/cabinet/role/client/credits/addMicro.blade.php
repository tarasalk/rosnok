<script>
    $(function() {
        $('#microAddForm').submit(function(event) {
            event.preventDefault();
            $.ajax({
                url: "{{ URL::route('client.credit.add.micro') }}",
                type: "POST",
                data: $(this).serialize(),
                success: function(r) {
                    if (r.status == 'ok') {
                        console.log('success');
                    }
                    else console.log('error');
                }
            });
        });
    });
</script>
<form id="microAddForm" class="consumerAddForm">
    <div class="row title">Добавление микрозайма</div>
    <div class="row">
        <label>
            <input type="checkbox" name="credit_task[]" value="{{$aTasks[1]->id}}">{{$aTasks[1]->task}}
        </label>
    </div>
    <div class="row">
        <div>Микрофинансовая организация</div>
        <div><input type="text" class="mfo_id" name="mfo_id"></div>
    </div>
    <div class="row block1">
        <div>
            <div>Номер договора</div>
            <div><input type="text" class="contract_number" name="contract_number"></div>
        </div>
        <div>
            <div>Дата заключения договора</div>
            <div><input type='text' name="date_contract_create" class="datepicker-here date_contract"></div>
        </div>
        <div>
            <div>Сумма займа</div>
            <div><input type="text" name="credit_sum" class="credit_sum"> руб.</div>
        </div>
        <div>
            <div>Срок займа</div>
            <div><input type="text" name="date_credit_end" class="datepicker-here date_credit_end"></div>
        </div>
    </div>
    <div class="title">Задачи</div>
    <div class="row tasksContainer">
        <label>
            <input type="checkbox" name="credit_task[]" value="{{$aTasks[0]->id}}">{{$aTasks[0]->task}}
        </label>
        <label>
            <input type="checkbox" name="credit_task[]" value="{{$aTasks[2]->id}}">{{$aTasks[2]->task}}
        </label>
        <label>
            <input type="checkbox" name="credit_task[]" value="{{$aTasks[3]->id}}">{{$aTasks[3]->task}}
        </label>
        <label>
            <input type="checkbox" name="credit_task[]" value="{{$aTasks[4]->id}}">{{$aTasks[4]->task}}
        </label>
        <label>
            <input type="checkbox" name="credit_task[]" value="{{$aTasks[5]->id}}">{{$aTasks[5]->task}}
        </label>
        <label>
            <input type="checkbox" name="credit_task[]" value="{{$aTasks[6]->id}}">{{$aTasks[6]->task}}
        </label>
        <label>
            <input type="checkbox" name="credit_task[]" value="{{$aTasks[7]->id}}">{{$aTasks[7]->task}}
        </label>
        <label>
            <input type="checkbox" name="credit_task[]" value="{{$aTasks[8]->id}}">{{$aTasks[8]->task}}
        </label>
    </div>
    <div class="row">
        <div>Примечание</div>
        <textarea class="notation" name="notation"></textarea>
    </div>
    <div class="row buttonsBlock">
        <input type="submit" class="btn btn_middle money" value="Сохранить">
        <button type="button" class="btn btn_middle" id="btnMicroAddCanсel">Отмена</button>
    </div>
</form>