@extends('cabinet.role.client.credits')
@section('credit-title')
    Редактирование кредита
@endsection
@section('credit-block')
    <script>
        $(function() {
            $('.datepicker-here').datetimepicker({
                timepicker: false,
                format:'Y-m-d'
            });

            $("#credit-select [value='{{$aCredit->credit_commitments_name}}']").attr("selected", "selected");

            @foreach($aCredit->tasks as $task)
                $("#credit-task-{{$task->task_id}}").attr("checked", "checked");
            @endforeach
        });

        function goBack() {
            location.href="{!! URL::route('client.credit.details').'?type='.$type.'&id='.$aCredit->id !!}";
        }
    </script>
    @if ($type == 'consumer')
        @include('cabinet.role.client.credits.editConsumer')
    @elseif ($type == 'card')
        @include('cabinet.role.client.credits.editCard')
    @elseif ($type == 'micro')
        @include('cabinet.role.client.credits.editMicro')
    @endif
@endsection