@extends('cabinet.role.client.credits')
@section('credit-title')
    Мои кредиты
@endsection
@section('credit-block')
    @include('cabinet.role.client.credits.main')
@endsection