@extends('cabinet.role.client.credits')
@section('credit-title')
    Информация о кредите
@endsection
@section('credit-block')
    <script>
        function editCredit() {
            location.href="{!! URL::route('client.credit.edit').'?type='.$type.'&id='.$aCredit->id !!}";
        }

        function goBack() {
            location.href="{{ URL::route('client.credits') }}";
        }
    </script>

    <h3>
        @if ($type == 'consumer')
            Потребительский кредит
        @elseif ($type == 'card')
            Кредитная карта
        @elseif ($type == 'micro')
            Микрозайм
        @endif
    </h3>

    <table class="credit-info">
        <thead>
            <tr>
                <th>Наименование</th>
                <th>Данные</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $type != 'micro' ? 'Банк' : 'МФО' }}</td>
                <td>{{ $aCredit->bank_name }}</td>
            </tr>
            <tr>
                <td>Номер договора</td>
                <td>{{ $aCredit->contract_number }}</td>
            </tr>
            <tr>
                <td>Дата заключения договора</td>
                <td>{{ $aCredit->date_contract_create }}</td>
            </tr>
            <tr>
                <td>Сумма кредита</td>
                <td>{{ $aCredit->credit_sum }}</td>
            </tr>
            @if ($type != 'micro')
                <tr>
                    <td>Номер счета</td>
                    <td>{{ $aCredit->account_number }}</td>
                </tr>
            @endif
            @if ($type == 'card')
                <tr>
                    <td>Номер карты</td>
                    <td>{{ $aCredit->card_number }}</td>
                </tr>
            @endif
            @if ($type != 'micro')
                <tr>
                    <td>Наименование кредитного обязательства</td>
                    <td>{{ $aCredit->credit_commitments_name }}</td>
                </tr>
            @endif
            @if ($type != 'micro')
                <tr>
                    <td>Сумма последнего платежа</td>
                    <td>{{ $aCredit->last_pay_sum }}</td>
                </tr>
            @endif
            @if ($type != 'micro')
                <tr>
                    <td>Дата последнего платежа</td>
                    <td>{{ $aCredit->date_last_pay }}</td>
                </tr>
            @endif
            <tr>
                <td>Срок кредита</td>
                <td>{{ $aCredit->date_credit_end }}</td>
            </tr>
            @if ($type != 'micro')
                <tr>
                    <td>Среднемесячный платеж</td>
                    <td>{{ $aCredit->average_monthly_pay }}</td>
                </tr>
            @endif
            <tr>
                <td>Примечание</td>
                <td>{{ $aCredit->notation }}</td>
            </tr>
        </tbody>
    </table>
    <div class="btn-block">
        <input type="submit" class="btn btn_middle money" value="Редактировать" onclick="editCredit()">
        <button type="button" class="btn btn_middle" onclick="goBack()">Назад</button>
    </div>

    <table class="credit-info">
        <thead>
            <tr>
                <th>Задачи</th>
            </tr>
        </thead>
        <tbody>
        @if (isset($aCredit->tasks))
            @foreach($aCredit->tasks as $task)
                <tr>
                    <td>{{ $task->task }}</td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
    <div class="btn-block">
        <input type="submit" class="btn btn_middle money" value="Добавить" onclick="editCredit()">
        <button type="button" class="btn btn_middle" onclick="goBack()">Назад</button>
    </div>
@endsection