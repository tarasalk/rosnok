<div class="clientCredits">
    <script>
        $(function() {
            $('#btnConsumerAddCanсel, #btnCardAddCanсel, #btnMicroAddCanсel').click(function() {
                $('#consumerAddForm').hide();
                $('#cardAddForm').hide();
                $('#microAddForm').hide();
                $('#btnAddCreditModal').show();
            });

            $('.datepicker-here').datetimepicker({
                timepicker: false,
                format:'Y-m-d'
            });
        });

        function openAddCreditModal() {
            $('#checkTypeCreditModal').show();
        }

        function openSelectedFormAddCredit() {
            selectedCreditType = $('input[name=credit_type]:checked').val();

            switch(selectedCreditType) {
                case 'consumer':
                    element = "#consumerAddForm";
                    break;

                case 'card':
                    element = "#cardAddForm";
                    break;

                case 'micro':
                    element = "#microAddForm";
                    break;

                default:
                    return;
            }

            $(element).show();
            $('#checkTypeCreditModal').hide();
            $('#btnAddCreditModal').hide();
        }
    </script>
    <div class="row">
        <button class="btn btn_middle money" id="btnAddCreditModal" onclick="openAddCreditModal();">Добавить кредит</button>
    </div>

    @include('cabinet.role.client.credits.addConsumer')
    @include('cabinet.role.client.credits.addCard')
    @include('cabinet.role.client.credits.addMicro')

    <table class="credit-info gray">
        <thead>
        <tr>
            <th>Номер договора</th>
            <th>Банк</th>
            <th>Примечание</th>
            <th>Сумма кредита</th>
            <th>Тип</th>
        </tr>
        </thead>
        <tbody>
        @if (isset($aCredits))
            @foreach($aCredits as $type => $aCreditsByType)
                @foreach($aCreditsByType as $oCredit)
                    <tr>
                        <td>
                            <a href="{{ URL::route('client.credit.details').'?type='.$type.'&id='.$oCredit->id }}">
                                {!! $oCredit->contract_number ? $oCredit->contract_number : '<span class="unknown">Не указан</span>' !!}
                            </a>
                        </td>
                        <td>{{ $oCredit->bank_name }}</td>
                        <td>{{ $oCredit->notation }}</td>
                        <td>{{ $oCredit->credit_sum != 0 ? $oCredit->credit_sum . ' руб.' : '' }}</td>
                        <td>{{ $type }}</td>
                    </tr>
                @endforeach
            @endforeach
        @endif
        </tbody>
    </table>

    <div id="checkTypeCreditModal" class="modal_overlay" onclick="closeModal(this, event);">
        <div class="modal_window">
            <button class="m_close" onclick="closeModal(this);">&times;</button>
            <div class="modal_header border_biz">Выбор типа кредита</div>
            <div class="modal_body">
                <div>
                    <div>
                        <label><input type="radio" name="credit_type" value="consumer" checked>потребительский кредит</label>
                    </div>
                    <div>
                        <label><input type="radio" name="credit_type" value="card">кредитная карта</label>
                    </div>
                    <div>
                        <label><input type="radio" name="credit_type" value="micro">микрозайм</label>
                    </div>
                </div>
                <div class="modal_footer">
                    <button class="btn btn_middle biz" onclick="openSelectedFormAddCredit();">Подтвердить</button>
                </div>
            </div>
        </div>
    </div>
</div>