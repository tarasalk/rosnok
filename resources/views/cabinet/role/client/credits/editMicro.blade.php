<script>
    $(function() {
        $('#microEditForm').submit(function(event) {
            event.preventDefault();
            $.ajax({
                url: "{{ URL::route('client.credit.edit.micro') }}",
                type: "POST",
                data: $(this).serialize(),
                success: function(r) {
                    alert('Отредактировано');
                    goBack();
                }
            });
        });
    });
</script>
<form id="microEditForm" class="credit-form">
    <h3>Микрозайм</h3>
    <div class="row">
        <label>
            <input type="checkbox" name="credit_task[]" id="credit-task-2" value="{{$aTasks[1]->id}}">{{$aTasks[1]->task}}
        </label>
    </div>
    <div class="row">
        <div>МФО</div>
        <div><input type="text" name="mfo_id" class="full_width" value="{{$aCredit->mfo_id}}"></div>
    </div>
    <div class="row flex">
        <div>
            <div>Номер договора</div>
            <div><input type="text" name="contract_number" value="{{$aCredit->contract_number}}"></div>
        </div>
        <div>
            <div>Дата заключения договора</div>
            <div><input type="text" name="date_contract_create" class="datepicker-here" value="{{$aCredit->date_contract_create}}"></div>
        </div>
        <div>
            <div>Сумма займа</div>
            <div><input type="text" name="credit_sum" value="{{$aCredit->credit_sum}}"> руб.</div>
        </div>
        <div>
            <div>Срок займа</div>
            <div><input type="text" name="date_credit_end" class="datepicker-here" value="{{$aCredit->date_credit_end}}"></div>
        </div>
    </div>
    <h3>Задачи</h3>
    <div class="row credit-task">
        <label>
            <input type="checkbox" name="credit_task[]" id="credit-task-1" value="{{$aTasks[0]->id}}">{{$aTasks[0]->task}}
        </label>
        <label>
            <input type="checkbox" name="credit_task[]" id="credit-task-3" value="{{$aTasks[2]->id}}">{{$aTasks[2]->task}}
        </label>
        <label>
            <input type="checkbox" name="credit_task[]" id="credit-task-4" value="{{$aTasks[3]->id}}">{{$aTasks[3]->task}}
        </label>
        <label>
            <input type="checkbox" name="credit_task[]" id="credit-task-5" value="{{$aTasks[4]->id}}">{{$aTasks[4]->task}}
        </label>
        <label>
            <input type="checkbox" name="credit_task[]" id="credit-task-6" value="{{$aTasks[5]->id}}">{{$aTasks[5]->task}}
        </label>
        <label>
            <input type="checkbox" name="credit_task[]" id="credit-task-7" value="{{$aTasks[6]->id}}">{{$aTasks[6]->task}}
        </label>
        <label>
            <input type="checkbox" name="credit_task[]" id="credit-task-8" value="{{$aTasks[7]->id}}">{{$aTasks[7]->task}}
        </label>
        <label>
            <input type="checkbox" name="credit_task[]" id="credit-task-9" value="{{$aTasks[8]->id}}">{{$aTasks[8]->task}}
        </label>
    </div>
    <div class="row">
        <div>Примечание</div>
        <textarea name="notation" class="notation">{{$aCredit->notation}}</textarea>
    </div>
    <div class="btn-block">
        <input type="hidden" name="credit_id" value="{{$aCredit->id}}">
        <input type="submit" class="btn btn_middle money" value="Сохранить">
        <button type="button" class="btn btn_middle" onclick="goBack()">Отмена</button>
    </div>
</form>