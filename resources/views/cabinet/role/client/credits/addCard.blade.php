<script>
    $(function() {
        $('#cardAddForm').submit(function(event) {
            event.preventDefault();
            $.ajax({
                url: "{{URL::route('client.credit.add.card')}}",
                type: "POST",
                data: $(this).serialize(),
                success: function(r) {
                    if (r.status == 'ok') {
                        console.log('success');
                    }
                    else console.log('error');
                }
            });
        });
    });

</script>
<form id="cardAddForm" class="cardAddForm">
    <div class="row title">Добавление кредитный карты</div>
    <div class="row">
        <label>
            <input name="credit_task[]" value="{{$aTasks[1]->id}}" type="checkbox">{{$aTasks[1]->task}}
        </label>
    </div>
    <div class="row">
        <div>Банк</div>
        <div><input type="text" class="bank_name" name="bank_name"></div>
    </div>
    <div class="row block1">
        <div>
            <div>Номер карты</div>
            <div><input type="text" class="card_number" name="card_number"></div>
        </div>
        <div>
            <div>Дата выдачи карты</div>
            <div><input type='text' name="date_contract_create" class="datepicker-here date_contract"></div>
        </div>
        <div>
            <div>Сумма кредита</div>
            <div><input type="text" name="credit_sum" class="credit_sum"> руб.</div>
        </div>
        <div>
            <div>Номер договора</div>
            <div><input type="text" class="contract_number" name="contract_number"></div>
        </div>
    </div>
    <div class="row block2">
        <div>
            <div>Номер счета</div>
            <div><input type="text" class="contract_number" name="account_number"></div>
        </div>
        <div>
            <div>Наименование кредитного обязательства</div>
            <div>
                <select name="credit_commitments_name" class="credit_commitments_name">
                    <option value="">Отсутствует</option>
                    <option value="loan">Договор займа</option>
                    <option value="contract">Кредитный договор</option>
                    <option value="credit">Заявление о выдаче кредита</option>
                    <option value="accept">Соглашение на предоставление кредита</option>
                    <option value="statement">Заявление клиента о заключении договора кредитования</option>
                    <option value="agreement">Заявление на заключение Соглашения о кредитовании</option>
                    <option value="card">Заявление для оформления банковской карты</option>
                    <option value="notarget">Индивидуальные условия нецелевого кредита</option>
                    <option value="individual">Индивидуальные условия предоставления нецелевого кредита</option>
                    <option value="credit_card">Договор о выпуске и использовании кредитной карты</option>
                    <option value="form">Анкета заявление</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row block1">
        <div>
            <div>Сумма последнего платежа</div>
            <div><input type="text" name="last_pay_sum" class="last_pay_sum"></div>
        </div>
        <div>
            <div>Дата последнего платежа</div>
            <div><input type="text" name="date_last_pay" class="datepicker-here date_last_pay"></div>
        </div>
        <div>
            <div>Срок кредита</div>
            <div><input type="text" name="date_credit_end" class="date_credit_end"></div>
        </div>
        <div>
            <div>Средний ежемесячный платеж</div>
            <div><input type="text" name="average_monthly_pay" class="average_monthly_pay"></div>
        </div>
    </div>
    <div class="title">Задачи</div>
    <div class="row tasksContainer">
        <label>
            <input name="credit_task[]" value="{{$aTasks[0]->id}}" type="checkbox">{{$aTasks[0]->task}}
        </label>
        <label>
            <input name="credit_task[]" value="{{$aTasks[2]->id}}" type="checkbox">{{$aTasks[2]->task}}
        </label>
        <label>
            <input name="credit_task[]" value="{{$aTasks[3]->id}}" type="checkbox">{{$aTasks[3]->task}}
        </label>
        <label>
            <input name="credit_task[]" value="{{$aTasks[4]->id}}" type="checkbox">{{$aTasks[4]->task}}
        </label>
        <label>
            <input name="credit_task[]" value="{{$aTasks[5]->id}}" type="checkbox">{{$aTasks[5]->task}}
        </label>
        <label>
            <input name="credit_task[]" value="{{$aTasks[6]->id}}" type="checkbox">{{$aTasks[6]->task}}
        </label>
        <label>
            <input name="credit_task[]" value="{{$aTasks[7]->id}}" type="checkbox">{{$aTasks[7]->task}}
        </label>
        <label>
            <input name="credit_task[]" value="{{$aTasks[8]->id}}" type="checkbox">{{$aTasks[8]->task}}
        </label>
    </div>
    <div class="row">
        <div>Примечание</div>
        <textarea class="notation" name="notation"></textarea>
    </div>
    <div class="row buttonsBlock">
        <input type="submit" class="btn btn_middle money" value="Сохранить">
        <button type="button" class="btn btn_middle" id="btnCardAddCanсel">Отмена</button>
    </div>
</form>