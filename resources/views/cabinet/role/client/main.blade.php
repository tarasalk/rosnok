@extends('cabinet.extends.main')
@section('head')
    <link rel="stylesheet" href="{{ URL::asset('thema/css/page/client.css') }}">
    <script>
        $(function() {
            /*$('#payBtn').click(function() {
                $.ajax({
                    url: "{{URL::route('clientPayConfirm')}}",
                    type: "POST",
                    success: function(r) {
                        if(r.success) {
                            $('#payResultMessage').html('оплатил');
                        }
                    }
                });
            });*/
        })
    </script>
@endsection
@section('content_wrapper')
    <div class="title">
        Карта клиента
    </div>
    <div class="content">
        <div class="clientMain">
            <div class="mainDocsBlock">
                <div style="font-size: 24px;margin-bottom: 10px;">Основные документы</div>
                <div>
                    @foreach($aMainDocs as $aDoc)
                        <div class="lineGrid" style="margin-top: 3px;">
                            <div class="icon-word" style="margin-right: 10px;"></div>
                            <div><a href="{{URL::route('docsDownloadMain', ['doc_name' => $aDoc['small_name']])}}">{{$aDoc['name']}}</a></div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="clientNameBlock">
                {{$oClientPeople->base->last_name}} {{$oClientPeople->base->first_name}}
                ID: {{$oClientPeople->user_id}}
            </div>
            <div class="clientInfoBlock">
                <div class="avatar"><img src="{{$oClientPeople->base->avatar}}"></div>
                <table class="info">
                    <tr>
                        <td>Дата регистрации:</td>
                        <td>{{$oClient->created_at}}</td>
                    </tr>
                    <tr>
                        <td>Начало работы с клиентом:</td>
                        <td>{{$oClient->date_start_work}}</td>
                    </tr>
                    <tr>
                        <td>Тариф:</td>
                        <td>{{$oClient->rate_pay}}</td>
                    </tr>
                    <tr>
                        <td>Период обслуживания:</td>
                        <td>TODO</td>
                    </tr>
                    <tr>
                        <td>Социальный статус:</td>
                        <td>{{$oClient->social_status}}</td>
                    </tr>
                    <tr>
                        <td>Телефон:</td>
                        <td>{{$oClientPeople->phone->country}}{{$oClientPeople->phone->phone}}</td>
                    </tr>
                    <tr>
                        <td>Офис:</td>
                        <td>{{$oClient->office->office_name}}</td>
                    </tr>
                    <tr>
                        <td>Агент:</td>
                        <td>
                            {{$oAgent->base->last_name}}
                            {{$oAgent->base->first_name}}
                            + {{$oAgent->phone->country}} {{$oAgent->phone->phone}}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection

