@extends('cabinet.extends.main')
@section('head')
    <link rel="stylesheet" href="{{ URL::asset('thema/css/page/client.css') }}">
@endsection
@section('content_wrapper')
    <div class="title">
        @yield('credit-title')
    </div>
    <div class="content">
        <div class="credit-block">
            @yield('credit-block')
        </div>
    </div>
@endsection