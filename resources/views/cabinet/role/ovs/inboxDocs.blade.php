{{--*/ use \App\Models\InboxDocs /*--}}

@extends('cabinet.extends.main')
@section('head')
    <link rel="stylesheet" href="{{ URL::asset('thema/css/page/operator.css') }}">
@endsection
@section('content_wrapper')
    <div class="title">
        Входящие документы
    </div>
    <div class="content">
        <script>
            $(function() {
                $('#operatorDocsFromClients').dataTable({
                    "order": [[ 5, "desc" ]],
                });

                $(".fancybox").fancybox();

                $("#modal-doc-accepted").click(function() {
                    $('#edit_doc_modal textarea[name=notation]').parent().hide();
                    $('#edit_doc_modal textarea[name=name]').parent().show();
                });

                $("#modal-doc-rebuke").click(function() {
                    $('#edit_doc_modal textarea[name=notation]').parent().show();
                    $('#edit_doc_modal textarea[name=name]').parent().show();
                });
                $("#modal-doc-denied").click(function() {
                    $('#edit_doc_modal textarea[name=notation]').parent().show();
                    $('#edit_doc_modal textarea[name=name]').parent().hide();
                });
            });

            function openSenderNotation(notation) {
                $('#sender_notation_modal div[name=notation]').text(notation);
                $('#sender_notation_modal').show();
            }

            function openEditDoc(doc_id) {
                $.ajax({
                    url: "{{URL::route('ovsGetInboxDocById')}}",
                    type: "POST",
                    data: {
                        doc_id: doc_id,
                    },
                    success: function(r) {
                        if (r.status == 'ok') {
                            $('#modal_doc_id').html(doc_id);
                            $('#edit_doc_modal textarea[name=name]').text(r.oDoc.name);
                            $('#edit_doc_modal textarea[name=notation]').text(r.oDoc.notation);
                            $("input[name='doc_status'][value="+r.oDoc.status+"]").prop('checked', true);

                            $('#modal-doc-accepted').trigger('click');
                            $('#edit_doc_modal').show();
                        }
                        else toastr.error('документ не найден');
                    }
                });
            }

            function changeDocStatus() {
                doc_id = $('#modal_doc_id').html();
                name = $('#edit_doc_modal textarea[name=name]').val();
                notation = $('#edit_doc_modal textarea[name=notation]').val();
                status = $("input[name='doc_status']:checked").val();

                switch (status) {
                    case '{{InboxDocs::status_accepted}}':
                    case '{{InboxDocs::status_rebuke}}':
                        if (name == '') {
                            toastr.error('Введите название документа');
                            return;
                        }
                        break;
                }

                $.ajax({
                    url: "{{URL::route('ovsInboxDocChecked')}}",
                    type: "POST",
                    data: {
                        doc_id: doc_id,
                        notation: notation,
                        status: status,
                        name: name
                    },
                    success: function(r) {
                        if (r.status == 'ok') {
                            toastr.success('Успешно');
                            $('#edit_doc_modal').hide();

                            setTimeout(function () {location.reload();}, 1000);
                        }
                        else  {
                            toastr.error('Ошибка');
                            console.log(r.error);
                        }
                    }
                });
            }

            function sendLawyer(doc_id, thiselement) {
                $(thiselement).prop('disabled', true);

                $.ajax({
                    url: "{{URL::route('ovsInboxDocSendLawyer')}}",
                    type: "POST",
                    data: {
                        doc_id: doc_id
                    },
                    success: function(r) {
                        if (r.status == 'ok')
                            toastr.info('Успешно');
                        else
                            toastr.error('Ошибка');
                    }
                });
            }
        </script>
        <div class="operatorInboxDocs">
            <div class="row menu_horizontal" style="margin-bottom: 10px;margin-top: 10px;">
                <a href="{{URL::route('ovsInboxDocs', ['filter' => 'all'])}}">Все ({{$aDocsCategoriesCount->all}})</a>
                <a href="{{URL::route('ovsInboxDocs', ['filter' => 'accepted'])}}">Принятые ({{$aDocsCategoriesCount->accepted}})</a>
                <a href="{{URL::route('ovsInboxDocs', ['filter' => 'rebuke'])}}">Принятые с замечаниями ({{$aDocsCategoriesCount->rebuke}})</a>
                <a href="{{URL::route('ovsInboxDocs', ['filter' => 'denied'])}}">Отклоненные ({{$aDocsCategoriesCount->denied}})</a>
                <a href="{{URL::route('ovsInboxDocs', ['filter' => 'new'])}}">Новые ({{$aDocsCategoriesCount->new}})</a>
            </div>
            <table id="operatorDocsFromClients" class="gray">
                <thead>
                <tr>
                    <th>№</th>
                    <th>Агент</th>
                    <th>Клиент</th>
                    <th>Название</th>
                    <th>Примечание</th>
                    <th>Добавлен</th>
                    <th>Обработан</th>
                    <th></th>
                    <th>Передать юристу</th>
                </tr>
                </thead>
                <tbody>
                @foreach($aDocs as $oDoc)
                    <tr>
                        <td class="num"
                            @if     ($oDoc->status == InboxDocs::status_denied)
                            style="box-shadow: -5px 0 0 0 #e31e25;"
                            @elseif ($oDoc->status == InboxDocs::status_rebuke)
                            style="box-shadow: -5px 0 0 0 #ef7f1b;"
                            @elseif ($oDoc->status == InboxDocs::status_accepted)
                            style="box-shadow: -5px 0 0 0 #61b239;"
                                @endif
                        >
                            <div>{{$oDoc->id}}</div>
                            @if (!empty($oDoc->sender_notation))
                                <div class="icon_glaz_blue pointer" title="примечание отправителя" onclick="openSenderNotation('{{$oDoc->sender_notation}}');"></div>
                            @endif
                            @if (!empty($oDoc->file_path))
                                @if(strripos($oDoc->file_path, '.jpg'))
                                    <div class="icon_info_blue fancybox pointer" title="файлы" href="{{$oDoc->file_path}}"></div>
                                @else
                                    <div class="icon_info_blue pointer" title="файлы">
                                        <a class="maximumLink" target='blank' href="{{$oDoc->file_path}}"></a>
                                    </div>
                                @endif
                            @endif
                        </td>
                        <td>{{$oDoc->agent_name}}</td>
                        <td>
                            <a href="{{URL::route('agentClientCab', ['client_id' => $oDoc->client_id])}}">
                                {{$oDoc->client_name}}
                            </a>
                        </td>
                        <td>{{$oDoc->name}}</td>
                        <td>{{$oDoc->operator_notation}}</td>
                        <td>{{$oDoc->created_at}}</td>
                        <td>{{$oDoc->date_operator_check}}</td>
                        <td onclick="openEditDoc('{{$oDoc->id}}')">
                            <span class="icon_pencil pointer"></span>
                        </td>
                        <td>
                            <input type="checkbox" onchange="sendLawyer('{{$oDoc->id}}', this)" @if($oDoc->send_lawyer) checked disabled @endif>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div id="sender_notation_modal" class="modal_overlay" onclick="closeModal(this, event);">
            <div class="modal_window">
                <button class="m_close" onclick="closeModal(this);">&times;</button>
                <div class="modal_header border_biz">Примечание агента</div>
                <div class="modal_body">
                    <div name="notation"></div>
                    <div class="modal_footer">
                        <button class="btn btn_middle biz" onclick="closeModal(this);">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="download_doc_modal" class="modal_overlay" onclick="closeModal(this, event);">
            <div class="modal_window">
                <button class="m_close" onclick="closeModal(this);">&times;</button>
                <div class="modal_header border_biz">Файлы</div>
                <div class="modal_body">
                    <div name="files"></div>
                    <div class="modal_footer">
                        <button class="btn btn_middle biz" onclick="closeModal(this);">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="edit_doc_modal" class="modal_overlay" onclick="closeModal(this, event);">
            <div class="modal_window">
                <button class="m_close" onclick="closeModal(this);">&times;</button>
                <div class="modal_header border_biz">Обработка док. ID: <span id="modal_doc_id"></span></div>
                <div class="modal_body">
                    <div>
                        <label><input type="radio" name="doc_status" value="{{InboxDocs::status_accepted}}" id="modal-doc-accepted">принять</label>
                        <label><input type="radio" name="doc_status" value="{{InboxDocs::status_rebuke}}" id="modal-doc-rebuke">принять с замечаниями</label>
                        <label><input type="radio" name="doc_status" value="{{InboxDocs::status_denied}}" id="modal-doc-denied">отклонить</label>
                    </div>
                    <div><textarea name="name" style="width: 100%;height: 100px;margin-top:10px;" placeholder="Название"></textarea></div>
                    <div><textarea name="notation" style="width: 100%;height: 150px;margin-top: 10px;" placeholder="Примечание"></textarea></div>
                    <div class="modal_footer">
                        <button class="btn btn_middle biz" onclick="changeDocStatus()">Сохранить</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

