@extends('extends.main')

@section('main_container')
    <div class="cabinet">
        <div class="sidebar">@include('cabinet.include.sidebar')</div>
        <div class="content_wrapper">
            @yield('content_wrapper')
        </div>
    </div>
@endsection