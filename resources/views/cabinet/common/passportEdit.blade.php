@extends('cabinet.extends.main')
@section('head')
    <link rel="stylesheet" href="{{ URL::asset('thema/css/page/passportEdit.css') }}">
@endsection
@section('content_wrapper')
    <div class="title">
        Паспортные данные
    </div>
    <div class="content">
        @include('cabinet.common.include.passport')
    </div>
@endsection