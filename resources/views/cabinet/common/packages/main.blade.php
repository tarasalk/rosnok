@extends('cabinet.extends.main')
@section('head')
    <link rel="stylesheet" href="{{ URL::asset('thema/css/page/packages.css') }}">
@endsection
@section('content_wrapper')
    <div class="title">
        Бизнес-портфели
    </div>
    <div class="content">
        <script>
            function buyPackage(packName) {

            }
        </script>
        <div class="packagesMain">
            <div class="list">
                <button class="btn btn_middle biz" onclick="buyPackage('lite');">Лайт</button>
                <button class="btn btn_middle biz" onclick="buyPackage('pro');">Про</button>
                <button class="btn btn_middle biz" onclick="buyPackage('premium');">Премиум</button>
            </div>
            <div class="packagesBuy" style="margin-top: 60px;">
                <div>
                    {{Auth::user()->getFirstName()}}, укажите номер мобильного Вашего наставника,
                    чтобы получить скидку 2000 рублей на покупку
                    бизнес-портфеля Лайт.
                </div>
                <div class="inputPhone">
                    <select name="" id="">
                        <option value="7">+7</option>
                        <option value="375">+375</option>
                        <option value="38">+38</option>
                    </select>
                    <input type="text">
                    <button class="btn btn_middle biz">Проверить</button>
                </div>
                <div class="parentHello" style="margin-top: 60px;">
                    <div class="parentBlock">
                        <div><img src=""></div>
                        <div>{{Auth::user()->getFirstName()}}, привет. Я твой наставник, дарю тебе скидку.</div>
                    </div>
                    <div class="buttons">
                        <button class="btn btn_middle biz">Назад к выбору наставника</button>
                        <button class="btn btn_middle biz">Купить сейчас пакет за ...руб.</button>
                    </div>
                </div>
                <div class="choicePayType" style="margin-top: 60px;">
                    <button class="btn btn_middle biz">Выбрать другой портфель</button>
                    <button class="btn btn_middle biz">Оплатить с лицевого счета</button>
                    <button class="btn btn_middle biz">Оплатить с кошелька Яндекс</button>
                </div>
                <div id="need_more_money_modal" class="modal_overlay" onclick="closeModal(this, event);" style="display: none;">
                    <div class="modal_window">
                        <button class="m_close" onclick="closeModal(this);">&times;</button>
                        <div class="modal_header border_biz">Недостаточно средств</div>
                        <div class="modal_body">
                            {{Auth::user()->getFirstName()}}, на балансе вашего лицевого счета мало денег для покупки выбраного портфеля.
                            У вас ...руб., а нужно ...руб.
                            Пополнить лицевой счет с кошелька Яндекс.Деньги?
                            <div class="modal_footer">
                                <button class="btn btn_middle biz">Вернуться назад</button>
                                <button class="btn btn_middle biz">Пополнить сейчас</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="margin-top: 60px;">
                    Вы будете перенаправлены в кошелек Яндекс.Деньги для завершения оплаты бизнес пакета ... стоимостью ...рублей.<br>
                    После покупки портфеля Ваш наставник получит уведомление и поставит Вас в структуру партнерской программы. Вы получите СМС об успешном завершении регистрации.
                    Данные для связи с наставником: Иван Иванов, +7 987 654-32-10
                    <button class="btn">Купить</button>
                </div>
            </div>
        </div>
    </div>
@endsection