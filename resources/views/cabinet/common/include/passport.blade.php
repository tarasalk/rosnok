<script>
    $(function(){
        $.protip({
            defaults: {
                position: 'top',
                trigger: 'sticky',
                autoHide: 3000,
                size: 'small'
            }
        });

        $('input[type=file]').styler();

        jQuery.validator.addMethod("unit_code", function(value, element, param) {
            return this.optional(element) || /^[0-9]{3}-[0-9]{3}$/i.test(value);
        }, "введите шесть цифр. Пример: 000-000");
        jQuery.validator.addMethod("pass_series", function(value, element, param) {
            return this.optional(element) || /^[0-9]{4}$/i.test(value);
        }, "введите четыре цифры. Пример: 1234");
        jQuery.validator.addMethod("pass_number", function(value, element, param) {
            return this.optional(element) || /^[0-9]{6}$/i.test(value);
        }, "введите шесть цифер. Пример: 123456");

        var val = $("#passportEditForm").validate({
            rules: {
                last_name: {
                    required: true,
                    minlength: 2,
                    rusWord: true
                },
                first_name: {
                    required: true,
                    minlength: 2,
                    rusWord: true
                },
                middle_name: {
                    required: true,
                    minlength: 2,
                    rusWord: true
                },
                gender: {
                    required: true
                },
                birthday: {
                    required: true,
                    date: true
                },
                pass_series: {
                    required: true,
                    pass_series: true
                },
                pass_number: {
                    required: true,
                    pass_number: true
                },
                pass_issued: {
                    required: true,
                    minlength: 2
                },
                pass_issued_date: {
                    required: true,
                    date: true
                },
                unit_code: {
                    required: true,
                    unit_code: true
                },
                birth_republic: {
                    required: true,
                    minlength: 2
                },
                birth_city: {
                    required: true,
                    minlength: 2
                },
                registration_republic: {
                    required: true,
                    minlength: 2
                },
                registration_city: {
                    required: true,
                    minlength: 2
                },
                registration_street: {
                    required: true,
                    minlength: 2
                },
                registration_house: {
                    required: true,
                    minlength: 1
                },
                registration_room: {
                    required: true,
                    minlength: 1
                },
                registration_date: {
                    required: true,
                    date: true
                },
                mail_republic: {
                    required: true,
                    minlength: 2
                },
                mail_city: {
                    required: true,
                    minlength: 2
                },
                mail_street: {
                    required: true,
                    minlength: 2
                },
                mail_house: {
                    required: true,
                    minlength: 1
                },
                mail_room: {
                    required: true,
                    minlength: 1
                }
            },
            // костыль от input default value
            onfocusout: function(element) { $(element).valid(); },
            errorPlacement: function(error, element) {
                element.protipShow({
                    title: error.text()
                });
            },
            submitHandler: function(form) {
                submitData();
            }
        });

        $("input[name=unit_code]").mask("999-999");
        $("input[name=pass_series]").mask("9999");
        $("input[name=pass_number]").mask("999999");

        // принудительная валидация
        @if (isset($passportData))
            val.form();
        @endif

        $('input[type=file]').fileupload({
            dataType: 'json',
            url: "{{URL::route('passportScansUpload')}}",
            done: function (e, data) {
                $('#' + $(this).attr('name')).val(data.result.response.path);
            }
        });

        $('.datepicker-here').datetimepicker({
            timepicker: false,
            format:'Y-m-d'
        });
    });

    function submitData() {
        $.ajax({
            url: "{{URL::route('passportCreateOrUpdate')}}",
            type: "POST",
            data: $('#passportEditForm').serialize(),
            success: function(r) {
                if (r.status == 'ok')
                    console.log('success');
                else
                    console.log('error');
            }
        });
    }

    function copyRegistrationData() {
        $('input[name=mail_republic]').val($('input[name=registration_republic]').val());
        $('input[name=mail_city]').val($('input[name=registration_city]').val());
        $('input[name=mail_street]').val($('input[name=registration_street]').val());
        $('input[name=mail_house]').val($('input[name=registration_house]').val());
        $('input[name=mail_corps]').val($('input[name=registration_corps]').val());
        $('input[name=mail_room]').val($('input[name=registration_room]').val());

        $("#passportEditForm").validate().form();
    }
</script>
<div class="passportEdit">
    <form id="passportEditForm">
        <div class="block1 scans">
            <div>
                <div class="block1_title">Сканы двух страниц паспорта</div>
                <div>
                    <input type="file" name="pass_scan1" class="smallInput">
                    <input type="file" name="pass_scan2" class="smallInput">
                    <input type="hidden" id="pass_scan1" name="pass_scan1">
                    <input type="hidden" id="pass_scan2" name="pass_scan2">
                </div>
            </div>
            <div>
                <div class="block1_title">Сканы доверенности</div>
                <div>
                    <input type="file" name="proxy_scan1" class="smallInput">
                    <input type="file" name="proxy_scan2" class="smallInput">
                    <input type="hidden" id="proxy_scan1" name="proxy_scan1">
                    <input type="hidden" id="proxy_scan2" name="proxy_scan2">
                </div>
            </div>
        </div>
        <div class="block1">
            <div>
                <div class="block1_title">Фамилия</div>
                <div><input type="text" name="last_name" value="{{$passportData->last_name or ''}}"></div>
            </div>
            <div>
                <div class="block1_title">Имя</div>
                <div><input type="text" name="first_name" value="{{$passportData->first_name or ''}}"></div>
            </div>
            <div>
                <div class="block1_title">Отчество</div>
                <div><input type="text" name="middle_name" value="{{$passportData->middle_name or ''}}"></div>
            </div>
            <div>
                <div class="block1_title">Дата рождения</div>
                <div><input type="text" name="birthday" readonly class="datepicker-here" value="{{$passportData->birthday or ''}}"></div>
            </div>
            <div>
                <div class="block1_title">Пол</div>
                <div class="gender">
                    <label for="gender_male">
                        М<input id="gender_male" name="gender" value="male" type="radio" @if (isset($passportData) && ($passportData->gender) == 'male') checked @endif>
                    </label>
                    <label for="gender_female">
                        Ж<input id="gender_female" name="gender" value="female" type="radio"  @if (isset($passportData) && ($passportData->gender) == 'female') checked @endif>
                    </label>
                </div>
            </div>
        </div>
        <div class="block1 b3">
            <div>
                <div class="block1_title">Дата выдачи паспорта</div>
                <div><input type="text" name="pass_issued_date" readonly class="datepicker-here" value="{{$passportData->pass_issued_date or ''}}"></div>
            </div>
            <div>
                <div class="block1_title">Серия и номер паспорта</div>
                <div class="serial_number">
                    <input type="text" name="pass_series" value="{{$passportData->pass_series or ''}}">
                    <input type="text" name="pass_number" value="{{$passportData->pass_number or ''}}">
                </div>
            </div>
            <div>
                <div class="block1_title">Код подразделения</div>
                <div><input type="text" name="unit_code" value="{{$passportData->unit_code or ''}}"></div>
            </div>
        </div>
        <div class="block1">
            <div class="full_block">
                <div class="block1_title">Кем выдан паспорт</div>
                <div><input type="text" name="pass_issued" value="{{$passportData->pass_issued or ''}}"></div>
            </div>
        </div>
        <div class="pe_title">Место рождения</div>
        <div class="block1 birthplace">
            <div>
                <div class="block1_title">Республика (область, край)</div>
                <div><input type="text" name="birth_republic" value="{{$passportData->birth_republic or ''}}"></div>
            </div>
            <div>
                <div class="block1_title">Город (посёлок, село)</div>
                <div>
                    <input type="text" name="birth_city" value="{{$passportData->birth_city or ''}}">
                </div>
            </div>
        </div>
        <div class="pe_title">Адрес регистрации</div>
        <div class="block1 registration_address">
            <div>
                <div class="block1_title">Республика (область, край)</div>
                <div><input type="text" class="republic" name="registration_republic" value="{{$passportData->registration_republic or ''}}"></div>
            </div>
            <div>
                <div class="block1_title">Город (посёлок, село)</div>
                <div>
                    <input type="text" class="city" name="registration_city" value="{{$passportData->registration_city or ''}}">
                </div>
            </div>
            <div>
                <div class="block1_title">Улица (проспект, переулок)</div>
                <div>
                    <input type="text" class="street" name="registration_street" value="{{$passportData->registration_street or ''}}">
                </div>
            </div>
            <div>
                <div class="block1_title">Дом</div>
                <div>
                    <input type="text" class="house" name="registration_house" value="{{$passportData->registration_house or ''}}">
                </div>
            </div>
            <div>
                <div class="block1_title">Корпус</div>
                <div>
                    <input type="text" class="corps" name="registration_corps" value="{{$passportData->registration_corps or ''}}">
                </div>
            </div>
            <div>
                <div class="block1_title">Квартира</div>
                <div>
                    <input type="text" class="room" name="registration_room" value="{{$passportData->registration_room or ''}}">
                </div>
            </div>
        </div>
        <div class="block1">
            <div>
                <div class="block1_title">Дата регистрации</div>
                <div>
                    <input type="text" name="registration_date" readonly class="datepicker-here" value="{{$passportData->registration_date or ''}}">
                </div>
            </div>
        </div>
        <div class="pe_title">Адрес для писем и документов <label class="em_title"><input type="checkbox" onchange="copyRegistrationData()">Совпадает с адресом регистрации</label></div>
        <div class="block1 email_address">
            <div>
                <div class="block1_title">Республика (область, край)</div>
                <div><input type="text" class="republic" name="mail_republic" value="{{$passportData->mail_republic or ''}}"></div>
            </div>
            <div>
                <div class="block1_title">Город (посёлок, село)</div>
                <div>
                    <input type="text" class="city" name="mail_city" value="{{$passportData->mail_city or ''}}">
                </div>
            </div>
            <div>
                <div class="block1_title">Улица (проспект, переулок)</div>
                <div>
                    <input type="text" class="street" name="mail_street" value="{{$passportData->mail_street or ''}}">
                </div>
            </div>
            <div>
                <div class="block1_title">Дом</div>
                <div>
                    <input type="text" class="house" name="mail_house" value="{{$passportData->mail_house or ''}}">
                </div>
            </div>
            <div>
                <div class="block1_title">Корпус</div>
                <div>
                    <input type="text" class="corps" name="mail_corps" value="{{$passportData->mail_corps or ''}}">
                </div>
            </div>
            <div>
                <div class="block1_title">Квартира</div>
                <div>
                    <input type="text" class="room" name="mail_room" value="{{$passportData->mail_room or ''}}">
                </div>
            </div>
        </div>
        <div style="margin-top: 10px;">
            <input type="submit" class="btn btn_middle biz" value="Сохранить">
        </div>
    </form>
</div>