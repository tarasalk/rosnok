@if (!Auth::check() )
    <script>
        $(function() {
            @if (isset($need_register))
                $('#auth_register_modal').show();
                $('#auth_register_modal input[name=arm_first_name]').val('{{$need_register['first_name']}}');
                $('#auth_register_modal input[name=arm_last_name]').val('{{$need_register['last_name']}}');
            @endif

            $('#auth_by_vk').click(function() {
                window.location.href = "{{URL::route('authVk')}}";
            });

            $('#open_auth_by_password').click(function() {
                $('#auth_method_modal').hide();
                $('#auth_password_modal').show();
            });

            $.protip({
                defaults: {
                    autoHide: 3000,
                    position: 'right',
                    trigger: 'sticky'
                }
            });
        });

        function openRegisterModal() {
            $('#auth_method_modal').hide();
            $('#auth_register_modal').show();
        }

        function openPasswordRecovery() {
            $('#auth_password_modal').hide();
            $('#password_recovery_modal').show();
        }

        function sendPhoneCode() {
            var country = $('select[name=arm_country] option:selected').val();
            var phone = $('input[name=arm_phone]').val();
            var captcha = $('input[name=arm_captcha]').val();
            var first_name = $('input[name=arm_first_name]').val();
            var last_name = $('input[name=arm_last_name]').val();

            $.ajax({
                url: "{{URL::route('sendPhoneCode')}}",
                type: "POST",
                data: {
                    captcha: captcha,
                    phone: phone,
                    country: country,
                    first_name: first_name,
                    last_name: last_name
                },
                success: function(r) {
                    if(r.status == 'ok') {
                        $('#auth_register_modal').hide();
                        $('#auth_phone_confirm_modal').show();
                    }
                    else {
                        console.log(r.errors);
                        getCaptchaImg('div[name=register_captcha]');

                        for (var group in r.validation) {
                            r.validation[group].forEach(function(item, i, arr) {
                                switch (item) {
                                    case 'The first name field is required.':
                                        $('input[name=arm_first_name]').protipShow({
                                            position: 'left',
                                            title: 'введите имя'
                                        });
                                        break;
                                    case 'The last name field is required.':
                                        $('input[name=arm_last_name]').protipShow({
                                            title: 'введите фамилию'
                                        });
                                        break;
                                    case 'The phone field is required.':
                                        $('input[name=arm_phone]').protipShow({
                                            title: 'введите телефон'
                                        });
                                        break;
                                    case 'The captcha field is required.':
                                        $('input[name=arm_captcha]').protipShow({
                                            title: 'введите код'
                                        });
                                        break;
                                    case 'validation.captcha':
                                        $('input[name=arm_captcha]').protipShow({
                                            title: 'неверный код'
                                        });
                                        break;
                                }
                            });
                        }

                        if (r.errors && r.errors[0] == 'phone_exists') {
                            $('input[name=arm_phone]').protipShow({
                                title: 'этот телефон уже зарегистрирован'
                            });
                        }
                    }
                }
            });
        }

        function confirmPhoneCode() {
            var country = $('select[name=arm_country] option:selected').val();
            var phone = $('input[name=arm_phone]').val();
            var code = $('#apcm_sms_code').val();

            $.ajax({
                url: "{{URL::route('confirmPhoneCode')}}",
                type: "POST",
                data: {
                    phone: phone,
                    country: country,
                    code: code
                },
                success: function(r) {
                    if (r.status == 'ok') {
                        window.location.href = '/';
                    }
                }
            });
        }

        function authByPassword() {
            var country = $('select[name=apm_country] option:selected').val();
            var phone = $('input[name=apm_phone]').val();
            var password = $('input[name=apm_password]').val();

            $.ajax({
                url: "{{URL::route('authPassword')}}",
                type: "POST",
                data: {
                    country: country,
                    phone: phone,
                    password: password
                },
                success: function(r) {
                    if(r.status == 'ok') {
                        window.location.reload();
                    }
                    else {
                        for (var group in r.validation) {
                            r.validation[group].forEach(function(item, i, arr) {
                                switch (item) {
                                    case 'The phone field is required.':
                                        $('input[name=apm_phone]').protipShow({
                                            title: 'введите телефон'
                                        });
                                        break;
                                    case 'The password field is required.':
                                        $('input[name=apm_password]').protipShow({
                                            title: 'введите пароль'
                                        });
                                        break;
                                }
                            });
                        }

                        if (r.errors[0] == 'user_not_found') {
                            $('input[name=apm_phone]').protipShow({
                                title: 'неверный телефон или пароль'
                            });
                        }

                        if (r.errors[0] == 'undefined_error') {
                            console.log('404?');
                        }
                    }
                }
            });
        }

        function recoveryPassword() {
            var country = $('select[name=prm_country] option:selected').val();
            var phone = $('input[name=prm_phone]').val();
            var captcha = $('input[name=prm_captcha]').val();

            $.ajax({
                url: "{{URL::route('recoveryPassword')}}",
                type: "POST",
                data: {
                    country: country,
                    phone: phone,
                    captcha: captcha
                },
                success: function(r) {
                    if(r.status == 'ok') {
                        $('#password_recovery_modal').hide();
                        $('#auth_password_modal').show();
                    }
                    else {
                        console.log(r);
                        getCaptchaImg('div[name=recovery_password_captcha]');

                        for (var group in r.validation) {
                            r.validation[group].forEach(function(item, i, arr) {
                                switch (item) {
                                    case 'validation.captcha':
                                        $('input[name=prm_captcha]').protipShow({
                                            title: 'неверный код'
                                        });
                                        break;
                                    case 'The captcha field is required.':
                                        $('input[name=prm_captcha]').protipShow({
                                            title: 'введите код'
                                        });
                                        break;
                                    case 'The phone field is required.':
                                        $('input[name=prm_phone]').protipShow({
                                            title: 'введите телефон'
                                        });
                                        break;
                                }
                            });
                        }
                    }
                }
            });
        }
    </script>
    <div id="auth_method_modal" class="modal_overlay" onclick="closeModal(this, event);">
        <div class="modal_window">
            <button class="m_close" onclick="closeModal(this);">&times;</button>
            <div class="modal_header border_biz">Авторизация</div>
            <div class="modal_body">
                <div class="auth_method" id="auth_by_vk">
                    <div class="icon_auth_vk"></div>
                    <div>Войти со своей страницы ВКонтакте</div>
                </div>
                <div class="auth_method" id="open_auth_by_password">
                    <div class="icon_auth_pass"></div>
                    <div>Войти по паролю</div>
                </div>
                <div><a onclick="openRegisterModal()">Еще не зарегистрированы?</a></div>
            </div>
        </div>
    </div>
    <div id="auth_password_modal" class="modal_overlay" onclick="closeModal(this, event);">
        <div class="modal_window">
            <button class="m_close" onclick="closeModal(this);">&times;</button>
            <div class="modal_header border_biz">Авторизация</div>
            <div class="modal_body">
                <div class="block_body">
                    <div class="block_title">Телефон</div>
                    <div>
                        <select name="apm_country">
                            <option value="7">+7 (RU)</option>
                            <option value="38">+38 (UA)</option>
                            <option value="375">+375 (BY)</option>
                            <option value="7">+7 (KZ)</option>
                        </select>
                        <input type="text" name="apm_phone">
                    </div>
                </div>
                <div class="block_body">
                    <div class="block_title">Пароль</div>
                    <div><input name="apm_password" type="text"></div>
                </div>
                <div class="modal_footer">
                    <button class="btn btn_middle biz" onclick="authByPassword()">Войти</button>
                    <button class="btn btn_middle biz" onclick="openPasswordRecovery()">Восстановление пароля</button>
                </div>
            </div>
        </div>
    </div>
    <div id="password_recovery_modal" class="modal_overlay" onclick="closeModal(this, event);">
        <div class="modal_window">
            <button class="m_close" onclick="closeModal(this);">&times;</button>
            <div class="modal_header border_biz">Восстановление пароля</div>
            <div class="modal_body">
                <div class="block_body1">
                    Для восстановления пароля введите телефон и код с картинки
                </div>
                <div class="block_body">
                    <div class="block_title">Телефон</div>
                    <div>
                        <select name="prm_country">
                            <option value="7">+7 (RU)</option>
                            <option value="38">+38 (UA)</option>
                            <option value="375">+375 (BY)</option>
                            <option value="7">+7 (KZ)</option>
                        </select>
                        <input type="text" name="prm_phone">
                    </div>
                </div>
                <div class="block_body">
                    <div name="recovery_password_captcha">{!!captcha_img('rosnok')!!}</div>
                    <div><input type="text" name="prm_captcha"></div>
                </div>
                <div class="modal_footer">
                    <button class="btn btn_middle biz" onclick="recoveryPassword()">Восстановить пароль</button>
                </div>
            </div>
        </div>
    </div>
    <div id="auth_register_modal" class="modal_overlay" onclick="closeModal(this, event);">
        <div class="modal_window">
            <button class="m_close" onclick="closeModal(this);">&times;</button>
            <div class="modal_header border_biz">Регистрация</div>
            <div class="modal_body">
                <div class="block_body1">
                    Для регистрации введите телефон и код с картинки
                </div>
                <div class="block_body" style="display:flex;">
                    <div>
                        <div class="block_title">Имя</div>
                        <div>
                            <input type="text" name="arm_first_name">
                        </div>
                    </div>
                    <div style="margin-left: 10px;">
                        <div class="block_title">Фамилия</div>
                        <div>
                            <input type="text" name="arm_last_name">
                        </div>
                    </div>
                </div>
                <div class="block_body">
                    <div class="block_title">Телефон</div>
                    <div>
                        <select name="arm_country">
                            <option value="7">+7 (RU)</option>
                            <option value="38">+38 (UA)</option>
                            <option value="375">+375 (BY)</option>
                            <option value="7">+7 (KZ)</option>
                        </select>
                        <input type="text" name="arm_phone">
                    </div>
                </div>
                <div class="block_body sms_warning">
                    На указанный телефон придет бесплатное СМС-сообщение с кодом для подтверждения телефона
                </div>
                <div class="block_body">
                    <div name="register_captcha">{!!captcha_img('rosnok')!!}</div>
                    <div><input type="text" name="arm_captcha"></div>
                </div>
                <div class="modal_footer">
                    <button class="btn btn_middle biz" onclick="sendPhoneCode()">Отправить код</button>
                </div>
            </div>
        </div>
    </div>
    <div id="auth_phone_confirm_modal" class="modal_overlay" onclick="closeModal(this, event);">
        <div class="modal_window">
            <button class="m_close" onclick="closeModal(this);">&times;</button>
            <div class="modal_header border_biz">Регистрация</div>
            <div class="modal_body">
                <div class="block_body1">
                    На указанный Вами телефон в течении нескольких минут<br>
                    придет СМС-подтверждение. Введите его в поле ниже
                </div>
                <div class="block_body">
                    <div class="block_title">Код</div>
                    <div>
                        <input type="text" id="apcm_sms_code">
                    </div>
                </div>
                <div class="modal_footer">
                    <button class="btn btn_middle biz" onclick="confirmPhoneCode()">Подтвердить</button>
                </div>
            </div>
        </div>
    </div>
@endif