<div class="header">
    <div class="center">
        <div class="menu" id="header_menu">
            <a href="{{URL::route('main')}}">Главная</a>
            <a href="{{URL::route('articleSudAll')}}">С.У.Д</a>
            <a href="{{URL::route('articleNewsAll')}}">Новости и аналитика</a>
        </div>
        <div class="rightBlock">
            @if (Auth::check())
                <div>
                    <a href="{{URL::route('cabinetDesktop')}}">
                        В кабинет
                    </a>
                </div>
                <div><button class="btn btn_small biz" onclick="window.location.href='/auth/logout'">Выйти</button></div>
            @else
                <div><button class="btn btn_small biz" onclick="openModalById('auth_method_modal');">Войти</button></div>
            @endif
        </div>
    </div>
</div>