{{--*/ $path_main_landing = URL::to('thema/img/main_landing') /*--}}

@extends('extends.main')
@section('head')
    @if(isset($needOgp) && $needOgp)
        <meta property="og:title" content="{{$ogp->title}}">
        <meta property="og:description" content="{{$ogp->description}}">
        <meta property="og:image" content="{{URL::to('/')}}{{$ogp->image}}">
    @endif

    <link rel="stylesheet" href="{{ URL::asset('thema/css/page/main_landing.css') }}">

    <script type="text/javascript">
        $(function() {
            $("input[name='clientPhone']").mask('+7 (999) 999-99-99?9');
        });

        function redirectSud() {
            window.location.href = "{{URL::route('articleSudAll')}}";
        }

        function openVideo(num) {
            iframe = $('#video_recall iframe');

            switch(num) {
                case 1:
                    url = 'https://www.youtube.com/embed/02-4oFMbpoQ';
                    break;

                case 2:
                    url = 'https://www.youtube.com/embed/PcRD96eh4ys?list=PLnKknCIQzlNIY7TDlFJvhzf_GiA57K88Y';
                    break;

                case 3:
                    url = 'https://www.youtube.com/embed/ttb7sX0c15g?list=PLnKknCIQzlNIY7TDlFJvhzf_GiA57K88Y';
                    break;

                case 4:
                    url = 'https://www.youtube.com/embed/7F5o-9wDcoU?list=PLnKknCIQzlNIY7TDlFJvhzf_GiA57K88Y';
                    break;

                case 5:
                    url = 'https://www.youtube.com/embed/UrjoG0bITAY?list=PLnKknCIQzlNIY7TDlFJvhzf_GiA57K88Y';
                    break;

                case 6:
                    url = 'https://www.youtube.com/embed/srN7CL7r5zQ';
                    break;

                case 7:
                    url = 'https://www.youtube.com/embed/u_OUscvwZOA?list=PLnKknCIQzlNIY7TDlFJvhzf_GiA57K88Y';
                    break;

                case 8:
                    url = 'https://www.youtube.com/embed/3FJQxmbC_nI?list=PLnKknCIQzlNIY7TDlFJvhzf_GiA57K88Y';
                    break;

                case 9:
                    url = 'https://www.youtube.com/embed/QO7Z-xl1Kio?list=PLnKknCIQzlNIY7TDlFJvhzf_GiA57K88Y';
                    break;

                case 10:
                    url = 'https://www.youtube.com/embed/Fu8mp8ePzzg?list=PLnKknCIQzlNIY7TDlFJvhzf_GiA57K88Y';
                    break;

                default:
                    console.log('video_not_found');
                    return;
            }

            iframe.attr('src', url);
            $('#video_recall').show();
        }

        var request;

        function sendRequest(thisBtn, checkRequestExists) {
            // по умолчанию проверяются дубликаты заявок
            if (checkRequestExists == undefined) checkRequestExists = true;

            if (thisBtn != null) {
                form = $(thisBtn).parent().parent();

                name = form.find('[name="clientName"]').val();
                phone = form.find('[name="clientPhone"]').val().replace(/\D/g, '');

                if (phone == '') {
                    toastr.info('Введите номер телефон');
                    return;
                }

                request = {
                    name: name,
                    phone: phone,
                    agentSubdomain: '{{$agentSubdomain}}'
                };
            }
            else if (request == undefined)
                return;

            $.ajax({
                url: "{{URL::route('landingSendRequest')}}",
                type: "POST",
                data: {
                    name: request.name,
                    phone: request.phone,
                    agentSubdomain: request.agentSubdomain,
                    checkRequestExists: checkRequestExists
                },
                success: function(r) {
                    if (r.status == 'ok') {
                        $('#requestExist').hide();
                        $('#js-request-accepted-text').html(r.agentCallbackMessage);
                        return $('#accepted').show();
                    }
                    else if (r.errors == 'request_exists')
                        $('#requestExist').show();
                    else
                        console.log(r.errors);
                }
            });
        }
    </script>
@endsection

@section('main_header')
    @include('main.header')
@endsection

@section('main_container')
    <div class="main_landing">
        <div class="block1">
            <div class="content">
                <div class="logo"></div>
                <div class="text1">
                    Как решить<br>
                    кредитные проблемы<br>
                    законным способом<br>
                    и начать жить полной жизнью?
                </div>
            </div>
        </div>

        <div class="block2">
            <div class="content">
                <div class="video">
                    <iframe src="https://www.youtube.com/embed/V-XSazLfuXM?wmode=opaque" frameborder="0" allowfullscreen=""></iframe>
                </div>
                <div class="text_see_video">Просмотри видео и узнай больше</div>
            </div>
        </div>

        <div class="block3">
            <div class="content">
                <div class="clock">
                    <div class="backImg"></div>
                    <div class="seconds">
                        <div></div>
                    </div>
                    <div class="closerRed"></div>
                    <div class="closerWhite"></div>
                </div>
                <div class="text">
                    <div class="bold">
                        Ваш долг,<br>
                        штрафы и пени<br>
                        ежесекундно растут?<br>
                    </div>
                    <div class="normal">
                        Вы находитесь в невыгодном положении <br>
                        перед банковской системой из-за невозможности <br>
                        платить за кредит! <br>
                    </div>
                </div>
            </div>
        </div>

        <div class="block4">
            <div class="content">
                <div class="left">
                    <div class="bold">
                        Узнайте как пошагово<br>
                        и законно выйти из сложной<br>
                        кредитной ситуации!
                    </div>
                    <div>
                        Запишитесь на бесплатную консультацию специалиста прямо сейчас.
                    </div>
                </div>
                <div class="right">
                    <div class="input-block">
                        <div>
                            <div class="input-title">Ваше имя:</div>
                            <div><input type="text" name="clientName"></div>
                        </div>
                        <div>
                            <div class="input-title">Номер телефона:</div>
                            <div><input type="text" name="clientPhone"></div>
                        </div>
                    </div>
                    <div>
                        <button class="btn btn_middle biz" onclick="sendRequest(this);">Получить консультацию</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="block5">
            <div class="content">
                <div class="video_container">
                    <div class="title1 bold">
                        Смотрите видео-отзывы о результатах<br>
                        нашей работы
                    </div>
                    <div>Тысячи людей уже начали решать свои кредитные проблемы, используя РОСНОК как опору. Это основа спокойствия и закона.</div>
                    <div class="video_list">
                        <div id="darkblock" onclick="closeVid()"></div>
                        <div class="video_block">
                            <img class="video_default_pic" onclick="openVideo(1)">
                            <figcaption onclick="openVideo(1);">
                                <a>Обращение клиента с проблемой запрета...</a>
                            </figcaption>
                        </div>
                        <div class="video_block">
                            <img class="video_default_pic" onclick="openVideo(2)">
                            <figcaption onclick="openVideo(2);">
                                <a>Запущена юридическая работа по избавлению...</a>
                            </figcaption>
                        </div>
                        <div class="video_block">
                            <img class="video_default_pic" onclick="openVideo(3)">
                            <figcaption onclick="openVideo(3);">
                                <a>Отмена решения суда по иску коллекторс...</a>
                            </figcaption>
                        </div>
                        <div class="video_block">
                            <img class="video_default_pic" onclick="openVideo(4)">
                            <figcaption onclick="openVideo(4);">
                                <a>Помощь в решении проблем с коллектор...</a>
                            </figcaption>
                        </div>
                        <div class="video_block">
                            <img class="video_default_pic" onclick="openVideo(5)">
                            <figcaption onclick="openVideo(5);">
                                <a>Помощь в решении проблем с коллектор...</a>
                            </figcaption>
                        </div>
                        <div class="video_block">
                            <img class="video_default_pic" onclick="openVideo(6)">
                            <figcaption onclick="openVideo(6);">
                                <a>Помощь в судебных разбирательствах...</a>
                            </figcaption>
                        </div>
                        <div class="video_block">
                            <img class="video_default_pic" onclick="openVideo(7)">
                            <figcaption onclick="openVideo(7);">
                                <a>Помощь пенсионерке в сложной ситуации с...</a>
                            </figcaption>
                        </div>
                        <div class="video_block">
                            <img class="video_default_pic" onclick="openVideo(8)">
                            <figcaption onclick="openVideo(8);">
                                <a>Помощь пенсионерке в сложной ситуации с...</a>
                            </figcaption>
                        </div>
                        <div class="video_block">
                            <img class="video_default_pic" onclick="openVideo(9)">
                            <figcaption onclick="openVideo(9);">
                                <a>Помощь в тяжелой кредитной ситуации...</a>
                            </figcaption>
                        </div>
                        <div class="video_block">
                            <img class="video_default_pic" onclick="openVideo(10)">
                            <figcaption onclick="openVideo(10);">
                                <a>Удалось вернуть деньги, которые судебные...</a>
                            </figcaption>
                        </div>
                    </div>
                </div>
                <div class="reasons_container">
                    <div class="title2 bold">
                        Почему<br>
                        люди выбирают РОСНОК?
                    </div>
                    <div class="reasons">
                        <div class="block_reason">
                            <div></div>
                            <div>
                                Более 3-х лет успешной работы
                                и тысячи людей, которые уже
                                скинули груз кредитных долгов
                            </div>
                        </div>
                        <div class="block_reason">
                            <div></div>
                            <div>
                                Отчёты о состоянии дел и работе
                                юристов предоставляются через
                                личный кабинет в реальном времени
                            </div>
                        </div>
                        <div class="block_reason">
                            <div></div>
                            <div>
                                Каждый подзащитный РОСНОК
                                получает бесплатные СМС-уведомления
                                на каждом этапе работы юристов
                            </div>
                        </div>
                        <div class="block_reason">
                            <div></div>
                            <div>
                                Звонки и угрозы от банков
                                и коллекторов принимают
                                специалисты колл-центра
                            </div>
                        </div>
                        <div class="block_reason">
                            <div></div>
                            <div>
                                Уникальная система электронного
                                документооборота, где не пропадет
                                ни один важный юридический документ
                            </div>
                        </div>
                        <div class="block_reason">
                            <div></div>
                            <div>
                                Низкие цены на юридические работы
                                доступные большинству граждан,
                                попавших в кредитную кабалу
                            </div>
                        </div>
                        <div class="block_reason">
                            <div></div>
                            <div>
                                Детальный анализ ситуации каждого
                                человека, обратившегося за помощью
                                к юристам и экспертам РОСНОК
                            </div>
                        </div>
                        <div class="block_reason">
                            <div></div>
                            <div>
                                Обратная связь и поддержка всегда,
                                на протяжении всего срока ведения
                                дела по проблемным кредитам
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="block6">
            <div class="content">
                <div class="title">
                    В общественный совет входят эксперты в области<br>
                    экономики и права
                </div>
                <div class="council_container">
                    <div class="man_block">
                        <div class="avatar"><img src="{{ $path_main_landing.'/goldfain.jpg' }}"></div>
                        <div class="name">
                            Евгений<br>
                            Гольдфайн
                        </div>
                        <div class="info">
                            Заслуженный экономист Российской Федерации.<br>
                            Лучший бухгалтер России 2004-2005г.<br>
                            Аналитик глобальных эконономических<br>
                            процессов.
                        </div>
                    </div>
                    <div class="man_block">
                        <div class="avatar"><img src="{{ $path_main_landing.'/renat_small.jpg' }}"></div>
                        <div class="name">
                            Ренат<br>
                            Шайхетдинов
                        </div>
                        <div class="info">
                            Глава Общественного совета РОСНОК.<br>
                            Сотрудник Следственного Комитета России в отставке.<br>
                            Руководитель юридического департамента.<br>
                            Старший преподаватель кафедры уголовного<br>
                            процесса Академии Следственного Комитета<br>
                            РФ до 2015г.
                        </div>
                    </div>
                    <div class="man_block">
                        <div class="avatar"><img src="{{ $path_main_landing.'/jalil.jpg' }}"></div>
                        <div class="name">
                            Джалиль<br>
                            Мубаракшин
                        </div>
                        <div class="info">
                            Советник главы общественного<br>
                            комитета РОСНОК.<br>
                            Профессиональный финансист<br>
                            с опытом работы свыше 20 лет
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="block7">
            <div class="content">
                <div class="title">Официальные документы РОСНОК</div>
                <div class="docs">
                    <div><img src="{{ $path_main_landing.'/ofDoc_1.jpg' }}"></div>
                    <div><img src="{{ $path_main_landing.'/ofDoc_2.jpg' }}"></div>
                    <div><img src="{{ $path_main_landing.'/ofDoc_3.jpg' }}"></div>
                </div>
            </div>
        </div>

        <div class="block8">
            <div class="content">
                <div class="title">
                    РОСНОК <br>
                    поможет Вам!
                </div>
                <div class="reasons">
                    <div class="block_reason">
                        <div></div>
                        <div>
                            Избавиться от незаконных<br>
                            комиссий по кредитам
                        </div>
                    </div>
                    <div class="block_reason">
                        <div></div>
                        <div>
                            Сократить сумму кредитной<br>
                            задолженности
                        </div>
                    </div>
                    <div class="block_reason">
                        <div></div>
                        <div>
                            Подготовить претензии<br>
                            и заявления в банки
                        </div>
                    </div>
                    <div class="block_reason">
                        <div></div>
                        <div>
                            Остановить рост кредитного<br>
                            долга через суд
                        </div>
                    </div>
                    <div class="block_reason">
                        <div></div>
                        <div>
                            Прекратить навязчивые<br>
                            звонки банков и коллекторов
                        </div>
                    </div>
                    <div class="block_reason">
                        <div></div>
                        <div>
                            Отправить запросы<br>
                            в правоохранительные органы
                        </div>
                    </div>
                    <div class="block_reason">
                        <div></div>
                        <div>
                            Сохранить имущество<br>
                            от ареста судебных приставов
                        </div>
                    </div>
                    <div class="block_reason">
                        <div></div>
                        <div>
                            Подвести кредитный<br>
                            долг к полному законному<br>
                            списанию
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="block4">
            <div class="content">
                <div class="left">
                    <div class="bold">
                        Сделать правильный шаг<br>
                        в решении<br>
                        накопившихся проблем!
                    </div>
                    <div>
                        Заполните свои данные и сделайте правильный первый шаг.
                    </div>
                </div>
                <div class="right">
                    <div class="input-block">
                        <div>
                            <div class="input-title">Ваше имя:</div>
                            <div><input type="text" name="clientName"></div>
                        </div>
                        <div>
                            <div class="input-title">Номер телефона:</div>
                            <div><input type="text" name="clientPhone"></div>
                        </div>
                    </div>
                    <div>
                        <button class="btn btn_middle biz" onclick="sendRequest(this);">Сделать шаг</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="block9">
            <div class="content">
                <div class="title">
                    Отстаивая Ваши законные интересы,<br>
                    взаимодействуем<br>
                    с госструктурами:
                </div>
                <div class="structure_list">
                    <div class="structure_block">
                        <a href="http://rkn.gov.ru/" class="hoverBlock" target="_blank"></a>
                        <div class="imageDiv"></div>
                        <div class="name">
                            Роскомнадзор
                        </div>
                    </div>
                    <div class="structure_block">
                        <a href="http://www.arbitr.ru/" class="hoverBlock" target="_blank"></a>
                        <div class="imageDiv"></div>
                        <div class="name">
                            Арбитражный<br>
                            суд РФ
                        </div>
                    </div>
                    <div class="structure_block">
                        <a href="https://mvd.ru/" class="hoverBlock" target="_blank"></a>
                        <div class="imageDiv"></div>
                        <div class="name">
                            Министерство<br>
                            внутренних дел РФ
                        </div>
                    </div>
                    <div class="structure_block">
                        <a href="http://www.vsrf.ru/" class="hoverBlock" target="_blank"></a>
                        <div class="imageDiv"></div>
                        <div class="name">
                            Верховный<br>
                            суд РФ
                        </div>
                    </div>
                    <div class="structure_block">
                        <a href="http://sledcom.ru/" class="hoverBlock" target="_blank"></a>
                        <div class="imageDiv"></div>
                        <div class="name">
                            Следственный<br>
                            комитет РФ
                        </div>
                    </div>
                    <div class="structure_block">
                        <a href="http://www.duma.gov.ru/" class="hoverBlock" target="_blank"></a>
                        <div class="imageDiv"></div>
                        <div class="name">
                            Государственная<br>
                            дума РФ
                        </div>
                    </div>
                    <div class="structure_block">
                        <a href="http://fssprus.ru/" class="hoverBlock" target="_blank"></a>
                        <div class="imageDiv"></div>
                        <div class="name">
                            Федеральная служба<br>
                            Судебных приставов
                        </div>
                    </div>
                    <div class="structure_block">
                        <a href="http://www.genproc.gov.ru/" class="hoverBlock" target="_blank"></a>
                        <div class="imageDiv"></div>
                        <div class="name">
                            Прокуратура РФ
                        </div>
                    </div>
                    <div class="structure_block">
                        <a href="http://rospotrebnadzor.ru/" class="hoverBlock" target="_blank"></a>
                        <div class="imageDiv"></div>
                        <div class="name">
                            Комитет по защите<br>
                            прав потребителей
                        </div>
                    </div>
                    <div class="structure_block">
                        <a href="http://www.cbr.ru/" class="hoverBlock" target="_blank"></a>
                        <div class="imageDiv"></div>
                        <div class="name">
                            Центральный<br>
                            банк РФ
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="block4 image_mod">
            <div class="content">
                <div class="left">
                    <div class="bold">
                        Обретите спокойствие<br>
                        и свободу от кредитов,
                    </div>
                    <div>
                        обратившись в Российский общественный совет по надзору в области<br>
                        кредитования прямо сейчас. Мы готовы ответить<br>
                        на все ваши вопросы, просто оставьте свою заявку здесь
                    </div>
                </div>
                <div class="right">
                    <div class="input-block">
                        <div>
                            <div class="input-title">Ваше имя:</div>
                            <div><input type="text" name="clientName"></div>
                        </div>
                        <div>
                            <div class="input-title">Номер телефона:</div>
                            <div><input type="text" name="clientPhone"></div>
                        </div>
                    </div>
                    <div>
                        <button class="btn btn_middle biz" onclick="sendRequest(this);">Подать заявку</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="block10">
            <div class="content">
                <div class="logo"></div>
                <div class="text">
                    <div class="bold">
                        Помните:<br>
                        Время — Деньги
                    </div>
                    <div>С уважением,  РОСНОК</div>
                </div>
            </div>
        </div>

        <div id="video_recall" class="modal_overlay" onclick="closeModal(this, event);">
            <div class="modal_window">
                <iframe src="" frameborder="0"></iframe>
            </div>
        </div>

        <div id="accepted" class="modal_overlay" onclick="closeModal(this, event);">
            <div class="modal_window">
                <button class="m_close" onclick="closeModal(this);">&times;</button>
                <div class="modal_header border_biz">Ваша заявка принята</div>
                <div class="modal_body">
                    <div class="row" id="js-request-accepted-text">

                    </div>
                    <div class="row">
                        А пока Вы можете ознакомиться с результатами работы наших юристов.
                    </div>
                    <div class="modal_footer">
                        <button class="btn btn_middle money" onclick="redirectSud();">Перейти к результатам</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="requestSuccess" class="modal_overlay" onclick="closeModal(this, event);">
            <div class="modal_window">
                <button class="m_close" onclick="closeModal(this);">&times;</button>
                <div class="modal_header border_biz">Системное сообщение</div>
                <div class="modal_body">
                    <div>Заявка успешно подана!</div>
                    <div class="modal_footer">
                        <button class="btn btn_middle money">Мне все понятно</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="requestExist" class="modal_overlay" onclick="closeModal(this, event);">
            <div class="modal_window">
                <button class="m_close" onclick="closeModal(this);">&times;</button>
                <div class="modal_header border_biz">Подсказка</div>
                <div class="modal_body">
                    <div>Вы уже подавали заявку на консультацию ранее.</div>
                    <div>Хотите отправить заявку еще раз?</div>
                    <div class="modal_footer">
                        <button class="btn btn_middle money" onclick="sendRequest(null, false);">Да, отправить</button>
                        <button class="btn btn_middle money" onclick="closeModal(this);">Нет, не нужно</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection