@extends('articles.extend')
@section('articles_container')
    <script>
        $(function() {
            $("a#img").fancybox();
        });
    </script>
    <div class="articles_container">
        <div class="title">
            <div class="center_container">
                <a href="{{URL::route('articleSudAll')}}">{{$articleInfo->title}}</a>
            </div>
        </div>
        <div class="article_block">
            <div class="center_container">
                <div>{{$oArticle->created_at->format('d.m.Y')}}</div>
                <div class="content">
                    @if(!empty($oArticle->imgSmall))
                    <div class="img">
                        <a id="img" href="{{"/$oArticle->imgBig"}}">
                            <img src="{{"/$oArticle->imgSmall"}}">
                        </a>
                    </div>
                    @endif
                    <div class="text_block">
                        <div class="title">{!!$oArticle->title!!}</div>
                        <div class="text">{!!str_replace("\n", "<br><br>", $oArticle->text)!!}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection