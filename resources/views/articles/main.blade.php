@extends('articles.extend')
@section('articles_container')
    <div class="articles_container">
        <div class="title">
            <div class="center_container">
                {{$articleInfo->title}}
            </div>
        </div>
        <div class="article_list">
            @foreach ($aArticle as $oArticle)
                <div class="article_element">
                    <div class="center_container">
                        <div>{{$oArticle->created_at->format('d.m.Y')}}</div>
                        <div class="content">
                            @if(!empty($oArticle->imgSmall))
                            <div class="img">
                                <img src="{{"/$oArticle->imgSmall"}}">
                            </div>
                            @endif
                            <div class="text_block">
                                <div class="title">{!!$oArticle->title!!}</div>
                                <div class="text">{!!str_replace("\n", "<br><br>", $oArticle->text)!!}</div>
                                <div class="link"><a href="{{URL::route($articleInfo->routeReadMore, $oArticle->name)}}">Читать далее</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection