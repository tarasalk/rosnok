@extends('extends.main')

@section('head')
    <link rel="stylesheet" href="{{ URL::asset('thema/css/page/articles.css') }}">
@endsection

@section('main_header')
    @include('main.header')
@endsection

@section('main_container')
    @yield('articles_container')
@endsection