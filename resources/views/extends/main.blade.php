<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{!! csrf_token() !!}" />

    <title>
        @section('title')
            Списать долги по кредитам? Реально через банкротство!
        @show
    </title>

    <link rel="shortcut icon" href="//cenoboy.ru/favicon.ico" type="image/x-icon">

    {{-- основные стили --}}
    <link rel="stylesheet" href="{{ URL::asset('thema/css/normalize.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('thema/css/global.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('thema/css/header.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('thema/css/main.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('thema/css/plugin_extends.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('thema/css/buttons.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('thema/css/modals.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('thema/css/icons.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('thema/css/tables.css') }}">

    {{-- стили плагинов --}}
    <link href='//fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ URL::asset('thema/css/jquery/jquery.fileupload.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('thema/css/jquery/jquery.formstyler.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('thema/css/jquery/jquery.datetimepicker.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('thema/css/jquery/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('thema/css/jquery/fancybox/jquery.fancybox-1.3.4.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('thema/css/air_datepicker/datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('thema/css/protip/protip.min.css') }}">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    {{-- скрипты плагинов --}}
    <script src="{{ URL::asset('thema/js/jquery/jquery-2.2.1.min.js') }}"></script>
    <script src="{{ URL::asset('thema/js/jquery/jquery.ui.widget.js') }}"></script>
    <script src="{{ URL::asset('thema/js/jquery/jquery.iframe-transport.js') }}"></script>
    <script src="{{ URL::asset('thema/js/jquery/jquery.fileupload.js') }}"></script>
    <script src="{{ URL::asset('thema/js/jquery/jquery.formstyler.min.js') }}"></script>
    <script src="{{ URL::asset('thema/js/jquery/jquery.ui.widget.js') }}"></script>
    <script src="{{ URL::asset('thema/js/jquery/jquery.validate.min.js') }}"></script>
    <script src="{{ URL::asset('thema/js/jquery/jquery.maskedinput.min.js') }}"></script>
    <script src="{{ URL::asset('thema/js/jquery/jquery.datetimepicker.full.min.js') }}"></script>
    <script src="{{ URL::asset('thema/js/jquery/jquery.cookie.js') }}"></script>

    <script src="{{ URL::asset('thema/js/jquery/jquery.fancybox-1.3.4.js') }}"></script>
    <script src="{{ URL::asset('thema/js/jquery/jquery.easing-1.3.pack.js') }}"></script>
    <script src="{{ URL::asset('thema/js/jquery/jquery.mousewheel-3.0.4.pack.js') }}"></script>
    <script src="{{ URL::asset('thema/js/jquery/jquery-ui.min.js') }}"></script>

    <script src="{{ URL::asset('thema/js/protip/protip.min.js') }}"></script>
    <script src="//cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    {{-- основные скрипты --}}
    <script src="{{ URL::asset('thema/js/global.js') }}"></script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        jQuery.validator.setDefaults({
            validClass: "jquery_validate_ok",
            errorClass: "jquery_validate_error"
        });
        jQuery.extend(jQuery.validator.messages, {
            required: "обязательное поле",
            remote: "исправьте это поле",
            email: "некорректный адрес почты",
            url: "некорректный адрес ссылки",
            date: "некорректная дата",
            //dateISO: "Please enter a valid date (ISO).",
            number: "введите числа",
            /*digits: "Please enter only digits.",
             creditcard: "Please enter a valid credit card number.",
             equalTo: "Please enter the same value again.",
             accept: "Please enter a value with a valid extension.",*/
            maxlength: jQuery.validator.format("введите не больше {0} символов"),
            minlength: jQuery.validator.format("введите не меньше {0} символов"),
            /*rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
             range: jQuery.validator.format("Please enter a value between {0} and {1}."),
             max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
             min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")*/
        });
        jQuery.validator.addMethod("rusWord", function(value, element) {
            return this.optional(element) || /^[А-ЯЁ][а-яё]*$/i.test(value);
        }, "только русские буквы");
        jQuery.validator.addMethod("length", function(value, element, param) {
            return this.optional(element) || value.length == param;
        }, "введите {0} символов");

        $.extend( $.fn.dataTable.defaults, {
            "language": {
                "search": "Поиск:",
                "info": "Показано _END_ из _TOTAL_ записей",
                "emptyTable": "не найдено",
                "lengthMenu": "Показать _MENU_",
                "infoEmpty": "Нет записей",
                "infoFiltered": "(поиск из _MAX_ записей)",
                "zeroRecords": "Ничего не найдено",
                "paginate": {
                    "next": "Далее",
                    "previous": "Назад"
                }
            }
        } );

        $(function () {
            $.datetimepicker.setLocale('ru');

            classes_AddRemove(['#header_menu'], 'active', false);
            classes_AddRemove(['div[data-menu=cabinet_menu]'], 'active', true);
            classes_AddRemove(['.menu_horizontal'], 'active', false);

            $('input[type=file]').styler();
        });
    </script>

    @section('head')
    @show
</head>
<body>
    @include('main.auth_modal')
    <div class="page">
        @yield('main_header')
        @yield('main_container')
        @yield('main_footer')
    </div>
    @include('include.yandexMetrika')
</body>
</html>