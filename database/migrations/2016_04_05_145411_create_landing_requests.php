<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandingRequests extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('landing_requests', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('agent_id');
			$table->string('user_name');
			$table->string('user_phone');
			$table->dateTime('processed_date');
			$table->string('comment');
			$table->tinyInteger('client_type');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('landing_requests');
	}
}
