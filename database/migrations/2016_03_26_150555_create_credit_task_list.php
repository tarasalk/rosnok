<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditTaskList extends Migration
{
    // список всех задач по кредитам
    public function up()
    {
        Schema::create('credit_task_list', function (Blueprint $table) {
            $table->increments('id');
            $table->text('task'); // текст задачи
        });
    }

    public function down()
    {
        Schema::drop('credit_task_list');
    }
}
