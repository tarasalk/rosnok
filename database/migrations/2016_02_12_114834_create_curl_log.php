<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurlLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // лог запросов через curlWrapper
        Schema::create('log_curl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sender_id'); // id юзера делающего запрос
            $table->string('url'); // адрес запроса
            $table->string('params'); // исходящие параметры
            $table->string('response'); // ответ
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('log_curl');
    }
}
