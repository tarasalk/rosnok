<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInboxDocEvents extends Migration
{
    public function up()
    {
        Schema::create('inbox_doc_events', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('doc_id');
            $table->integer('event_type_id');
            $table->integer('initiator_id'); // кто вызвал событие
            $table->string('notation');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('inbox_doc_events');
    }
}
