<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Клиенты Роснок
 */
class CreateClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id');
            $table->integer('agent_id');
            $table->integer('rate_pay');
            $table->integer('office_id');
            $table->integer('month');
            $table->tinyInteger('pay_status'); // WTF?
            $table->string('social_status'); // пенсионер, официально трудоустроен, нет официального дохода
            $table->integer('service_live'); // 7 или 12 месяцев срок обслуживания
            $table->dateTime('date_end_paid_month'); // дата окончания оплаченного месяца
            $table->dateTime('date_start_work'); // дата старта работы с клиентом(аналог activated_date)
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('clients');
    }
}
