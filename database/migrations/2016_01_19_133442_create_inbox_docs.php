<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Входящие документы
 */
class CreateInboxDocs extends Migration
{
	public function up() {
		Schema::create('inbox_docs', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('client_id');
			$table->integer('sender_id');
			$table->string('name');
			$table->tinyInteger('status');
			$table->string('file_path');
			$table->timestamps();
		});
	}

	public function down() {
		Schema::drop('inbox_docs');
	}
}
