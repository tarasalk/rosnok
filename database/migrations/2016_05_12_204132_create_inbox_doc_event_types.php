<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInboxDocEventTypes extends Migration
{
    public function up()
    {
        Schema::create('inbox_doc_event_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alias');
            $table->string('name');
        });
    }

    public function down()
    {
        Schema::drop('inbox_doc_event_types');
    }
}
