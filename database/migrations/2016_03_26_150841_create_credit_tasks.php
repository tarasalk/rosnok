<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditTasks extends Migration
{
    // связь между кредитами и задачами
    public function up()
    {
        Schema::create('credit_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('credit_id');
            $table->integer('task_id');
            $table->string('credit_type');// кредит, карта, микрозайм...
        });
    }

    public function down()
    {
        Schema::drop('credit_tasks');
    }
}
