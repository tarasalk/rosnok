<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditConsumers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_consumers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id'); // номер клиента
            $table->string('bank_name'); // название банка
            $table->string('contract_number'); // номер договора
            $table->dateTime('date_contract_create'); // дата заключения договора
            $table->bigInteger('credit_sum'); // сумма кредита
            $table->string('account_number'); // номер счета
            $table->string('credit_commitments_name'); // наименование кредитного обязательства
            $table->bigInteger('last_pay_sum'); // сумма последнего платежа
            $table->dateTime('date_last_pay'); // дата последнего платежа
            $table->dateTime('date_credit_end'); // срок кредита
            $table->bigInteger('average_monthly_pay'); // средний ежемесячный платеж
            $table->text('notation'); // примечание
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('credit_consumers');
    }
}
