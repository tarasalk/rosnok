<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffices extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('offices', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('holder_id');
			$table->string('office_name');
			$table->tinyInteger('has_payments');
			$table->string('alias');
			$table->string('attorney_number');
			$table->dateTime('attorney_date');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('offices');
	}
}
