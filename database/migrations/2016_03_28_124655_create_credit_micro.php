<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditMicro extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('credit_micro', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('client_id'); // номер клиента
			$table->string('mfo_id'); // id мфо
			$table->string('contract_number'); // номер договора
			$table->dateTime('date_contract_create'); // дата заключения договора
			$table->bigInteger('credit_sum'); // сумма кредита
			$table->dateTime('date_credit_end'); // срок кредита
			$table->text('notation'); // примечание
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('credit_micro');
	}
}
