<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSentDocs extends Migration
{
    public function up() {
        Schema::create('sent_docs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id');
            $table->integer('sender_id');
            $table->text('name');
            $table->enum('template_type', ['lawyer', 'template']);
            $table->text('recipient');
            $table->text('recipient_address');
            $table->text('sender_notation');
            $table->string('file_path');
            $table->dateTime('oisu_handed_date');
            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('sent_docs');
    }
}
