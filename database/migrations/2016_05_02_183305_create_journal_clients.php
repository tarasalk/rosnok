<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJournalClients extends Migration
{
    public function up()
    {
        Schema::create('journal_clients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agent_id');
            $table->string('name');
            $table->string('phone');
            $table->string('notation');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('journal_clients');
    }
}
