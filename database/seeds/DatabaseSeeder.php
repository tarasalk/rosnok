<?php

use App\Models\CreditTaskList;
use App\Models\InboxDocEventTypes;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
         $this->call(CreditTaskListSeeder::class);
         $this->call(InboxDocEventTypesSeeder::class);
    }
}

class CreditTaskListSeeder extends Seeder {

    public function run() {
        DB::table('credit_task_list')->truncate();

        CreditTaskList::create(['task' => 'Подготовить, предоставить мне на подпись и отправить заявление (запрос, претензию) в кредитную организацию']);
        CreditTaskList::create(['task' => 'Восстановить утерянные документы по кредиту (займу) и обязать кредитную организацию выслать их мне по адресу, указанному в договоре']);
        CreditTaskList::create(['task' => 'Подготовить запрос в коллекторскую компанию о правомочности требования моей задолженности']);
        CreditTaskList::create(['task' => 'Подготовить запрос в кредитное учреждение об отсутствии претензий и предоставление договора цессии']);
        CreditTaskList::create(['task' => 'Провести переговоры с коллекторской компанией, в том случае если третья сторона дает согласие на диалог']);
        CreditTaskList::create(['task' => 'Организовать переадресацию, обработку и фиксацию, поступающих мне нежелательных телефонных звонков']);
        CreditTaskList::create(['task' => 'Отправить заявление на отмену судебного приказа, заочного решения, аппеляцию']);
        CreditTaskList::create(['task' => 'Представлять мои интересы в федеральной службе судебных приставов']);
        CreditTaskList::create(['task' => 'Получить постановление о закрытии исполнительного производства в ФССП']);
    }
}

class InboxDocEventTypesSeeder extends Seeder {

    public function run() {
        DB::table('inbox_doc_event_types')->truncate();

        InboxDocEventTypes::create(['alias' => 'status_new', 'name' => 'новый']);
        InboxDocEventTypes::create(['alias' => 'status_accept', 'name' => 'принят']);
        InboxDocEventTypes::create(['alias' => 'status_rebuke', 'name' => 'принят с замечаниями']);
        InboxDocEventTypes::create(['alias' => 'status_denied', 'name' => 'отклонен']);
        InboxDocEventTypes::create(['alias' => 'send_lawyer', 'name' => 'отправлен юристу']);
        InboxDocEventTypes::create(['alias' => 'lawyer_read', 'name' => 'юрист прочитал']);
    }
}